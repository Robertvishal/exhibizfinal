//
//  EXLoginVisitorVC.swift
//  Exhibiz
//
//  Created by Appzoc on 07/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit

class EXLoginVisitorVC: UIViewController
{
    @IBOutlet var usernameTF: UITextField!
    @IBOutlet var passwordTF: UITextField!

    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nextBTNTapped(_ sender: UIButton)
    {
        if isValid()
        { loginWebCall()
        }
    }
    
    @IBAction func forgotBTNTapped(_ sender: UIButton)
    {
        let alert = UIAlertController(title: "Forgot Password ?", message: "Enter your email to reset password", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.text = ""
        }
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler:
            { [weak alert] (_) in
            let textField = alert?.textFields![0]
            print("Text field: \(textField!.text!)")
            if BaseValidator.isValid(email: textField!.text!)
            {
            }
            else
            { bannerWithMessage(message: "Enter a valid email")
              self.forgotBTNTapped(sender)
            }
            }))
        alert.addAction((UIAlertAction(title: "CANCEL", style: .cancel, handler: nil)))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func signUpBTNTapped(_ sender: UIButton)
    { let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXVisitorSignUpVC")
        self.present(nextVC, animated: true, completion: nil)
    }
    
    func isValid() -> Bool
    { if usernameTF.text!.isEmpty
      { bannerWithMessage(message: "Enter Email")
        return false
      }
      else if passwordTF.text!.isEmpty
      { bannerWithMessage(message: "Enter Password")
        return false
      }
      else
      { return true
      }
    }
    
    func loginWebCall()
    {
        var parameter = [String:String]()
        parameter["email"] = usernameTF.text!
        parameter["password"] = passwordTF.text!
        parameter["device_id"] = UserDefaults.standard.string(forKey: "DeviceToken")
        parameter["os"] = "I"
        parameter["user_type"] = "5"
        
        print(parameter)
        Webservices.postMethod(url: loginApi, parameter: parameter)
        { (isFetched, resultData) in
            if isFetched
            {   let userID = ((resultData["Data"] as! [[String:Any]])[0])["id"] as? String ?? ""
                let username = ((resultData["Data"] as! [[String:Any]])[0])["username"] as? String ?? ""
                let imageURL = ((resultData["Data"] as! [[String:Any]])[0])["image"] as? String ?? ""
                let userInterest = ((resultData["Data"] as! [[String:Any]])[0])["interest"] as? String ?? ""
                let contactNo = ((resultData["Data"] as! [[String:Any]])[0])["contactno"] as? String ?? ""
                let userEmail = ((resultData["Data"] as! [[String:Any]])[0])["email"] as? String ?? ""

                UserDefaults.standard.set(userID, forKey: "UserID")
                UserDefaults.standard.set(username, forKey: "Username")
                UserDefaults.standard.set(imageURL, forKey: "ImageURL")
                UserDefaults.standard.set(userEmail, forKey: "UserEmail")
                UserDefaults.standard.set(contactNo, forKey: "ContactNo")
                UserDefaults.standard.set(userInterest, forKey: "UserInterest")
                UserDefaults.standard.set(self.passwordTF.text!, forKey: "UserPassword")
                
                let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXAllEventsVisitorVC") as! EXAllEventsVisitorVC
                nextVC.isReloadRequired = true
                self.present(nextVC, animated: true, completion: nil)
            }
        }
    }
}


