//
//  EXUserSelectVC.swift
//  Exhibiz
//
//  Created by Appzoc on 03/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit

class EXUserSelectVC: UIViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    @IBAction func adminBTNTapped(_ sender: UIButton)
    { let storyBoard = UIStoryboard(name: "EXAdmin", bundle: nil)
      let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXLoginAdminVC")
      self.present(nextVC, animated: true, completion: nil)
    }
    @IBAction func organiserBTNTapped(_ sender: UIButton)
    { let storyBoard = UIStoryboard(name: "EXOrganiser", bundle: nil)
      let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXLoginOrganiserVC")
      self.present(nextVC, animated: true, completion: nil)
    }
    @IBAction func exhibitorBTNTapped(_ sender: UIButton)
    { let storyBoard = UIStoryboard(name: "EXExhibitor", bundle: nil)
      let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXLoginExhibitorVC")
      self.present(nextVC, animated: true, completion: nil)
    }
    @IBAction func visitorBTNTapped(_ sender: UIButton)
    { let storyBoard = UIStoryboard(name: "EXVisitor", bundle: nil)
      let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXLoginVisitorVC")
      self.present(nextVC, animated: true, completion: nil)
    }

}
