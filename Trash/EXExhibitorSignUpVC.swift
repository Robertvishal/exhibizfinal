//
//  EXExhibitorSignUpVC.swift
//  Exhibiz
//
//  Created by Appzoc on 22/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit

class EXExhibitorSignUpVC: UIViewController
{
    @IBOutlet var companynameTF: UITextField!
    @IBOutlet var passwordTF: UITextField!
    @IBOutlet var emailTF: UITextField!
    @IBOutlet var mobileNoTF: UITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nextBTNTapped(_ sender: UIButton)
    { if isValid()
      { registerWebCall()
      }
    }
    
    func isValid() -> Bool
    {
        if companynameTF.text!.isEmpty
        { bannerWithMessage(message: "Enter Company name")
            return false
        }
        else if passwordTF.text!.isEmpty
        { bannerWithMessage(message: "Enter Password")
            return false
        }
        else if emailTF.text!.isEmpty
        { bannerWithMessage(message: "Enter Email")
            return false
        }
        else if BaseValidator.isValid(email: emailTF.text!)
        { bannerWithMessage(message: "Enter valid Email")
            return false
        }
        else if mobileNoTF.text!.isEmpty
        { bannerWithMessage(message: "Enter Mobile Number")
            return false
        }
        else if !BaseValidator.isValid(digits: mobileNoTF.text!)
        { bannerWithMessage(message: "Enter Valid Mobile Number")
            return false
        }
        else
        { return true
        }
    }
    
    func registerWebCall()
    {
        var parameter = [String:Any]()
        parameter["email"] = emailTF.text!
        parameter["password"] = passwordTF.text!
        parameter["device_id"] = UserDefaults.standard.string(forKey: "DeviceToken")
        parameter["os"] = "I"
        parameter["phone"] = mobileNoTF.text!
        parameter["company_name"] = companynameTF.text!
        parameter["usertype"] = "[4]"
        print("params",parameter)
        print("URL",registrationApi)
        Webservices.postMethod(url: registrationApi, parameter: parameter)
        { (isFetched, resultData) in
            if isFetched
            {
                bannerWithMessage(message: resultData["Message"] as? String ?? "")
                self.dismiss(animated: true, completion: nil)
            }
        }
    }


}
