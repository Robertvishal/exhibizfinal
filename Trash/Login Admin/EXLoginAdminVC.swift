//
//  EXLoginAdminVC.swift
//  Exhibiz
//
//  Created by Appzoc on 03/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import BRYXBanner

class EXLoginAdminVC: UIViewController
{
    @IBOutlet var userNameTF: UITextField!
    @IBOutlet var passwordTF: UITextField!
    
    // Data variables
    final var userNameText: String = ""
    final var passwordText: String = ""
    final var deviceID = "a"
    lazy var iOS = "I"
    private var userType = "2" // admin
    lazy var URLLogin = "ExhibizWeb/backend/api/login"
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nextBTNTapped(_ sender: UIButton)
    {
        if BaseValidator.isValid(email: userNameTF.text)
        {
            userNameText = userNameTF.text ?? ""
            passwordText = passwordTF.text ?? ""
            self.webCall()
        }
        else
        {
            // invalid/empty email banner
            Banner(title: "Invalid Email!", subtitle: "Please use a valid email to login", backgroundColor: bannerRed).show(duration: 3)
        }

        
    }
    
    @IBAction func forgotBTNTapped(_ sender: UIButton)
    {
        let alert = UIAlertController(title: "Forgot Password ?", message: "Enter your email to reset password", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.text = ""
        }
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler:
            { [weak alert] (_) in
                let textField = alert?.textFields![0]
                print("Text field: \(textField!.text!)")
                if BaseValidator.isValid(email: textField!.text!)
                {
                }
                else
                { bannerWithMessage(message: "Enter a valid email")
                    self.forgotBTNTapped(sender)
                }
        }))
        alert.addAction((UIAlertAction(title: "CANCEL", style: .cancel, handler: nil)))
        self.present(alert, animated: true, completion: nil)
    }
    
}


// Mark:- Handling username and password TF delegates
extension EXLoginAdminVC: UITextFieldDelegate
{

    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField == userNameTF
        {
            self.userNameText = ""
        }
        else
        {
            self.passwordText = ""
        }
        return true
    }
    
}

// Mark:- Handling web services
extension EXLoginAdminVC
{
    
    private func webCall()
    {
         //userNameText = "varungopal2@gmail.com"//"admin@exhi.com"
       // varungopal2@gmail.com
        // passwordText = "123456"
        let params:Json = ["email": userNameText, "password": passwordText, "device_id": UserDefaults.standard.string(forKey: "DeviceToken") as Any, "os": iOS, "user_type": userType]
        debugPrint("params:",params)
    
        Webservices.postMethod(url: URLLogin, parameter: params) { (isSucceeded, response) in
            if isSucceeded
            {
                let userID = ((response["Data"] as! [[String:Any]])[0])["id"] as? String ?? ""
                let username = ((response["Data"] as! [[String:Any]])[0])["username"] as? String ?? ""
                let imageURL = ((response["Data"] as! [[String:Any]])[0])["image"] as? String ?? ""
                UserDefaults.standard.set(userID, forKey: "UserID")
                UserDefaults.standard.set(username, forKey: "Username")
                UserDefaults.standard.set(imageURL, forKey: "ImageURL")
                
                guard let array:[Any] = response["Data"] as? Array, let object: Json = array[0] as? Json, !object.isEmpty else { return }
                let data = LoginModel.getValues(from: object)

                Credentials.shared.id = data.id!.description
                Credentials.shared.email = data.email!
    
                
                let isShow = response["Isshowbanner"] as! Int
                let ShowUrl = response["Url"] as! String
                
                
                 if isShow == 1
                 {
                    let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "Show_Web_ViewController") as! Show_Web_ViewController
                    nextVC.webUrl = ShowUrl
                    self.present(nextVC, animated: true, completion: nil)
                 }
                 else
                 {
                    let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXMyEventsVC") as! EXMyEventsVC
                    nextVC.isRequiredReload = true
                    self.present(nextVC, animated: true, completion: nil)
                }
            }
        }
    }
}



