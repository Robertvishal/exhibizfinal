//
//  EXLoginAdmin Web + Model.swift
//  Exhibiz
//
//  Created by Appzoc on 24/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import Foundation

public class Credentials {
    final lazy var id: String = ""
    final lazy var email: String = ""
    static let shared = Credentials()
    private init(){}
}
//  Mark:- Login Model
public class LoginModel {
    let id : Int?
    public var username : String?
    public var email : String?
    public var contactno : Int?
    public var interest : Int?
    public var image : String?
    public var created_at : String?
    public var type_id : Int?

    
    final class func getValues(from object: Json) -> LoginModel {
        return LoginModel(dictionary: object)!
    }
    
    required public init?(dictionary: Json) {
        id = Int(dictionary["id"] as? String ?? "0")
        username = dictionary["username"] as? String ?? ""
        email = dictionary["email"] as? String ?? ""
        contactno = Int(dictionary["contactno"] as? String ?? "0")
        interest = Int(dictionary["interest"] as? String ?? "0")
        image = dictionary["image"] as? String ?? ""
        created_at = dictionary["created_at"] as? String ?? ""
        type_id = Int(dictionary["type_id"] as? String ?? "0")
        
    }
    
}



