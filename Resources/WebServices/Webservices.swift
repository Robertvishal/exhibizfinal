//
//  Webservices.swift
//  Exhibiz
//
//  Created by Appzoc on 22/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import Foundation
import Alamofire
import BRYXBanner

// OLD BaSE URLS
//let baseURL = "http://app.appzoc.com/"
//let baseURLImage = "http://app.appzoc.com/ExhibizWeb/backend/storage/app/"
//let imageURL = "ExhibizWeb/backend/storage/app/"

// New Base URLS
let baseURL = "https://signitivebeta.com/"
let baseURLImage = "http://signitivebeta.com/exhibizWeb/backend/storage/app/"
let imageURL = "exhibizWeb/backend/storage/app/"

//"http://app.appzoc.com/ExhibizWeb/backend/api/login" old
//http://signitivebeta.com/exhibizWeb/backend/api/login new
//http://signitivebeta.com/exhibizWeb/backend/storage/app/uploads/sponsors/VR8IeC8UqK6HKRwhntSOz6vddUMcZ6NKDlV13SeL.jpeg

let usernameAuth = "exhibix"
let passwordAuth = "web"

// API List

let allEventsList = "ExhibizWeb/backend/api/events"
let allExhibitionsList = "ExhibizWeb/backend/api/get/exhibition/" // EventID

extension String {
    var correctBaseURL: String {
        return self.replacingOccurrences(of: "ExhibizWeb", with: "exhibizWeb")
    }
}

final class Webservices
{
    private init(){}
    class func getMethod(url:String, errorBanner:Bool = true, CompletionHandler:@escaping ((Bool,[String:Any])->()))
    {
        let activity = ActivityIndicator()
        activity.start()
        print("\(baseURL)\(url.correctBaseURL)")
        let user = usernameAuth
        let password = passwordAuth
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        
        Alamofire.request("\(baseURL)\(url.correctBaseURL)",
            method: .get,
            parameters: nil,
            encoding: URLEncoding.default,
            headers:headers)
            .validate()
            .responseJSON
            { response in
                activity.stop()
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let result = response.result.value as! [String:Any]
                        print(result)
                        
                        let Errorcode = result["Error_Code"] as? String ?? "1"
                        let Message = result["Message"] as? String ?? "Error occured while fetching data"
                        
                        if Errorcode == "0"
                        {
                            CompletionHandler(true, result)
                        }
                        else
                        {   CompletionHandler(false, ["":""])
                            if errorBanner == true
                            {
                                Banner(title: "Oops...!", subtitle: Message, backgroundColor: bannerRed).show(duration: 3)
                            }
                        }
                    }
                    
                    break
                    
                case .failure(_):
                    CompletionHandler(false, ["":""])
                    Banner(title: "Oops...!", subtitle: "Something went wrong while connecting", backgroundColor: bannerRed).show(duration: 3)
                    break
                }
        }
    }
    
    // Parameterised GET Method
    class func getMethodWith(url:String, parameter: [String:Any]?,CompletionHandler:@escaping ((Bool,[String:Any])->()))
    {
        let activity = ActivityIndicator()
        activity.start()
        print("\(baseURL)\(url.correctBaseURL)")
        let user = usernameAuth
        let password = passwordAuth
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        
        Alamofire.request("\(baseURL)\(url.correctBaseURL)",
            method: .get,
            parameters: parameter,
            encoding: URLEncoding.default,
            headers:headers)
            .validate()
            .responseJSON
            { response in
                
                switch(response.result)
                {
                case .success(_):
                    activity.stop()
                    if response.result.value != nil
                    {
                        let result = response.result.value as! [String:Any]
                        print(result)
                        
                        let Errorcode = result["Error_Code"] as? String ?? "1"
                        let Message = result["Message"] as? String ?? "Error occured while fetching data"
                        
                        if Errorcode == "0"
                        {
                            CompletionHandler(true, result)
                        }
                        else
                        {   CompletionHandler(false, ["":""])
                            Banner(title: "Oops...!", subtitle: Message, backgroundColor: bannerRed).show(duration: 3)
                        }
                    }
                    
                    break
                    
                case .failure(_):
                    activity.stop()
                    CompletionHandler(false, ["":""])
                    Banner(title: "Oops...!", subtitle: "Something went wrong while connecting", backgroundColor: bannerRed).show(duration: 3)
                    break
                }
        }
    }

    
    
    //Mark:- POST Methods
    class func postMethod(url:String, parameter:[String:Any], CompletionHandler:@escaping ((Bool,[String:Any])->()))
    {
        let activity = ActivityIndicator()
        activity.start()
        
        let user = usernameAuth
        let password = passwordAuth
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/x-www-form-urlencoded"]
        
        print("\(baseURL)\(url.correctBaseURL)")
        
        Alamofire.request("\(baseURL)\(url.correctBaseURL)",
                          method: .post,
                          parameters: parameter,
                          encoding: URLEncoding.default,
                          headers:headers)
            .responseJSON
            { response in
                activity.stop()
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let result = response.result.value as! [String:Any]
                        print(result)
                        
                        let Errorcode = result["Error_Code"] as? String ?? "1"
                        let Message = result["Message"] as? String ?? "Error occured while fetching data"
                        
                        if Errorcode == "0"
                        {
                            CompletionHandler(true, result)
                        }
                        else
                        {   CompletionHandler(false, ["":""])
                            Banner(title: "Oops...!", subtitle: Message, backgroundColor: bannerRed).show(duration: 3)
                        }
                    }
                    
                    break
                    
                case .failure(_):
                    CompletionHandler(false, ["":""])
                    Banner(title: "Oops...!", subtitle: "Something went wrong while connecting", backgroundColor: bannerRed).show(duration: 3)
                }
        }
    }
    
    
    // Mark:- Muiltipart data web call
    
    class func postMethodMultiPartImage(url:String, parameter:[String:Any], imageParameter: [String:[UIImage]] , CompletionHandler:@escaping ((Bool,[String:Any])->())) {
       
        let activity = ActivityIndicator()
        activity.start()
        print("\(baseURL)\(url.correctBaseURL)")
        
        let user = usernameAuth
        let password = passwordAuth
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/json"]
        
        Alamofire.upload(multipartFormData:
            { multipartFormData in
                
                for (key, value) in parameter
                {
                    multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
                }

                if !imageParameter.isEmpty {
                    let imageKey = imageParameter[imageParameter.startIndex].key
                    let imageArray = imageParameter[imageKey]
                    var index: Int = 1
                    for image in imageArray! {
                        let imageData = UIImageJPEGRepresentation(image, 1)!
                        let imageFileName = imageKey + index.description + ".jpeg"
                        multipartFormData.append(imageData, withName: imageKey, fileName: imageFileName, mimeType: "image/jpeg")
                        index += 1
                    }
                    
                }
                
        },
                         to: "\(baseURL)\(url.correctBaseURL)",
            headers : headers,
            encodingCompletion:
            { encodingResult in
                activity.stop()

                switch encodingResult {
                    
                case .success(let upload, _, _):
                    upload.responseString(completionHandler: { (response) in
                        print(response)
                    })
                    
                    upload.responseJSON(completionHandler: { response in
                        print(response)
                        if response.result.value != nil
                        {
                            let result = response.result.value as! [String:Any]
                            debugPrint(result)
                            
                            let Errorcode = result["Error_Code"] as? String ?? "1"
                            let Message = result["Message"] as? String ?? "Error occured while fetching data"
                            
                            if Errorcode == "0"
                            {
                                CompletionHandler(true, result)
                            }
                            else
                            {   CompletionHandler(false, ["":""])
                                Banner(title: "Oops...!", subtitle: Message, backgroundColor: bannerRed).show(duration: 3)
                            }
                        }
                    })
                    
                case .failure(_):
                    activity.stop()
                    CompletionHandler(false, ["":""])
                    Banner(title: "Oops...!", subtitle: "Something went wrong while connecting", backgroundColor: bannerRed).show(duration: 3)

                }
        }
        )
        
    }
    
    class func postMethodMultiPartImageWithResponseReturnIfFailure(url:String, parameter:[String:Any], imageParameter: [String:[UIImage]] , CompletionHandler:@escaping ((Bool,[String:Any])->())) {
        
        let activity = ActivityIndicator()
        activity.start()
        print("\(baseURL)\(url.correctBaseURL)")
        
        let user = usernameAuth
        let password = passwordAuth
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/json"]
        
        Alamofire.upload(multipartFormData:
            { multipartFormData in
                
                for (key, value) in parameter
                {
                    multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
                }
                
                if !imageParameter.isEmpty {
                    let imageKey = imageParameter[imageParameter.startIndex].key
                    let imageArray = imageParameter[imageKey]
                    var index: Int = 1
                    for image in imageArray! {
                        let imageData = UIImageJPEGRepresentation(image, 1)!
                        let imageFileName = imageKey + index.description + ".jpeg"
                        multipartFormData.append(imageData, withName: imageKey, fileName: imageFileName, mimeType: "image/jpeg")
                        index += 1
                    }
                    
                }
                
        },
                         to: "\(baseURL)\(url.correctBaseURL)",
            headers : headers,
            encodingCompletion:
            { encodingResult in
                activity.stop()
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    upload.responseString(completionHandler: { (response) in
                        print(response)
                    })
                    
                    upload.responseJSON(completionHandler: { response in
                        print(response)
                        if response.result.value != nil
                        {
                            let result = response.result.value as! [String:Any]
                            debugPrint(result)
                            
                            let Errorcode = result["Error_Code"] as? String ?? "1"
                            
                            if Errorcode == "0"
                            {
                                CompletionHandler(true, result)
                            }
                            else
                            {   CompletionHandler(false, result)
                                
                            }
                        }
                    })
                    
                case .failure(_):
                    activity.stop()
                    CompletionHandler(false, ["":""])
                    Banner(title: "Oops...!", subtitle: "Something went wrong while connecting", backgroundColor: bannerRed).show(duration: 3)
                    
                }
        }
        )
        
    }
    class func postMethodMultiPartSpeakerImage(url:String, parameter:[String:Any],
                                                imageParameter: [String:[UIImage]],
                                                speakerImages:[UIImage],
                                                CompletionHandler:@escaping ((Bool,[String:Any])->()))
    {
        let activity = ActivityIndicator()
        activity.start()
        print("\(baseURL)\(url.correctBaseURL)")
        
        let user = usernameAuth
        let password = passwordAuth
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/json"]
        
        Alamofire.upload(multipartFormData:
            { multipartFormData in
                
                for (key, value) in parameter
                {
                    multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
                }
                if !imageParameter.isEmpty
                {
                    let imageKey = imageParameter[imageParameter.startIndex].key
                    let imageArray = imageParameter[imageKey]
                    var index: Int = 1
                    for image in imageArray!
                    {
                        let imageData = UIImageJPEGRepresentation(image, 1)!
                        let imageFileName = imageKey + index.description + ".jpeg"
                        multipartFormData.append(imageData, withName: imageKey, fileName: imageFileName, mimeType: "image/jpeg")
                        index += 1
                    }
                    
                    for image in speakerImages
                    {
                        let imageData = UIImageJPEGRepresentation(image, 1)!
                        let imageFileName = "speakerimage[]" + index.description + ".jpeg"
                        multipartFormData.append(imageData, withName: "speakerimage[]", fileName: imageFileName, mimeType: "image/jpeg")
                        index += 1
                    }
                }
                
        },
                         to: "\(baseURL)\(url.correctBaseURL)",
            headers : headers,
            encodingCompletion:
            { encodingResult in
                activity.stop()

                switch encodingResult {
                    
                case .success(let upload, _, _):
                    upload.responseString(completionHandler: { (response) in
                        print(response)
                    })
                    
                    upload.responseJSON(completionHandler: { response in
                        print(response)
                        if response.result.value != nil
                        {
                            let result = response.result.value as! [String:Any]
                            debugPrint(result)
                            
                            let Errorcode = result["Error_Code"] as? String ?? "1"
                            let Message = result["Message"] as? String ?? "Error occured while fetching data"
                            
                            if Errorcode == "0"
                            {
                                CompletionHandler(true, result)
                            }
                            else
                            {   CompletionHandler(false, ["":""])
                                Banner(title: "Oops...!", subtitle: Message, backgroundColor: bannerRed).show(duration: 3)
                            }
                        }
                    })
                    
                case .failure(_):
                    activity.stop()
                    CompletionHandler(false, ["":""])
                    Banner(title: "Oops...!", subtitle: "Something went wrong while connecting", backgroundColor: bannerRed).show(duration: 3)
                    
                }
        }
        )
    }
    
    
    
    //Mark:- paymentTestWebService
    class func paymentTestWebService(CompletionHandler:@escaping ((Bool,[String:Any])->()))
    {
        let activity = ActivityIndicator()
        activity.start()
        
        let parameter = ["amount":"100","currency":"SAR","description":"Slot Booking Test Payment"]
        let user = "sk_test_R2usNPbmUgXddBAhEAfQZSxHUZK984Faxhp8jJk7"
        let password = ""
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)","Content-Type":"application/x-www-form-urlencoded"]
        
        Alamofire.request("https://api.moyasar.com/v1/invoices",
            method: .post,
            parameters: parameter,
            encoding: URLEncoding.default,
            headers:headers)
            .responseJSON
            { response in
                activity.stop()
                switch(response.result)
                {
                case .success(_):
                    if response.result.value != nil
                    {
                        let result = response.result.value as! [String:Any]
                        print(result)
                        CompletionHandler(true, result)
                    }
                    
                    break
                    
                case .failure(_):
                    CompletionHandler(false, ["":""])
                    Banner(title: "Oops...!", subtitle: "Something went wrong while connecting", backgroundColor: bannerRed).show(duration: 3)
                }
        }
    }
    
}

class ActivityIndicator
{
    var activityView:UIActivityIndicatorView!
    var view = UIView(frame: UIScreen.main.bounds)
    
    func start()
    {
        if activityView == nil
        { activityView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
          activityView.frame = CGRect(x: (view.bounds.maxX/2)-20,
                                        y: (view.bounds.maxY/2)-20,
                                        width: 40, height: 40)
          self.view.addSubview(activityView)
          self.view.backgroundColor = UIColor(red:0.26, green:0.26, blue:0.26, alpha:0.8)
          UIApplication.shared.keyWindow?.addSubview(self.view)
          activityView.startAnimating()
        }
        else if activityView.isAnimating == true
        {
        }
        else
        { UIApplication.shared.keyWindow?.addSubview(self.view)
          activityView.startAnimating()
        }
    }
    
    func stop()
    {
        if activityView == nil || activityView.isAnimating == false
        {
        }
        else
        { activityView.stopAnimating()
          self.view.removeFromSuperview()
        }
    }
}



