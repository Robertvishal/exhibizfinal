//
//  Constants.swift
//  Exhibiz
//
//  Created by Appzoc on 05/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import Foundation
import UIKit
import BRYXBanner
import Photos
import StoreKit

// Constant Values
let blueColor = UIColor(red:0.10, green:0.45, blue:0.91, alpha:1.0)
let mercuryColor = UIColor(red:0.92, green:0.92, blue:0.92, alpha:1.0)
let bannerRed = UIColor(red:0.78, green:0.16, blue:0.16, alpha:1.0)
let greenColour = UIColor(red:0.15, green:0.50, blue:0.05, alpha:1.0)

// bannerRed = UIColor(red:0.78, green:0.16, blue:0.16, alpha:1.0)
public typealias Json = [String:Any]
public typealias JsonArray = [Any]


// Constant Functions
func openAppStore()
{
    
    rateExhbiz()
//    let url  = URL(string: "itms-apps://itunes.apple.com/us/app/facebook/id284882215?mt=8")
//    if UIApplication.shared.canOpenURL(url!)
//    {
//        UIApplication.shared.openURL(url!)
//    }
}

func commonRating(){
    
}

    func commomShare(viewcontroller:UIViewController)
    {
        if let link = URL(string: "https://itunes.apple.com/us/app/exhbiz/id1436806178?ls=1&mt=8") {
            let text = "Download Exhibiz"
            let contents = [text,link] as [Any]
            
            let activityVC = UIActivityViewController(activityItems: contents, applicationActivities: nil)
            activityVC.popoverPresentationController?.sourceView = viewcontroller.view;
            viewcontroller.present(activityVC, animated: true, completion: nil)

        }
    }

    func ComingSoonBanner()
    {
        Banner(title: "Please be patient",
               subtitle: "We are working on that",
               image: #imageLiteral(resourceName: "trolley.png"),
               backgroundColor: greenColour).show(duration: 2)
    }

func bannerWithMessage(message:String)
{
    Banner(title: nil,
           subtitle: message,
           image: nil,
           backgroundColor: greenColour).show(UIApplication.shared.keyWindow, duration: 2)
}
func bannerWith(title: String, andMessage:String)
{
    Banner(title: title,
           subtitle: andMessage,
           image: nil,
           backgroundColor: greenColour).show(UIApplication.shared.keyWindow, duration: 2)
}

func dayNameFrom(_ string: String?, format: String) -> String {
    guard let dateString = string, dateString != "" else { return "" }
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format
    let date = dateFormatter.date(from: dateString)
    dateFormatter.dateFormat = "EEEE"
    return dateFormatter.string(from: date!)
}

func fullDateFrom(_ string: String?, format: String) -> String {
    guard let dateString = string, dateString != "" else { return "" }
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format
    let date = dateFormatter.date(from: dateString)
    dateFormatter.dateFormat = "MMM dd, yyyy"
    return dateFormatter.string(from: date!)
}

func getfullDateInCommonFormat(_ string: String?, format: String) -> String {
    guard let dateString = string, dateString != "" else { return "" }
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format
    let date = dateFormatter.date(from: dateString)
    dateFormatter.dateFormat = "dd-MM-yyyy"
    return dateFormatter.string(from: date!)
}

func getCreatedDate() -> String
{
    let date = Date()
    let selectedDate = DateFormatter()
    selectedDate.dateFormat = "yyyy-MM-dd"
    return selectedDate.string(from: date)
}

func commonShare(title:String, description:String, image:UIImage, viewController:UIViewController)
{
    let myString = title
    let myLink = description
    let myArray = [myString,myLink as Any,image] as [Any]
    let sample = UIActivityViewController(activityItems: myArray, applicationActivities: nil)
    viewController.present(sample, animated: true, completion: nil)
    sample.popoverPresentationController?.sourceView = viewController.view;
}

func getUIImage(asset: PHAsset) -> UIImage
{
    print(asset)
//    var img: UIImage?
//    let manager = PHImageManager.default()
//    let options = PHImageRequestOptions()
//    options.version = .original
//    options.isSynchronous = true
//    manager.requestImageData(for: asset, options: options) { data, _, _, _ in
//
//        if let data = data {
//            img = UIImage(data: data)
//        }
//    }
//    return img!
    
    let manager = PHImageManager.default()
    let option = PHImageRequestOptions()
    var thumbnail = UIImage()
    option.isSynchronous = true
    manager.requestImage(for: asset, targetSize: CGSize(width: 100, height: 100), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
        thumbnail = result!
    })
    return thumbnail
    
}


func getImageFromUrl(url: String) -> UIImage?
{
    let url = URL(string:url)
    var image:UIImage?
    if let data = try? Data(contentsOf: url!)
    {
        image = UIImage(data: data)
    }
    return image
}

extension UITableView
{
    func scrollTableToTop()
    {
        if self.numberOfRows(inSection: 0)>0
        {
            self.scrollToRow(at: IndexPath(item: 0, section: 0),
                                  at: UITableViewScrollPosition.top,
                                  animated: true)
        }
    }
}



func rateExhbiz()
{
    if #available(iOS 10.3, *) {
        
        SKStoreReviewController.requestReview()
        
    } else {
        
        //https://itunes.apple.com/us/app/exhbiz/id1436806178?ls=1&mt=8
        
        guard let url = URL(string: "https://itunes.apple.com/us/app/exhbiz/id1436806178?ls=1&mt=8") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
}


func makeCall(to number: Int) {
    //number = "+966 55 505 0249"
    if let url = URL(string: "tel://+966555050249"), UIApplication.shared.canOpenURL(url)
    {
        if #available(iOS 10, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }
    
}








