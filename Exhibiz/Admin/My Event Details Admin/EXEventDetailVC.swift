//
//  EXEventDetailVC.swift
//  Exhibiz
//
//  Created by Appzoc on 12/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import ImageSlideshow
import Kingfisher

class EXEventDetailVC: UIViewController
{
    @IBOutlet var organisersCV: UICollectionView!
    @IBOutlet var sponsorsCV: UICollectionView!
    @IBOutlet var uploadEventBTN: BaseButton!
    @IBOutlet var slideShowView: ImageSlideshow!
    @IBOutlet var eventTitleLBL: UILabel!
    @IBOutlet var descriptionLBL: UILabel!
    @IBOutlet var locationLBL: UILabel!
    @IBOutlet var startDayNameLBL: UILabel!
    @IBOutlet var endDayNameLBL: UILabel!
    @IBOutlet var startDateLBL: UILabel!
    @IBOutlet var endDateLBL: UILabel!
    @IBOutlet var eventTypeLBL: UILabel!
    @IBOutlet var slotsAvailableLBL: UILabel!
    @IBOutlet var entryTypePaidBTN: BaseButton!
    @IBOutlet var entryTypeFreeBTN: BaseButton!
    @IBOutlet var ticketPriceLBL: UILabel!
    @IBOutlet var heightTicketPriceView: NSLayoutConstraint!
    
    @IBOutlet var speakersHeight: NSLayoutConstraint!
    @IBOutlet var speakersTV: UITableView!
    @IBOutlet var editBTN: UIButton!
    
    
    // Data Variables
    final var isFromMyEvents = false // for upload data after adding
    final var content: ADFuturePresentPastModel?
    final var organisorsArray = [ADAssignedMembersModel]()
    final var sponsorsArray = [ADSponersModel]()
    final var sliderImagesArray = [KingfisherSource]()
    final var selectedOrganisors = [ADOrganisorListModel]()
    final var selectedSponsors = [ADSponsorListModel]()
    final var addEventParameter = Json()
    final var addEventParameterevd = Json()
    lazy var URLAddEvent = "ExhibizWeb/backend/api/add/events/"
    var ConferenceModel = [ConferenceCellModel]()
    
    var imageLinkArray:[String] = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        if isFromMyEvents
        { uploadEventBTN.setTitle("Announcement", for: .normal)
        }
        else
        { uploadEventBTN.setTitle("Upload Event", for: .normal)
          addEventParameterevd = addEventParameter
        }
        slideShowView.contentScaleMode = .scaleAspectFill
        slideShowView.slideshowInterval = 2
 
        setUpInterface()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        if content?.category_id! == 1
        { speakersHeight.constant = speakersTV.contentSize.height + 30
        }
        else
        { speakersHeight.constant = 0
        }
        
        setUpTapGesture()
    }
    
    func setUpTapGesture(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        slideShowView.addGestureRecognizer(tapGesture)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer){
        
        slideShowView.presentFullScreenController(from: self)
        
//        print("Gesture Tapped")
//        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EXImageViewerVC") as! EXImageViewerVC
//        vc.sourceType = .link
//        vc.linkSource = imageLinkArray
//        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func gotoMapTapped(_ sender: UIButton) {
        self.openInMap()
    }
    
    func openInMap(){
        guard let latitude = content?.lat else {return}
        guard let longitude = content?.lon else {return}
        let mapAppURL = "comgooglemaps://?center=\(latitude),\(longitude)&zoom=18"
        let browserURL = "https://www.google.com/maps/@\(latitude),\(longitude),18z"
        if let _ = URL(string: "comgooglemaps://"), UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!){
            UIApplication.shared.openURL(URL(string: "\(mapAppURL)")!)
        }else if UIApplication.shared.canOpenURL(URL(string: "\(browserURL)")!){
            UIApplication.shared.openURL(URL(string: "\(browserURL)")!)
        }
    }
    
    /// updating UI with given data in the content variable
    
    private func setUpInterface(){
        var format: String = ""
        self.intialiseVariables()
        guard let content = content else { return }
        
        if isFromMyEvents
        {
            setEventImagesWithURL()
            if let organisors = content.assignedmembers, !organisors.isEmpty{
                organisorsArray = organisors
            }
            if let sponsors = content.sponers, !sponsors.isEmpty{
                sponsorsArray = sponsors
            }
            format = "dd-MM-yyyy"
        }
        else
        {
            format = "dd-MM-yyyy"
            setEventImagesWithImage()
        }
        
        eventTitleLBL.text = content.title
        descriptionLBL.text = content.description
        locationLBL.text = content.address
        startDayNameLBL.text = "Start Date"//content.start_date dayNameFrom(content.start_date, format: format)
        endDayNameLBL.text = "End Date"//content.end_date dayNameFrom(content.end_date, format: format)
        startDateLBL.text = content.start_date//fullDateFrom(content.start_date, format: format)
        endDateLBL.text = content.end_date//fullDateFrom(content.end_date, format: format)
        switch content.category_id! {
        case 1:
            eventTypeLBL.text = "Conference"
            break
        case 2:
            eventTypeLBL.text = "Career Fair"
            break
        case 3:
            eventTypeLBL.text = "Exhibition"
            break
        default:
            eventTypeLBL.text = ""
        }
        
       // slotsAvailableLBL.text = content.center_name //"View Exhibition Centre"//content.slots_number?.description
        
        if content.ticket_type == 1
        {
            /// paid tickets
            entryTypePaidBTN.backgroundColor = blueColor
            entryTypePaidBTN.setTitleColor(.white, for: .normal)
            
            entryTypeFreeBTN.backgroundColor = .white
            entryTypeFreeBTN.setTitleColor(.darkGray, for: .normal)
            //heightTicketPriceView.constant = 70

        }
        else if content.ticket_type == 0
        {
            /// free tickets
            entryTypePaidBTN.backgroundColor = .white
            entryTypePaidBTN.setTitleColor(.darkGray, for: .normal)
            
            entryTypeFreeBTN.backgroundColor = blueColor
            entryTypeFreeBTN.setTitleColor(.white, for: .normal)
            //heightTicketPriceView.constant = 0

        }
        //ticketPriceLBL.text = content.price.unwrap

        organisersCV.reloadData()
        sponsorsCV.reloadData()
        
    }
    
    /// clearing all variables
    private func intialiseVariables(){
        organisorsArray.removeAll()
        sponsorsArray.removeAll()
        sliderImagesArray.removeAll()
    }
    
    /// set images to slideshow
    private func setEventImagesWithURL(){
        guard let eventImageURLs = content?.event_images, !eventImageURLs.isEmpty else { return }
        
        if let eventImagesURLArray = content?.event_images, !eventImagesURLArray.isEmpty {
            for itemURL in eventImagesURLArray {
                sliderImagesArray.append(KingfisherSource(urlString: baseURLImage + itemURL)!)
                imageLinkArray.append(itemURL)
            }
        }
        
        BaseThread.asyncMain {
            self.slideShowView.setImageInputs(self.sliderImagesArray)
             self.slideShowView.isUserInteractionEnabled = true
           
            
        }
        
    }
    
    private func setEventImagesWithImage(){
        guard let eventImages = content?.pics, !eventImages.isEmpty else { return }
        var sliderImages = [ImageSource]()
        for item in eventImages {
            sliderImages.append(ImageSource(image: item))
        }
        BaseThread.asyncMain {
            self.slideShowView.setImageInputs(sliderImages)
        }
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func viewCenterTapped(_ sender: UIButton) {
        
        bannerWithMessage(message: "Coming soon")
//        let vc = UIStoryboard(name: "SlotBookingWebview", bundle: nil).instantiateViewController(withIdentifier: "EXViewExhibitionCenterVC") as! EXViewExhibitionCenterVC
//        print("content?.center_url:",content!.center_url)
//        vc.URLexhibitionCenter = (content!.center_url)
//
//        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func uploadBTNTapped(_ sender: BaseButton)
    {
        if isFromMyEvents
        {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let announceVC = storyBoard.instantiateViewController(withIdentifier: "EXCreateAnnounceVC") as! EXCreateAnnounceVC
            announceVC.eventID = String(describing: content!.id!)
            announceVC.userType = .Admin
            self.present(announceVC, animated: true, completion: nil)
        }
        else
        {
            webCall()
        }
    }
    
    @IBAction func editBTNTapped(_ sender: UIButton)
    {
        if isFromMyEvents
        {
            let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXAdminEditEventVC") as! EXAdminEditEventVC
            nextVC.eventId = self.content!.id!
            self.present(nextVC, animated: true, completion: nil)
        }
        else
        {
            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        }
    }
}

// Speaker TableView Delegates
extension EXEventDetailVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if isFromMyEvents
        { return (self.content?.confernecedetails.count)!
        }
        else
        { return ConferenceModel.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! speakerConferenceCell
        
        if isFromMyEvents
        {
            cell.speakerName.text = self.content?.confernecedetails[indexPath.row].name
            let url = URL(string: "\(baseURL)\(imageURL)\(String(describing: self.content!.confernecedetails[indexPath.row].image))")
            cell.speakerImageView.kf.setImage(with: url, placeholder: UIImage(named: "noProfilePic.png"))
        }
        else
        {
            cell.speakerName.text = ConferenceModel[indexPath.row].speakerName
            cell.speakerImageView.image = ConferenceModel[indexPath.row].speakerImage
        }
        
        return cell
    } 
}



// CollectionView Delegates
extension EXEventDetailVC:UICollectionViewDelegate,UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == organisersCV
        {
            return isFromMyEvents ? organisorsArray.count : selectedOrganisors.count
           // return organisorsArray.count
        }
        else
        {
            return isFromMyEvents ? sponsorsArray.count : selectedSponsors.count
           // return sponsorsArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == organisersCV
        {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EXADOrganisorsCVC", for: indexPath) as? EXADOrganisorsCVC else { return UICollectionViewCell() }
            
            if isFromMyEvents
            {
                let organisor = organisorsArray[indexPath.item]
                cell.organisorNameLBL.text = organisor.username
                cell.organisorImage.kf.setImage(with: URL(string: baseURLImage + organisor.image!), placeholder:#imageLiteral(resourceName: "noProfilePic.png"))

            }
            else
            {
                let organisor = selectedOrganisors[indexPath.item]
                cell.organisorNameLBL.text = organisor.username
                cell.organisorImage.kf.setImage(with: URL(string: baseURLImage + organisor.image!), placeholder:#imageLiteral(resourceName: "noProfilePic.png"))
            }
        
          return cell
        }
        else
        {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EXADSponsersCVC", for: indexPath) as? EXADSponsersCVC else { return UICollectionViewCell() }
            
            if isFromMyEvents
            {
                let sponsor = sponsorsArray[indexPath.item]
                cell.sponsorImage.kf.setImage(with: URL(string: baseURLImage + sponsor.image!), placeholder:#imageLiteral(resourceName: "3.jpg") , options: nil, progressBlock: nil, completionHandler: nil)
            }
            else
            {
                let sponsor = selectedSponsors[indexPath.item]
                cell.sponsorImage.kf.setImage(with: URL(string: baseURLImage + sponsor.image!), placeholder:#imageLiteral(resourceName: "3.jpg") , options: nil, progressBlock: nil, completionHandler: nil)
            }

          return cell
        }
    }
    
    
}

extension EXEventDetailVC {
    
    private func webCall(){
        let url = URLAddEvent + Credentials.shared.id
        var parameter = addEventParameter
        debugPrint("addEventParameter-param",parameter as AnyObject)
  
        var imageParameter = [String:[UIImage]]()
        if let eventImages = parameter["pics"] as? [UIImage] {
            imageParameter["pics[]"] = eventImages
            //speakerImagesArray1.append(eventImages)
        } 
        
        parameter["category_id"] =  (addEventParameter["category_id"] as? Int)?.description
        parameter["entry_type"] = (addEventParameter["entry_type"] as? Int)?.description
        parameter["slots_num"] = 0.description//(addEventParameter["slots_num"] as? Int)?.description
        parameter["sponsers"] =  (addEventParameter["sponsers"] as? [Int])?.description
        parameter["assign"] = (addEventParameter["assign"] as? [Int])?.description
        parameter["price"] = 100.description
        
        parameter["center_id"] = (addEventParameter["center_id"] as? Int)?.description

        parameter["EventCategory"] = parameter["category_id"] as! String
        
        parameter.removeValue(forKey: "center_id")
        parameter.removeValue(forKey: "center_url")
        parameter.removeValue(forKey: "center_name")
        parameter.removeValue(forKey: "pics")

        print("Parameter-After",parameter)
        
        
        if parameter["category_id"] as! String == "1"
        {
            var speakerImagesArray = [UIImage]()
            var speakerNameArray = [String]()
            
            for items in ConferenceModel
            {
                speakerNameArray.append(items.speakerName)
                speakerImagesArray.append(items.speakerImage!)
            }
            
            parameter["speakername"] = String(describing: speakerNameArray)
            print("=================")
            print("Parameter::::",parameter)
            
            Webservices.postMethodMultiPartSpeakerImage(url: url, parameter: parameter, imageParameter: imageParameter, speakerImages: speakerImagesArray) { (isSucceeded, response) in
                
                if isSucceeded {
                    let message = response["Message"] as? String ?? "Event Added"
                    bannerWithMessage(message: message)
                    BaseThread.asyncMainDelay(seconds: 2, {
                        self.dismissVC()
                    })
                }
            }
        }
        else
        {
            print("=================")

            print("Parameter::::",parameter)

            Webservices.postMethodMultiPartImage(url: url, parameter: parameter, imageParameter: imageParameter)
            { (isSucceeded, response) in
                
                if isSucceeded {
                    let message = response["Message"] as? String ?? "Event Added"
                    bannerWithMessage(message: message)
                    BaseThread.asyncMainDelay(seconds: 2, {
                        self.dismissVC()
                    })
                }
            }
            
        }
    }
    
    private func dismissVC(){
   
        let myEventsVC = self.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController as! EXMyEventsVC
        myEventsVC.isRequiredReload = true
        self.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)

    }
    
}

// Mark: - Check whether the optional String is empty or not
protocol StringEmptyable{}
extension String: StringEmptyable{}
extension Optional where Wrapped: StringEmptyable {
    
    var unwrap: String{
        switch self {
        case .none:
            return ""
        case .some(let value):
            return value as! String
        }
    }
    
//    func unwrapped()-> String{
//        switch self {
//        case .none:
//            return ""
//        case .some(let value):
//            return value as! String
//        }
//    }
}




