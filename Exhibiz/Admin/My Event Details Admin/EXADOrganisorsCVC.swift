//
//  EXADOrganisorsCVC.swift
//  Exhibiz
//
//  Created by Appzoc on 28/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit

class EXADOrganisorsCVC: UICollectionViewCell {
    @IBOutlet var organisorImage: BaseImageView!
    @IBOutlet var organisorNameLBL: UILabel!
    
    @IBOutlet var selectionImage: BaseImageView!
    
}
