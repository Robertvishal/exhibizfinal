//
//  Show_Web_ViewController.swift
//  Exhibiz
//
//  Created by Appzoc on 09/04/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import Kingfisher

class Show_Web_ViewController: UIViewController {
    
    
 
    @IBOutlet var imageView_Show: UIImageView!
    
    @IBAction func but_Close(_ sender: UIButton) {
        
        
        
        //self.dismiss(animated: true, completion: nil)
        let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXMyEventsVC") as! EXMyEventsVC
        nextVC.isRequiredReload = true
        self.present(nextVC, animated: true, completion: nil)
        
    }
    var webUrl:String?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        imageView_Show.kf.setImage(with: URL(string: webUrl!), placeholder:#imageLiteral(resourceName: "noimage.jpg") , options: [.transition(.fade(0.25))], progressBlock: nil, completionHandler: nil)
        imageView_Show.contentMode = .scaleToFill

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
