//
//  EXAddAdressVC.swift
//  googleMapIntgrtn
//
//  Created by Janaina on 2/5/18.
//  Copyright © 2018 Janaina. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import MapKit
import DropDown

class EXAddAddressVC: UIViewController,CLLocationManagerDelegate,GMSMapViewDelegate,UITextFieldDelegate
{
    
    @IBOutlet weak var navigationBarView: UIView!
    @IBOutlet weak var backBTN: UIButton!
    @IBOutlet var mapViewDisplayView: UIView!
    @IBOutlet weak var searchBarView: UIView!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var searchBTN: UIButton!
    @IBOutlet weak var addLocationBTN: UIButton!
    
    var mapView1 = GMSMapView()
    var gmsFetcher: GMSAutocompleteFetcher!
    var marker = GMSMarker()
    
    //24.7241503,46.2620306
    var userSetLatitude = 24.7241503
    var userSetLongitude = 46.2620306
    var userSetAddress = String()
    private var userSetLocation = ""
    let dropDown = DropDown()
    var resultsArray = [String]()
    var placeIDArray = [String]()
    var indexOfPlaceID = 0
    
    final var addEventParameter = Json()
    final var selectedOrganisors = [ADOrganisorListModel]()
    final var selectedSponsors = [ADSponsorListModel]()
    
    var ConferenceModel = [ConferenceCellModel]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        debugPrint("addaddresszz",addEventParameter as AnyObject)
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        searchTF.delegate = self


//        userSetLatitude = 9.9971
//        userSetLongitude = 76.3028
        let openingLatitude = 9.9971
        let openingLongitude = 76.3028
        
        BaseThread.asyncMain {
            let camera = GMSCameraPosition.camera(withLatitude: openingLatitude, longitude: openingLongitude, zoom: 10)
            self.mapView1 = GMSMapView.map(withFrame: CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: self.mapViewDisplayView.frame.height), camera: camera)
            self.mapView1.delegate = self
            
            
            self.mapViewDisplayView.addSubview(self.mapView1)
            
            self.mapViewDisplayView.bringSubview(toFront: self.addLocationBTN)
            self.mapViewDisplayView.bringSubview(toFront: self.searchBarView)

            //self.view.addSubview(self.mapView1)
//            self.mapView1.superview?.addSubview(self.addLocationBTN)
//            self.mapView1.superview?.addSubview(self.searchBarView)

        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        BaseThread.asyncMain {
            self.mapView1.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: self.mapViewDisplayView.frame.height)
        }

    }
    @IBAction func editingDidBegin(_ sender: UITextField)
    { sender.text = ""
    }
    

    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
   func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D)
    {
        mapView1.clear()
        userSetLatitude = coordinate.latitude
        userSetLongitude = coordinate.longitude
       
        marker = GMSMarker(position: coordinate)
        let location = CLLocation(latitude: userSetLatitude, longitude: userSetLongitude)

        fetchAddress(location: location)
        {place, subAdministrativeArea,postalCode,country  in

            print(place)
            debugPrint(subAdministrativeArea)
            
            self.userSetAddress = place + "," + subAdministrativeArea + "," + postalCode + "," + country
            self.marker.snippet = self.userSetAddress
            self.userSetLocation = place
       }
   
        marker.map = mapView
        marker.appearAnimation = GMSMarkerAnimation.pop
        mapView1.selectedMarker = marker
        marker.icon = UIImage(named: "Location")
        
   }
    
   func fetchAddress(location: CLLocation, completion: @escaping (String, String, String, String) -> ())
   {
        CLGeocoder().reverseGeocodeLocation(location)
        { placemarks, error in
            if let error = error
            {
                print(error)
            } else if let place = placemarks?.first?.name,
                let subAdministrativeArea = placemarks?.first?.subAdministrativeArea,
                let postalCode = placemarks?.first?.postalCode,
                let country =  placemarks?.first?.country
                
            {
                completion(place, subAdministrativeArea, postalCode, country)
            }
        }
    }
    
    
    
    @IBAction func searchBTNTapped(_ sender: UIButton)
    {
      guard let text = searchTF.text, !text.isEmpty else
      {
         mapView1.clear()
         return
      }
     self.locationOnMap()
        mapView1.clear()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
      if let text = textField.text,
      let textRange = Range(range, in: text)
      {
               let updatedText = text.replacingCharacters(in: textRange,with: string)
               self.resultsArray.removeAll()
               self.placeIDArray.removeAll()
        
            gmsFetcher = GMSAutocompleteFetcher()
            gmsFetcher.delegate = self
            gmsFetcher?.sourceTextHasChanged(updatedText)
        }
        return true
    }
    

    func locationOnMap()
    {
      if self.placeIDArray.indices.contains(self.indexOfPlaceID)
      {
        GMSPlacesClient.shared().lookUpPlaceID(self.placeIDArray[self.indexOfPlaceID])
        { (place, error) in
            
            if let error = error
            {
                print("lookup place id query error: \(error.localizedDescription)")
                return
            }
            guard let place = place else
            {
                print("No place details for \(self.placeIDArray[self.indexOfPlaceID])")
                return
            }
            
            self.userSetLatitude = place.coordinate.latitude
            self.userSetLongitude = place.coordinate.longitude
            self.userSetAddress = place.formattedAddress!
            
            let locationNameArray = place.formattedAddress?.components(separatedBy: ",")
            self.userSetLocation = (locationNameArray?.isEmpty)! ? "" : locationNameArray![0]
            let camera = GMSCameraPosition.camera(withLatitude: self.userSetLatitude, longitude: self.userSetLongitude, zoom: 15)
            self.mapView1.camera = camera
            self.marker.position = CLLocationCoordinate2D(latitude: self.userSetLatitude, longitude: self.userSetLongitude)
            self.marker.snippet = self.userSetAddress
            self.marker.icon = UIImage(named: "Location")
            self.marker.map = self.mapView1
            self.marker.appearAnimation = GMSMarkerAnimation.pop
            self.mapView1.selectedMarker = self.marker
        
            self.addLocationBTN.isEnabled = true
        }
      }
    }
    
  
    @IBAction func addLocationBTNTapped(_ sender: UIButton)
    {
        print(self.userSetLatitude)
        print(self.userSetLongitude)
        print(self.userSetAddress)
        print(self.userSetLocation)
        
        userSetLatitude = 24.7241503
         userSetLongitude = 46.2620306
        
       // if validateData() {
            //24.7241503,46.2620306
            
            self.addEventParameter["address"] = self.userSetAddress
            self.addEventParameter["lat"] = 24.7241503.description//self.userSetLatitude.description
            self.addEventParameter["lon"] = 46.2620306.description//self.userSetLongitude.description
            self.addEventParameter["location"] = self.userSetLocation
            
            let previewContent = ADFuturePresentPastModel.getParameter(from: addEventParameter)
            debugPrint(previewContent as AnyObject)
            let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXEventDetailVC") as! EXEventDetailVC
            nextVC.addEventParameter = self.addEventParameter
            nextVC.selectedOrganisors = self.selectedOrganisors
            nextVC.selectedSponsors = self.selectedSponsors
            nextVC.content = previewContent
            nextVC.ConferenceModel = self.ConferenceModel
            self.present(nextVC, animated: true, completion: nil)
        //}

        debugPrint("addLocationBTNTapped",addEventParameter as AnyObject)
        
    }
    
    private func validateData() -> Bool{
        if userSetLongitude != 181.0 &&
           userSetLatitude != 181.0 &&
           !userSetAddress.isEmpty &&
           !userSetLocation.isEmpty {
            return true
        }else{
            bannerWith(title: "Mandatory", andMessage: "Choose a location")
            return false
        }
    }
}

extension EXAddAddressVC: GMSAutocompleteFetcherDelegate
{
    
    public func didAutocomplete(with predictions: [GMSAutocompletePrediction])
    {
        
        for prediction in predictions
        {
            
            if let prediction = prediction as GMSAutocompletePrediction!
            {
                self.resultsArray.append(prediction.attributedFullText.string)
                self.placeIDArray.append(prediction.placeID!)
                
            }
            dropDown.anchorView = searchTF
            dropDown.dataSource = resultsArray
            dropDown.direction = .bottom
            dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
            dropDown.selectionAction =
            {  index,item in
                self.indexOfPlaceID = index
                self.searchTF.text = item
                self.searchBTNTapped(self.searchBTN)
            }
            dropDown.show()
        }
    }
    
    func didFailAutocompleteWithError(_ error: Error)
    {
        print(error)
    }
}
