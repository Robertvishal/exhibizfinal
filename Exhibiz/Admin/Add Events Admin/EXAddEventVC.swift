//
//  EXAddEventVC.swift
//  Exhibiz
//
//  Created by Appzoc on 05/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import OpalImagePicker
import DatePickerDialog
import Kingfisher

class ConferenceCell: UITableViewCell
{
    @IBOutlet var speakerNameTF: UITextField!
    @IBOutlet var speakerImage: UIImageView!
    @IBOutlet var imageBTN: UIButton!
}

class ConferenceCellModel
{
    var speakerName = ""
    var speakerImage:UIImage?
}

class EXAddEventVC: UIViewController,OpalImagePickerControllerDelegate, EXAddOrganiserVCDelegate, SideOptionsDelegate
{
    @IBOutlet var eventTitleTextView: UITextViewFixed!
    @IBOutlet var descriptionTextView: UITextViewFixed!
    @IBOutlet var slotNoTF: UITextField!
    @IBOutlet var ticketPriceTF: UITextField!
    @IBOutlet var startDateLBL: UILabel!
    @IBOutlet var endDateLBL: UILabel!
    @IBOutlet var organisorsNameLBL: UILabel!
    @IBOutlet var eventPicsCV: UICollectionView!
    @IBOutlet var sponsersCV: UICollectionView!
    @IBOutlet var picturesCVHeight: NSLayoutConstraint!
    @IBOutlet var sponsersCVHeight: NSLayoutConstraint!
    @IBOutlet var freeTicketBTN: BaseButton!
    @IBOutlet var paidTicketBTN: BaseButton!
    
    @IBOutlet var minusBTN: UIButton!
    @IBOutlet var speakersTV: UITableView!
    
    
    @IBOutlet var ticketPriceViewHeight: NSLayoutConstraint!
    @IBOutlet var speakersHeight: NSLayoutConstraint!
    var ConferenceModel = [ConferenceCellModel()]
    // Data passing properties
    final var categoryId: Int = 1 // 1 for conference
    
    // Data stored properties
    private var selectedOrganisors = [ADOrganisorListModel]()
    private var selectedSponsors = [ADSponsorListModel]()
    private var selectedOrganisorsIds = [Int]()
    private var selectedSponsorsIds = [Int]()
    private var selectedSponsersImages = [UIImage]()
    private var selectedOrganisorsImages = [UIImage]()
    private var selectedPics = [UIImage]()
    private var datePicker = DatePickerDialog()
    private var isEventPicCV: Bool = false
    private var fullOrganisorsUpdated = [ADOrganisorListModel]()
    private var fullSponsorsUpdated  = [ADSponsorListModel]()
    private var organisorsName: String = ""
    private var fullOrganisors = [ADOrganisorListModel]()
    private var fullSponsors = [ADSponsorListModel]()
    private var entryType: Int = 1 // paid ticket and 0 for free
    private var startDate: Date = Date()
    
    private var exhibitionCenters: [PLSideOptionsModel] = []
    private var selectedExhibitionIndex: IndexPath?
    private var exhibitionsCenterId: Int = -1
    private var exhibitionCenterURL: String = ""
    
    private lazy var URLOrganisorsList = "ExhibizWeb/backend/api/get/orgainzers/"
    private lazy var URLSponsorsList = "ExhibizWeb/backend/api/get/sponsers/"

    private lazy var URLExhibitionCentersList = "ExhibizWeb/backend/api/get/exhibitioncenters/"

    override func viewDidLoad()
    {
        super.viewDidLoad()
        webCallOrganisors()
        webCallSponsors()
        //webCallExhibitionCenters()
        datePicker = DatePickerDialog(textColor: .black, buttonColor: blueColor, font: UIFont.systemFont(ofSize: 14), locale: nil, showCancelButton: true)
        
        if categoryId == 1
        { speakersHeight.constant = 120
          self.slotNoTF.text = "0"
          self.slotNoTF.isUserInteractionEnabled = false
        }
        else
        { speakersHeight.constant = 0
        }
        self.minusBTN.isHidden = true
        let center = ["Ernakulam","Alappuzha","Thiruvananthapuram","Kozhikkodu"]
        for i in 0...3 {
            exhibitionCenters.append(PLSideOptionsModel.init(sourceName: center[i], sourceID: i,sourceURL: center[i] + "_URL"))
        }
        
    }
    
    @IBAction func addSpeakerBTNTapped(_ sender: UIButton)
    {
        ConferenceModel.append(ConferenceCellModel())
        speakersTV.reloadData()
        speakersHeight.constant = speakersTV.contentSize.height + 40
        minusBTN.isHidden = false
        
        for item in ConferenceModel
        {
            print(item.speakerName)
            print(item.speakerImage as Any)
        }
    }
    
    @IBAction func minusSpeakerBTNTapped(_ sender: UIButton)
    {
        ConferenceModel.removeLast()
        speakersTV.reloadData()
        speakersHeight.constant = speakersTV.contentSize.height + 40
        if ConferenceModel.count == 1
        { minusBTN.isHidden = true
        }
    }
    
    @IBAction func addSpeakerImageTapped(_ sender: UIButton)
    {
        let imagePicker = OpalImagePickerController()
        imagePicker.maximumSelectionsAllowed = 1
        presentOpalImagePickerController(imagePicker, animated: true,
                                         select:
        { (assets) in
            self.ConferenceModel[sender.tag].speakerImage = getUIImage(asset: assets[0])
            imagePicker.dismiss(animated: true, completion: nil)
            self.speakersTV.reloadData()
        }, cancel:
        {
            //Cancel
        })
    }
    
    @IBAction func speakerTFEndEditing(_ sender: UITextField)
    {
        ConferenceModel[sender.tag].speakerName = sender.text!
    }
    
    
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage])
    {
        if isEventPicCV
        {
            selectedPics = selectedPics + images
            eventPicsCV.reloadData()
        }
        else
        {
            selectedSponsersImages = selectedSponsersImages + images
            sponsersCV.reloadData()
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func chooseExhibitionCenterTapped(_ sender: UIButton) {
        print("chooseExhibitionCenterTapped")
        if exhibitionCenters.isEmpty {
            webCallExhibitionCenters()
        }else {
            showSideOption(presentOn: self, delegate: self, source: exhibitionCenters)
        }

    }
    
    // Delegate from selection option (right side)
    func sideOptionSelected(data: PLSideOptionsModel, indexPath: IndexPath) {
        print("sideOptionSelected:",data.name,"ID",data.id)
        slotNoTF.text = data.name
        exhibitionsCenterId = data.id
        exhibitionCenterURL = data.url
    }
    
    
    @IBAction func startDateBTNTapped(_ sender: UIButton)
    {
        datePicker.show("Choose Start Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", defaultDate: Date(), minimumDate: Date(), maximumDate: nil, datePickerMode: .date) { (chosenDate) in
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy"
            if let date = chosenDate {
                self.startDate = date
                self.startDateLBL.text = dateFormatter.string(from: date)
            }
        }
    }
    
    @IBAction func endDateBTNTapped(_ sender: UIButton)
    {
        datePicker.show("Choose End Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", defaultDate: Date(), minimumDate: startDate, maximumDate: nil, datePickerMode: .date) { (chosenDate) in
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy"
            if let date = chosenDate {
                self.endDateLBL.text = dateFormatter.string(from: date)
            }
        }
    }
    
    @IBAction func freeTicketBTNTapped(_ sender: BaseButton)
    {
//        ticketPriceViewHeight.constant = 1
      freeTicketBTN.backgroundColor = blueColor
      freeTicketBTN.setTitleColor(UIColor.white, for: .normal)
      paidTicketBTN.backgroundColor = UIColor.white
      paidTicketBTN.setTitleColor(UIColor.darkGray, for: .normal)
        entryType = 0
    }
    
    @IBAction func paidTicketBTNTapped(_ sender: BaseButton)
    { //ticketPriceViewHeight.constant = 70
      paidTicketBTN.backgroundColor = blueColor
      paidTicketBTN.setTitleColor(UIColor.white, for: .normal)
      freeTicketBTN.backgroundColor = UIColor.white
      freeTicketBTN.setTitleColor(UIColor.darkGray, for: .normal)
        entryType = 1
    }
    
    @IBAction func addOrganiserBTNTapped(_ sender: UIButton)
    {
        if fullOrganisorsUpdated.isEmpty
        {
            webCallOrganisors()
        }
        else
        {
            let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXAddOrganiserVC") as! EXAddOrganiserVC
            nextVC.contentArray = fullOrganisorsUpdated
            nextVC.delegate = self
            nextVC.selectedOrganisors = self.selectedOrganisors
            nextVC.selectedOrganisorsImages = self.selectedOrganisorsImages
            self.present(nextVC, animated: true, completion: nil)
        }
    }
    
    func didSelectOrganisors(selectedList: [ADOrganisorListModel], correspondingImages images: [UIImage], from fullList: [ADOrganisorListModel])
    {
        fullOrganisorsUpdated.removeAll()
        selectedOrganisors.removeAll()
        selectedOrganisorsImages.removeAll()
        selectedOrganisorsIds.removeAll()
        var names = [String]()
        var index: Int = 0
        if !selectedList.isEmpty {
            for item in selectedList {
                self.selectedOrganisorsIds.append(item.id!)
                self.selectedOrganisorsImages.append(images[index])
                names.append(item.username!)
                index += 1
            }
            organisorsNameLBL.text = names.joined(separator: ", ")
        }else{
            organisorsNameLBL.text = ""
        }
        organisorsName = organisorsNameLBL.text ?? ""
        fullOrganisorsUpdated = fullList
        selectedOrganisors = selectedList
    }
    
    func didSelectSponsors(selectedList: [ADSponsorListModel], correspondingImages images: [UIImage], from fullList: [ADSponsorListModel]) {
        fullSponsorsUpdated.removeAll()
        selectedSponsors.removeAll()
        selectedSponsersImages.removeAll()
        selectedSponsorsIds.removeAll()
        var index: Int = 0
        if !selectedList.isEmpty {
            for item in selectedList {
                selectedSponsorsIds.append(item.id!)
                selectedSponsersImages.append(images[index])
                index += 1
            }
        }
        fullSponsorsUpdated = fullList
        selectedSponsors = selectedList
        sponsersCV.reloadData()
    }


    
    @IBAction func AddLocationBTNTapped(_ sender: BaseButton)
    {
        if validateData()
        {
            if categoryId == 1
            {
                if ConferenceModelValidation()
                {
                    let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXAddAddressVC") as! EXAddAddressVC
                    nextVC.addEventParameter = getParameter()
                    nextVC.selectedSponsors = self.selectedSponsors
                    nextVC.selectedOrganisors = self.selectedOrganisors
                    nextVC.ConferenceModel = self.ConferenceModel
                    self.present(nextVC, animated: true, completion: nil)
                }
                else
                {
                    bannerWithMessage(message: "Speakers name and image could not be left blank")
                }
            }
            else
            {
                let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXAddAddressVC") as! EXAddAddressVC
                nextVC.addEventParameter = getParameter()
                nextVC.selectedSponsors = self.selectedSponsors
                nextVC.selectedOrganisors = self.selectedOrganisors
                self.present(nextVC, animated: true, completion: nil)
            }
            
        }
       
    }
    
    func ConferenceModelValidation() -> Bool
    {
        for items in ConferenceModel
        { if items.speakerName == "" || items.speakerImage == nil
        {return false}
        }
        return true
    }
    
    private func getParameter() -> Json
    {
        var params = Json()
        params["title"] = self.eventTitleTextView.text
        params["description"] = self.descriptionTextView.text
        params["start_date"] = getfullDateInCommonFormat(self.startDateLBL.text, format: "MMM dd, yyyy")
        params["end_date"] = getfullDateInCommonFormat(self.endDateLBL.text, format: "MMM dd, yyyy")
        params["sponsers"] = self.selectedSponsorsIds
        params["assign"] = self.selectedOrganisorsIds
        params["sad_num"] = "123456"
        params["entry_type"] = self.entryType
        params["pics"] = self.selectedPics
        params["category_id"] = self.categoryId
        
//        params["slots_num"] = 1.description //Int(self.slotNoTF.text!)?.description
//        params["price"] = self.ticketPriceTF.text!

        params["center_id"] = exhibitionsCenterId
        params["center_name"] = self.slotNoTF.text!
        params["center_url"] = self.exhibitionCenterURL
        
        return params
    }
    
    private func validateData() -> Bool
    {
        let bannerTitle: String = "Mandatory!"
        
        if  BaseValidator.isNotEmpty(string: eventTitleTextView.text) &&
            BaseValidator.isNotEmpty(string: descriptionTextView.text) &&
            BaseValidator.isNotEmpty(string: slotNoTF.text) &&
            BaseValidator.isNotEmpty(string: startDateLBL.text) &&
            BaseValidator.isNotEmpty(string: endDateLBL.text) &&
            !selectedPics.isEmpty &&
            !selectedOrganisorsIds.isEmpty &&
            !selectedSponsorsIds.isEmpty
        {
            /*if entryType == 1
            {
                if !BaseValidator.isNotEmpty(string: ticketPriceTF.text)
                {
                    bannerWith(title: bannerTitle, andMessage: "Enter ticket price")
                    return false
                }
                else
                {
                    if 0 == Int(ticketPriceTF.text!) || !BaseValidator.isValid(digits: ticketPriceTF.text!) {
                        bannerWith(title: bannerTitle, andMessage: "Enter valid price other than zero")
                        return false
                    }
                }
            }
            if categoryId != 1
            {
                if 0 == Int(slotNoTF.text!) || !BaseValidator.isValid(digits: slotNoTF.text!)
                {
                    bannerWith(title: bannerTitle, andMessage: "Enter valid slot number other than zero")
                    return false
                }
                
                
            } */
            
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM dd, yyyy"
            let start = formatter.date(from: startDateLBL.text!)
            let end = formatter.date(from: endDateLBL.text!)
            
            if start!.compare(end!) == .orderedDescending {
                bannerWith(title: bannerTitle, andMessage: "End date is earlier than start date")
                return false
            }
            
            return true
        }
        else
        {
            if !BaseValidator.isNotEmpty(string: eventTitleTextView.text) {
                bannerWith(title: bannerTitle, andMessage: "Enter event title")
                return false
            }
            
            if !BaseValidator.isNotEmpty(string: descriptionTextView.text) {
                bannerWith(title: bannerTitle, andMessage: "Enter description")
                return false
            }
            
            if selectedPics.isEmpty {
                bannerWith(title: bannerTitle, andMessage: "Choose event images")
                return false
            }
            
            if !BaseValidator.isNotEmpty(string: slotNoTF.text) {
                //bannerWith(title: bannerTitle, andMessage: "Enter slot number")
                bannerWith(title: bannerTitle, andMessage: "Choose exhibition center")

                return false
            }else {
//                if 0 == Int(slotNoTF.text!) || !BaseValidator.isValid(digits: slotNoTF.text!) {
//                    bannerWithMessage(message: "Enter valid number other than zero")
//                    return false
//                }
            }
            
            if !BaseValidator.isNotEmpty(string: startDateLBL.text) {
                bannerWith(title: bannerTitle, andMessage: "Enter start date")
                return false
            }
            
            if !BaseValidator.isNotEmpty(string: endDateLBL.text) {
                bannerWith(title: bannerTitle, andMessage: "Enter end date")
                return false
            }
            
            if selectedOrganisorsIds.isEmpty {
                bannerWith(title: bannerTitle, andMessage: "Select organisors")
                return false
            }
            
            if selectedSponsorsIds.isEmpty {
                bannerWith(title: bannerTitle, andMessage: "Select sponsors")
                return false
            }
            
        }
        return false
    }

    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    
}

// Mark:- Handling TextViews
extension EXAddEventVC: UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView == eventTitleTextView &&  eventTitleTextView.text == "Event Title" && eventTitleTextView.text == nil
        { eventTitleTextView.text = ""
        }
        if textView == descriptionTextView && descriptionTextView.text == "Purpose of listing" && eventTitleTextView.text == nil
        { descriptionTextView.text = ""
        }
    }

    func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView == eventTitleTextView &&  eventTitleTextView.text == "" && eventTitleTextView.text == nil
        { eventTitleTextView.text = "Event Title"
        }
        if textView == descriptionTextView && descriptionTextView.text == "" && eventTitleTextView.text == nil
        { descriptionTextView.text = "Purpose of listing"
        }
    }
}

// CollectionView Delegates
extension EXAddEventVC:UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    { if collectionView == eventPicsCV
      { return selectedPics.count+1 }
      else
      { return selectedSponsersImages.count+1 }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let addEventCell = collectionView.dequeueReusableCell(withReuseIdentifier: "addEventCell", for: indexPath) as! addEventCell
        let addPhotoCell = collectionView.dequeueReusableCell(withReuseIdentifier: "addphotoCell", for: indexPath)
        picturesCVHeight.constant = eventPicsCV.contentSize.height
        sponsersCVHeight.constant = sponsersCV.contentSize.height
        if collectionView == eventPicsCV
        {
            /// photo adding cell
            if indexPath.item == 0
            { return addPhotoCell
            }
            else
            {
                addEventCell.cellImageView.image = selectedPics[indexPath.item-1]
                addEventCell.cellDeleteBTN.isHidden = false
                addEventCell.tag = indexPath.item - 1
                addEventCell.cellDeleteBTN.addTarget(self, action: #selector(deleteBTNTapped(_:)), for: .touchUpInside)
                return addEventCell
            }
        }
        else
        {
            if indexPath.item == 0
            { return addPhotoCell
            }
            else
            {
                addEventCell.cellImageView.image = selectedSponsersImages[indexPath.item-1]
                return addEventCell
            }
        }
    }
    
    @objc func deleteBTNTapped(_ sender: UIButton) {
        print(sender.tag)
        if !selectedPics.isEmpty && selectedPics.count > sender.tag {
            selectedPics.remove(at: sender.tag)
            eventPicsCV.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if indexPath.item == 0
        {
            if collectionView == eventPicsCV
            { isEventPicCV = true
              let imagePicker = OpalImagePickerController()
              imagePicker.imagePickerDelegate = self
              imagePicker.maximumSelectionsAllowed = (5 - selectedPics.count)
              
              if imagePicker.maximumSelectionsAllowed > 0
              { present(imagePicker, animated: true, completion: nil)
              }
              else
              { bannerWithMessage(message: "Maximum images selected")
              }
            }
            else
            {
                if fullSponsorsUpdated.isEmpty
                {
                    webCallSponsors()
                }
                else
                {
                    let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXAddOrganiserVC") as! EXAddOrganiserVC
                    nextVC.titleLBLString = "Sponsered By"
                    nextVC.contentType = .sponsor
                    nextVC.delegate = self
                    nextVC.contentArray = fullSponsorsUpdated
                    nextVC.selectedSponsors = self.selectedSponsors
                    nextVC.selectedSponsorsImages = self.selectedSponsersImages
                    self.present(nextVC, animated: true, completion: nil)
                }
            }
        }
        else
        {
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.bounds.width/3, height: (collectionView.bounds.width/3)*0.857)
    }

}

// Mark:- Handling web services
extension EXAddEventVC
{
    
    final func webCallOrganisors()
    {
        let userID = UserDefaults.standard.string(forKey: "UserID")!
        Webservices.getMethod(url: URLOrganisorsList + userID)
        { (isSucceeded, response) in
            guard let array:JsonArray = response["Data"] as? Array, !array.isEmpty else { return }
            self.fullOrganisors = ADOrganisorListModel.getValues(from: array)
            self.fullOrganisorsUpdated = self.fullOrganisors
        }

    }
    
    final func webCallSponsors()
    {
        let userID = UserDefaults.standard.string(forKey: "UserID")!
        Webservices.getMethod(url: URLSponsorsList + userID)
        { (isSucceeded, response) in
            guard let array:JsonArray = response["Data"] as? Array, !array.isEmpty else { return }
            self.fullSponsors = ADSponsorListModel.getValues(from: array)
            self.fullSponsorsUpdated = self.fullSponsors
        }

    }
    
    private func webCallExhibitionCenters(){
        let userID = UserDefaults.standard.string(forKey: "UserID")!
        Webservices.getMethod(url: URLExhibitionCentersList + userID)
        { (isSucceeded, response) in
            
            
            guard let array:JsonArray = response["Data"] as? JsonArray, !array.isEmpty else { return }
            
            self.exhibitionCenters = PLSideOptionsModel.getData(fromArray: array)

        }

    }

}

//Table View Delegates
extension EXAddEventVC:UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return ConferenceModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConferenceCell") as! ConferenceCell
        cell.speakerNameTF.tag = indexPath.row
        cell.speakerImage.tag = indexPath.row
        cell.imageBTN.tag = indexPath.row
        cell.speakerNameTF.text = ConferenceModel[indexPath.row].speakerName
        if ConferenceModel[indexPath.row].speakerImage != nil
        { cell.imageBTN.setTitle("", for: .normal)
        }
        cell.speakerImage.image = ConferenceModel[indexPath.row].speakerImage
        return cell
    }
}

class addEventCell: UICollectionViewCell
{
    @IBOutlet var cellImageView: UIImageView!
    @IBOutlet var cellDeleteBTN: UIButton!
}













