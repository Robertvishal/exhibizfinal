//
//  EXAddOrganiserVC.swift
//  Exhibiz
//
//  Created by Appzoc on 12/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit

protocol EXAddOrganiserVCDelegate {
    func didSelectOrganisors(selectedList: [ADOrganisorListModel], correspondingImages images: [UIImage], from fullList: [ADOrganisorListModel])
    func didSelectSponsors(selectedList: [ADSponsorListModel], correspondingImages images: [UIImage], from fullList: [ADSponsorListModel])
}

class EXAddOrganiserVC: UIViewController
{
    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var contentCV: UICollectionView!
    @IBOutlet var pageControl: UIPageControl!
    
    enum ExAddContentType: Int {
        case organisor
        case sponsor
    }
    
    // Data variables
    final var delegate: EXAddOrganiserVCDelegate?
    final var titleLBLString = "Assigned to"
    final var contentArray = JsonArray()
    final var selectedOrganisors = [ADOrganisorListModel]()
    final var selectedOrganisorsImages = [UIImage]()
    final var selectedSponsors = [ADSponsorListModel]()
    final var selectedSponsorsImages = [UIImage]()
    final var contentType: ExAddContentType = .organisor
    
    private var contentArrayOrganisor = [ADOrganisorListModel]()
    private var contentArraySponsor = [ADSponsorListModel]()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        pageControl.currentPage = 0
        titleLBL.text = titleLBLString
        contentCV.allowsMultipleSelection = true

        switch contentType
        {
        case .organisor:
            contentArrayOrganisor = contentArray as! [ADOrganisorListModel]
            let result = Double(Double(contentArrayOrganisor.count) / 6)
            pageControl.numberOfPages = Int(result.rounded(.up))
            break
        case .sponsor:
            contentArraySponsor = contentArray as! [ADSponsorListModel]
            let result = Double(Double(contentArraySponsor.count) / 6)
            pageControl.numberOfPages = Int(result.rounded(.up))
            break
        }
        
        contentCV.reloadData()
    }
    
    @IBAction func tickBTNTapped(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
        switch contentType {
        case .organisor:
            delegate?.didSelectOrganisors(selectedList: selectedOrganisors, correspondingImages: selectedOrganisorsImages, from: contentArrayOrganisor)
            break
        case .sponsor:
            delegate?.didSelectSponsors(selectedList: selectedSponsors, correspondingImages: selectedSponsorsImages, from: contentArraySponsor)
            break
        }
        
    }
    
}

// CollectionView Delegates
extension EXAddOrganiserVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        
        return contentType == .organisor ? contentArrayOrganisor.count : contentArraySponsor.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? EXADOrganisorsCVC else { return UICollectionViewCell() }
        
        switch contentType {
        case .organisor:
            let cellData = contentArrayOrganisor[indexPath.item]
            if cellData.isSelected
            {
                cell.selectionImage.isHidden = false
                cell.isSelected = true
                collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .centeredHorizontally)
            }
            else
            {
                cell.selectionImage.isHidden = true
                cell.isSelected = false
                collectionView.deselectItem(at: indexPath, animated: false)
            }
            cell.organisorNameLBL.text = cellData.username
            cell.organisorImage.kf.setImage(with: URL(string:baseURL+imageURL+cellData.image!), placeholder:#imageLiteral(resourceName: "noProfilePic.png") , options: nil, progressBlock: nil, completionHandler: nil)
            break
            
        case .sponsor:
            let cellData = contentArraySponsor[indexPath.item]
            if cellData.isSelected
            {
                cell.selectionImage.isHidden = false
                cell.isSelected = true
                collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .centeredHorizontally)
            }
            else
            {
                cell.selectionImage.isHidden = true
                cell.isSelected = false
                collectionView.deselectItem(at: indexPath, animated: false)
            }
            cell.organisorNameLBL.text = cellData.name
            cell.organisorImage.kf.setImage(with: URL(string: baseURL+imageURL+cellData.image!), placeholder:#imageLiteral(resourceName: "noProfilePic.png") , options: nil, progressBlock: nil, completionHandler: nil)
            break
            
        }
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: contentCV.bounds.width/3, height: contentCV.bounds.height/2)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? EXADOrganisorsCVC else { return }
        print("didSelectItemAt")
        switch contentType {
        case .organisor:
            contentArrayOrganisor[indexPath.item].isSelected = true
            cell.selectionImage.isHidden = false
            selectedOrganisors.append(contentArrayOrganisor[indexPath.item])
            selectedOrganisorsImages.append(cell.organisorImage.image!)
            break
        case .sponsor:
            contentArraySponsor[indexPath.item].isSelected = true
            cell.selectionImage.isHidden = false
            selectedSponsors.append(contentArraySponsor[indexPath.item])
            selectedSponsorsImages.append(cell.organisorImage.image!)
            break
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? EXADOrganisorsCVC else { return }
        print("deselected")
        switch contentType {
        case .organisor:
            contentArrayOrganisor[indexPath.item].isSelected = false
            cell.selectionImage.isHidden = true
            if let index = selectedOrganisors.index(where: { $0.id == contentArrayOrganisor[indexPath.item].id }) {
                selectedOrganisors.remove(at: index)
                selectedOrganisorsImages.remove(at: index)
            }
            break
        case .sponsor:
            contentArraySponsor[indexPath.item].isSelected = false
            cell.selectionImage.isHidden = true
            if let index = selectedSponsors.index(where: { $0.id == contentArraySponsor[indexPath.item].id }) {
                selectedSponsors.remove(at: index)
                selectedSponsorsImages.remove(at: index)
            }
            break
        }

    }
    
    //ScrollView delegate method
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let pageWidth = scrollView.frame.width
        let currentPage = Int((scrollView.contentOffset.x + pageWidth / 2) / pageWidth)
        self.pageControl.currentPage = currentPage
    }
    
    
}

//// Mark: - Handling web service
//extension EXAddOrganiserVC {
//    final func webCall(){
//        URLOrganisorsList = URLOrganisorsList + Credentials.shared.id
//        Webservices.getMethod(url: URLOrganisorsList) { (isSucceeded, response) in
//            guard let array:JsonArray = response["Data"] as? Array, !array.isEmpty else { return }
//
//            self.contentArray = ADOrganisorsModel.getValues(from: array)
//            self.contentCV.reloadData()
//        }
//    }
//}








