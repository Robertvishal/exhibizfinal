//
//  EXMyEventsVC.swift
//  Exhibiz
//
//  Created by Appzoc on 03/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import SideMenu
import Kingfisher
import DropDown

class EXMyEventsVC: UIViewController
{
    @IBOutlet var navigationTitleLBL: UILabel!
    @IBOutlet var slideView: UIView!
    @IBOutlet var liveBTN: UIButton!
    @IBOutlet var eventsTV: UITableView!
    @IBOutlet var noEventsLBL: UILabel!
    
    enum EventDisplayType: Int
    {
        case live
        case future
        case past
    }
    var selectedTab = 1
    
    // Data Variables
    final var isRequiredReload: Bool = false
    
    private var userType: String = "2"
    private var adminId: Int = 0
    private var sourceLive = [ADFuturePresentPastModel]()
    private var sourceFuture = [ADFuturePresentPastModel]()
    private var sourcePast = [ADFuturePresentPastModel]()
    private var displayEvent: EventDisplayType = .live
    private var contentArray = [ADFuturePresentPastModel]()

    lazy var URLMyEventsAdmin = "ExhibizWeb/backend/api/events/"

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.eventsTV.contentInset = UIEdgeInsetsMake(40, 0, 70, 0)
        
        setInitialFrame()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if isRequiredReload
        {
            getMyEventsWeb(parameter: ["type" : "2"])
            isRequiredReload = false
        }
    }

    
    @IBAction func liveBTNTapped(_ sender: UIButton)
    {
        displayEvent = .live
        animateView(sender: sender)
        eventsTV.reloadData()
    }
    
    @IBAction func futureBTNTapped(_ sender: UIButton)
    {
        displayEvent = .future
        animateView(sender: sender)
        eventsTV.reloadData()
    }
    
    @IBAction func pastBTNTapped(_ sender: UIButton)
    {
        displayEvent = .past
        animateView(sender: sender)
        eventsTV.reloadData()
    }
    
    @IBAction func sideMenuTapped(_ sender: UIButton)
    {
        let sideMenuVC = self.storyboard!.instantiateViewController(withIdentifier: "EXAdminSideMenuVC") as! EXAdminSideMenuVC
        
        let SideMenuController = UISideMenuNavigationController(rootViewController: sideMenuVC)
        SideMenuController.navigationBar.isHidden = true
        SideMenuController.leftSide = true

        self.present(SideMenuController, animated: true, completion: nil)
    }
    
    @IBAction func searchBTNTapped(_ sender: UIButton)
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let searchVC = storyBoard.instantiateViewController(withIdentifier: "EXSearchVC") as! EXSearchVC
        searchVC.contentArray = self.contentArray
        searchVC.fromUser = .Admin
        self.present(searchVC, animated: true, completion: nil)
    }
    
    @IBAction func filterBTNTapped(_ sender: UIButton)
    {
        let optionMenu = UIAlertController(title: "Choose Type of event", message: nil, preferredStyle: .actionSheet)
        
        let allEvents = UIAlertAction(title: "All Events", style: .default, handler:
        { (alert: UIAlertAction!) -> Void in
            self.getMyEventsWeb(parameter: ["type" : "2"])
        })
        
        let exhibition = UIAlertAction(title: "Exhibition", style: .default, handler:
        { (alert: UIAlertAction!) -> Void in
            self.getMyEventsWeb(parameter: ["type" : "2","eventtype":"3"])
        })
        
        let careerFair = UIAlertAction(title: "Career Fair", style: .default, handler:
        { (alert: UIAlertAction!) -> Void in
            self.getMyEventsWeb(parameter: ["type" : "2","eventtype":"2"])
        })
        
        let conference = UIAlertAction(title: "Conference", style: .default, handler:
        { (alert: UIAlertAction!) -> Void in
            self.getMyEventsWeb(parameter: ["type" : "2","eventtype":"1"])
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler:nil)
        
        optionMenu.addAction(exhibition)
        optionMenu.addAction(careerFair)
        optionMenu.addAction(conference)
        optionMenu.addAction(allEvents)
        optionMenu.addAction(cancel)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    @IBAction func addEventBTNTapped(_ sender: UIButton)
    { let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXEventSelectVC")
      self.present(nextVC, animated: true, completion: nil)
    }
}

// Commom Function
extension EXMyEventsVC
{
    func animateView(sender:UIButton)
    { UIView.animate(withDuration: 0.5, animations:{ self.slideView.frame = CGRect(x: sender.frame.minX, y: sender.frame.maxY, width: sender.frame.width,
                                                                                   height: sender.frame.height)})
    }
    
    func setInitialFrame()
    { self.slideView.frame = CGRect(x: liveBTN.frame.minX, y: liveBTN.frame.maxY, width: liveBTN.frame.width,
                                    height: liveBTN.frame.height)
    }
}

// TableView Delegates
extension EXMyEventsVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        contentArray.removeAll()
        switch displayEvent
        {
        case .live: contentArray = sourceLive
        case .future: contentArray = sourceFuture
        case .past: contentArray = sourcePast
        }
        if contentArray.isEmpty
        { noEventsLBL.isHidden = false
        }
        else
        { noEventsLBL.isHidden = true
        }
        return contentArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "EXADMyEventsTVC") as? EXADMyEventsTVC else { return UITableViewCell()}
        let cellData = contentArray[indexPath.row]
        cell.eventTitleLBL.text = cellData.title!
        cell.eventLocationLBL.text = cellData.address!
        guard let imageURLs = cellData.event_images, !imageURLs.isEmpty else { return cell }
        cell.eventImage.kf.setImage(with: URL(string: baseURLImage  + imageURLs[0]), placeholder:#imageLiteral(resourceName: "noimage.jpg") , options: [.transition(.fade(0.25))], progressBlock: nil, completionHandler: nil)
        cell.bannerTagBTN.setTitle(cellData.category!, for: .normal)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: false)
        let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXEventDetailVC") as! EXEventDetailVC
        nextVC.content = contentArray[indexPath.row]
        nextVC.isFromMyEvents = true
        self.present(nextVC, animated: true, completion: nil)
    }
    
    func selectedFromSearch(eventModel:ADFuturePresentPastModel)
    {
        let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXEventDetailVC") as! EXEventDetailVC
        nextVC.content = eventModel
        nextVC.isFromMyEvents = true
        self.present(nextVC, animated: true, completion: nil)
    }
}


// Mark:- Handling web services
extension EXMyEventsVC
{
    private func getMyEventsWeb(parameter:[String:String])
    {
        clearData()
//        var parameter = Json()
//        parameter.updateValue(userType, forKey: "type")
        
        Webservices.getMethodWith(url: URLMyEventsAdmin + Credentials.shared.id , parameter: parameter, CompletionHandler: { (isSucceeded, response) in
            if isSucceeded {
                guard let array:JsonArray = response["Data"] as? Array, let object: Json = array[0] as? Json, !object.isEmpty else { return }
                let data =  ADMyEventsModel.getValues(from: object)
                if let live = data.present, !live.isEmpty { self.sourceLive = live }
                if let future = data.future, !future.isEmpty { self.sourceFuture = future }
                if let past = data.past, !past.isEmpty { self.sourcePast = past }
                BaseThread.asyncMain {
                    self.eventsTV.reloadData()
                    self.eventsTV.scrollTableToTop()
                }
            }
            })
    }
    
    private func clearData()
    {
        sourceLive.removeAll()
        sourceFuture.removeAll()
        sourcePast.removeAll()
        contentArray.removeAll()
    }
    

}






