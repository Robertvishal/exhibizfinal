//
//  EXADMyEventsTVC.swift
//  Exhibiz
//
//  Created by Appzoc on 26/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit

class EXADMyEventsTVC: UITableViewCell
{

    @IBOutlet var eventTitleLBL: UILabel!
    @IBOutlet var eventLocationLBL: UILabel!
    @IBOutlet var eventImage: BaseImageView!
    @IBOutlet var bannerTagBTN: UIButton!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }

}
