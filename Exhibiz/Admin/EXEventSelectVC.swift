//
//  EXEventSelectVC.swift
//  Exhibiz
//
//  Created by Appzoc on 03/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit

class EXEventSelectVC: UIViewController
{
    @IBOutlet var conferenceBTN: BaseButton!
    @IBOutlet var careerBTN: BaseButton!
    @IBOutlet var exhibitionBTN: BaseButton!
    
    enum CategoryType: Int
    {
        case conference = 1
        case careerFair
        case exhibition
    }
    
    private var category: CategoryType = .exhibition
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        conferenceBTN.backgroundColor = mercuryColor
        careerBTN.backgroundColor = mercuryColor
        exhibitionBTN.backgroundColor = blueColor
    }
    
    @IBAction func nextBTNTapped(_ sender: UIButton)
    { let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXAddEventVC") as! EXAddEventVC
        nextVC.categoryId = self.category.rawValue
        print(self.category.rawValue)
      self.present(nextVC, animated: true, completion: nil)
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func conferenceBTNTapped(_ sender: UIButton)
    { conferenceBTN.backgroundColor = blueColor
      careerBTN.backgroundColor = mercuryColor
      exhibitionBTN.backgroundColor = mercuryColor
        category = .conference
    }
    @IBAction func careerBTNTapped(_ sender: UIButton)
    { conferenceBTN.backgroundColor = mercuryColor
      careerBTN.backgroundColor = blueColor
      exhibitionBTN.backgroundColor = mercuryColor
        category = .careerFair
    }
    @IBAction func exhibitionBTNTapped(_ sender: UIButton)
    { conferenceBTN.backgroundColor = mercuryColor
      careerBTN.backgroundColor = mercuryColor
      exhibitionBTN.backgroundColor = blueColor
      category = .exhibition
    }
}








