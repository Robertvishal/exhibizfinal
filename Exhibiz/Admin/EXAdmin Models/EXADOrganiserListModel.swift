//
//  EXADOrganiserModel.swift
//  Exhibiz
//
//  Created by Appzoc on 01/03/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import Foundation

// Mark:- Admin Organisor Model
public class ADOrganisorListModel {
    public var id : Int?
    public var username : String?
    public var email : String?
    public var image : String?
    public var isSelected: Bool = false
    
    ///  Returns an array of models based on given dictionary.
    class func getValues(from array: JsonArray) -> [ADOrganisorListModel]
    {
        var models:[ADOrganisorListModel] = []
        for item in array
        {
            models.append(ADOrganisorListModel(dictionary: item as! Json)!)
        }
        return models
    }
    
    
    ///Returns model from given dictionary.
    class func getValues(from object: Json) -> ADOrganisorListModel
    {
        return ADOrganisorListModel(dictionary: object)!
    }
    init?(dictionary: Json) {
        id = Int(dictionary["id"] as? String ?? "0")
        username = dictionary["username"] as? String ?? ""
        email = dictionary["email"] as? String ?? ""
        image = dictionary["image"] as? String ?? ""
    }
    
}
