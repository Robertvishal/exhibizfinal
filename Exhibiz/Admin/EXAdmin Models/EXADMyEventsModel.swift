//
//  EXADMyEventsModel.swift
//  Exhibiz
//
//  Created by Appzoc on 26/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import Foundation
import UIKit

// Mark:- Admin Events Model and sub models
public class ADMyEventsModel {
    public var future : [ADFuturePresentPastModel]?
    public var present : [ADFuturePresentPastModel]?
    public var past : [ADFuturePresentPastModel]?
    
    ///  Returns an array of models based on given dictionary.
    final class func getValues(from object: Json) -> ADMyEventsModel {
        return ADMyEventsModel(dictionary: object)!
    }
    
    ///  Constructs the object based on the given dictionary.
    required public init?(dictionary: Json) {
        
        if (dictionary["future"] != nil) { future = ADFuturePresentPastModel.getValues(from: dictionary["future"] as! JsonArray) }
        if (dictionary["present"] != nil) { present = ADFuturePresentPastModel.getValues(from: dictionary["present"] as! JsonArray) }
        if (dictionary["past"] != nil) { past = ADFuturePresentPastModel.getValues(from: dictionary["past"] as! JsonArray) }
    }
}


// Mark:- Common model for future, present and past in My Events Model
public class ADFuturePresentPastModel {
    /// common data for event details service result and for event details preview (with post event parameter)
    public var title : String?
    public var description : String?
    public var start_date : String?
    public var end_date : String?
    public var address : String?
    public var lat : String?
    public var lon : String?
    public var category_id : Int?
    public var location: String?
    /// data only for event details service result
    public var id : Int?
    public var admin_id : Int?
    public var slots_number : Int?
    public var slots : [ADSlotsModel]?
    public var sadadnumber : String?
    public var days : String?
    public var maximum_tickets : String?
    
    public var price : String?
    public var ticket_type : Int?
    public var created_at : String?
    public var category : String?
    public var event_images : [String]?
    public var assignedmembers : [ADAssignedMembersModel]?
    public var sponers : [ADSponersModel]?
    public var date : String?
    public var day : Int?
    
    /// data only for event details preview (with post event parameter)
    public var sponsers : [Int]?
    public var assign : [Int]?
    public var pics : [UIImage]?
    var confernecedetails = [conferenceDetailsModel]()
    
    public var center_name: String = ""
    //// this is for showing exhibition center in webview in details(both preview and details)
    public var center_url: String = "http://signitivebeta.com/exhibizWeb/slot_booking_beta"
    public var center_id: Int = 0
    
    ///  Returns an array of models based on given dictionary.
    final class func getValues(from array: JsonArray) -> [ADFuturePresentPastModel]
    {
        var models:[ADFuturePresentPastModel] = []
        for item in array
        {
            models.append(ADFuturePresentPastModel(dictionary: item as! Json)!)
        }
        return models
    }
    
    ///Returns model from given dictionary.
    final class func getValues(from object: Json) -> ADFuturePresentPastModel
    {
        return ADFuturePresentPastModel(dictionary: object)!
    }
    
    ///Returns model from given parameter.
    final class func getParameter(from object: Json) -> ADFuturePresentPastModel
    {
        
        return ADFuturePresentPastModel(parameter: object)!
    }
    
    ///  Constructs the object based on the given dictionary.
    required public init?(dictionary: Json) {
        id = Int(dictionary["id"] as? String ?? "0")
        admin_id = Int(dictionary["admin_id"] as? String ?? "0")
        title = dictionary["title"] as? String ?? ""
        description = dictionary["description"] as? String ?? ""
        category_id = Int(dictionary["category_id"] as? String ?? "0")
        lat = dictionary["lat"] as? String ?? ""
        lon = dictionary["lon"] as? String ?? ""
        address = dictionary["address"] as? String ?? ""
        
        slots_number = Int(dictionary["slots_number"] as? String ?? "0")
        
        center_name = dictionary["center_name"] as? String ?? ""
        //center_url = dictionary["center_url"] as? String ?? ""
        center_id = Int(dictionary["center_id"] as? String ?? "0")!
        
        if (dictionary["slots"] != nil) { slots = ADSlotsModel.getValues(from: dictionary["slots"] as! JsonArray) }
        sadadnumber = dictionary["sadadnumber"] as? String ?? ""
        start_date = dictionary["start_date"] as? String ?? ""
        end_date = dictionary["end_date"] as? String ?? ""
        days = dictionary["days"] as? String ?? ""
        maximum_tickets = dictionary["maximum_tickets"] as? String ?? ""
        price = dictionary["price"] as? String ?? ""
        ticket_type = Int(dictionary["ticket_type"] as? String ?? "2")
        created_at = dictionary["created_at"] as? String ?? ""
        category = dictionary["category"] as? String ?? ""
        if (dictionary["event_images"] != nil) { event_images = dictionary["event_images"] as? [String] ?? [] }
        if (dictionary["assignedmembers"] != nil) { assignedmembers = ADAssignedMembersModel.getValues(from: dictionary["assignedmembers"] as! JsonArray)}
        if (dictionary["sponers"] != nil) { sponers = ADSponersModel.getValues(from: dictionary["sponers"] as! JsonArray) }
        date = dictionary["date"] as? String ?? ""
        day = Int(dictionary["day"] as? String ?? "0")
        location = dictionary["location"] as? String ?? ""
        
        for item in dictionary["confernecedetails"] as? [[String:Any]] ?? [[String:Any]]()
        {
            let confernecedetails = conferenceDetailsModel()
            self.confernecedetails.append(confernecedetails.createModel(data: item))
        }
    }
    
    required public init?(parameter: Json) {
        category_id = parameter["category_id"] as? Int ?? 0
        slots_number = parameter["slots_num"] as? Int ?? 0
        ticket_type = parameter["entry_type"] as? Int ?? 2
        
        center_name = parameter["center_name"] as? String ?? ""
        center_url = parameter["center_url"] as? String ?? ""
        center_id = Int(parameter["center_id"] as? String ?? "0")!
        
        title = parameter["title"] as? String ?? ""
        description = parameter["description"] as? String ?? ""
        lat = parameter["lat"] as? String ?? ""
        lon = parameter["lon"] as? String ?? ""
        address = parameter["address"] as? String ?? ""
        location = parameter["location"] as? String ?? ""
        start_date = parameter["start_date"] as? String ?? ""
        end_date = parameter["end_date"] as? String ?? ""
        sadadnumber = parameter["sad_num"] as? String ?? ""
        price = parameter["price"] as? String ?? ""
        if (parameter["sponsers"] != nil) { sponsers = parameter["sponsers"] as? [Int] ?? [] }
        if (parameter["assign"] != nil) { assign = parameter["assign"] as? [Int] ?? [] }
        if (parameter["pics"] != nil) { pics = parameter["pics"] as? [UIImage] ?? [] }
        
    }

}


// Mark:- Admin slots model in My Events Model
public class ADSlotsModel {
    public var id : Int?
    public var event_id : Int?
    public var status : Int?
    public var booking_exhibitor_id : Int?

    public var size : Double?
    public var price : Double?
    public var image : String?
    public var created_at : String?
    //
    public var booking_image : String?
    public var address : String?
    public var description : String?
    public var company_name : String?
    public var user_id : String?
    public var user_email: String?
    
    public var isNotEmpty = false
    public var currentImage: UIImage?
    
    ///  Returns an array of models based on given dictionary.
    final class func getValues(from array: JsonArray) -> [ADSlotsModel] {
        var models:[ADSlotsModel] = []
        for item in array {
            models.append(ADSlotsModel(dictionary: item as! Json)!)
        }
        return models
    }
    
    ///  Returns model from given dictionary.
    final class func getValues(from object: Json) -> ADSlotsModel
    {
        return ADSlotsModel(dictionary: object)!
    }
    
    ///  Constructs the object based on the given dictionary.
    required public init?(dictionary: Json) {
        
        id = Int(dictionary["id"] as? String ?? "0")
        event_id = Int(dictionary["event_id"] as? String ?? "0")
        size = (dictionary["size"] as? String ?? "0").toDouble() ?? 0.0
        price = (dictionary["price"] as? String ?? "0").toDouble() ?? 0.0
        image = dictionary["image"] as? String ?? ""
        status = Int(dictionary["status"] as? String ?? "0")
        booking_exhibitor_id = Int(dictionary["booking_exhibitor_id"] as? String ?? "0")
        created_at = dictionary["created_at"] as? String ?? ""
        
        booking_image = dictionary["booking_image"] as? String ?? ""
        address = dictionary["address"] as? String ?? ""
        description = dictionary["description"] as? String ?? ""
        company_name = dictionary["company_name"] as? String ?? ""
        user_id = dictionary["user_id"] as? String ?? ""
        user_email = dictionary["user_email"] as? String ?? ""

    }
}


// Mark:- Admin assigned members model in My Events Model
public class ADAssignedMembersModel {
    public var id : Int?
    public var username : String?
    public var company_name : String?
    public var email : String?
    public var password : String?
    public var password2 : String?
    public var contactno : Int?
    public var interest : Int?
    public var image : String?
    public var created_at : String?
    
    ///  Returns an array of models based on given array.
    final class func getValues(from array: JsonArray) -> [ADAssignedMembersModel] {
        var models:[ADAssignedMembersModel] = []
        for item in array {
            models.append(ADAssignedMembersModel(dictionary: item as! Json)!)
        }
        return models
    }

    ///  Returns model from given dictionary.
    final class func getValues(from object: Json) -> ADAssignedMembersModel
    {
        return ADAssignedMembersModel(dictionary: object)!
    }
    
    ///  Constructs the object based on the given dictionary.
    required public init?(dictionary: Json) {
        
        id = Int(dictionary["id"] as? String ?? "0")
        username = dictionary["username"] as? String ?? ""
        company_name = dictionary["company_name"] as? String ?? ""
        email = dictionary["email"] as? String ?? ""
        password = dictionary["password"] as? String ?? ""
        password2 = dictionary["password2"] as? String ?? ""
        contactno = Int(dictionary["contactno"] as? String ?? "0")
        interest = Int(dictionary["interest"] as? String ?? "0")
        image = dictionary["image"] as? String ?? ""
        created_at = dictionary["created_at"] as? String ?? ""
    }
}


// Mark:- Admin sponers model in My Events Model
public class ADSponersModel {
    public var id : Int?
    public var username : String?
    public var company_name : String?
    public var email : String?
    public var password : String?
    public var password2 : String?
    public var contactno : Int?
    public var interest : Int?
    public var image : String?
    public var created_at : String?
    
    ///  Returns an array of models based on given array.
    final class func getValues(from array: JsonArray) -> [ADSponersModel] {
        var models:[ADSponersModel] = []
        for item in array {
            models.append(ADSponersModel(dictionary: item as! Json)!)
        }
        return models
    }

    ///  Returns model from given dictionary.
    final class func getValues(from object: Json) -> ADSponersModel
    {
        return ADSponersModel(dictionary: object)!
    }
    
    ///  Constructs the object based on the given dictionary.
    required public init?(dictionary: Json) {
        id = Int(dictionary["id"] as? String ?? "0")
        username = dictionary["username"] as? String ?? ""
        company_name = dictionary["company_name"] as? String ?? ""
        email = dictionary["email"] as? String ?? ""
        password = dictionary["password"] as? String ?? ""
        password2 = dictionary["password2"] as? String ?? ""
        contactno = Int(dictionary["contactno"] as? String ?? "0")
        interest = Int(dictionary["interest"] as? String ?? "0")
        image = dictionary["image"] as? String ?? ""
        created_at = dictionary["created_at"] as? String ?? ""
    }
}
