//
//  EXADSponsorModel.swift
//  Exhibiz
//
//  Created by Appzoc on 02/03/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import Foundation

// Mark:- Admin Sponsor Model
public class ADSponsorListModel {
    public var id : Int?
    public var name : String?
    public var admin_id : Int?
    public var image : String?
    public var created_at : String?
    public var isSelected: Bool = false
    
    ///  Returns an array of models based on given array of dictionary.
    class func getValues(from array: JsonArray) -> [ADSponsorListModel]
    {
        var models:[ADSponsorListModel] = []
        for item in array
        {
            models.append(ADSponsorListModel(dictionary: item as! Json)!)
        }
        return models
    }
    
    ///Returns model from given dictionary.
    class func getValues(from object: Json) -> ADSponsorListModel
    {
        return ADSponsorListModel(dictionary: object)!
    }
    
    init?(dictionary: Json) {
        id = Int(dictionary["id"] as? String ?? "0")
        name = dictionary["name"] as? String ?? ""
        created_at = dictionary["created_at"] as? String ?? ""
        image = dictionary["image"] as? String ?? ""
        admin_id = Int(dictionary["admin_id"] as? String ?? "0")
    }
    
}
