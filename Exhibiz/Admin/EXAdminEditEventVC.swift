//
//  EXAdminEditEventVC.swift
//  Exhibiz
//
//  Created by Appzoc on 04/06/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import OpalImagePicker
import DatePickerDialog
import Kingfisher

class EXAdminEditEventVC: UIViewController,OpalImagePickerControllerDelegate, EXAddOrganiserVCDelegate, SideOptionsDelegate
{
    @IBOutlet var eventTitleTextView: UITextViewFixed!
    @IBOutlet var descriptionTextView: UITextViewFixed!
    @IBOutlet var addressLBL: UILabel!
    @IBOutlet var slotNoTF: UITextField!
    @IBOutlet var startDateLBL: UILabel!
    @IBOutlet var endDateLBL: UILabel!
    @IBOutlet var eventPicsCV: UICollectionView!
    @IBOutlet var sponsersCV: UICollectionView!
    @IBOutlet var ticketPriceLBL: UITextField!
    @IBOutlet var picturesCVHeight: NSLayoutConstraint!
    @IBOutlet var sponsersCVHeight: NSLayoutConstraint!
    @IBOutlet var freeTicketBTN: BaseButton!
    @IBOutlet var paidTicketBTN: BaseButton!
    @IBOutlet var ticketPriceViewHeight: NSLayoutConstraint!
    @IBOutlet var organisorsNameLBL: UILabel!
    @IBOutlet var minusBTN: UIButton!
    @IBOutlet var speakersTV: UITableView!
    @IBOutlet var speakersHeight: NSLayoutConstraint!
    var ConferenceModel = [ConferenceCellModel]()
    
    // Data passing variables
    final var eventId: Int = 0
    
    // stored properties
    private var selectedSponsorsIds = [Int]()
    private var selectedSponsersImages = [UIImage]()
    private var selectedSponsors = [ADSponsorListModel]()
    private var fullSponsorsUpdated  = [ADSponsorListModel]()
    private var eventImages = [AnyObject]()
    private var allSelectedSponsors = [AnyObject]()
    private var datePicker = DatePickerDialog()
    private var entryType: Int = 1 // paid ticket and 0 for free
    private var startDate: Date = Date()
    var editEventParameter = Json()
    private var eventImagesURLArray = [String]()
    private var content: ADFuturePresentPastModel?
    private var selectedPics = [UIImage]()
    private var assignedMembers = [ADAssignedMembersModel]() // for data passing to other edit address vc
    
    private var exhibitionCenters: [PLSideOptionsModel] = []
    private var selectedExhibitionIndex: IndexPath?
    private var exhibitionsCenterId: Int = -1
    
    var fullOrganisors = [ADOrganisorListModel]()
    var organiserIDArray = [Int]()
    
    private lazy var URLEventsList = "ExhibizWeb/backend/api/events"
    private lazy var URLSponsorsList = "ExhibizWeb/backend/api/get/sponsers/"
    private lazy var URLDeleteEventImage = "ExhibizWeb/backend/api/delete/event_images"
    
    private lazy var URLExhibitionCentersList = "ExhibizWeb/backend/api/get/exhibitioncenters/"

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        datePicker = DatePickerDialog(textColor: .black, buttonColor: blueColor, font: UIFont.systemFont(ofSize: 14), locale: nil, showCancelButton: true)
        webGetCurrentEventDetails()
        webCallOrganisors()
        //webCallExhibitionCenters()

        
    }
    
    @IBAction func addSpeakerBTNTapped(_ sender: UIButton)
    {
        //        ConferenceModel.append(ConferenceCellModel())
        //        speakersTV.reloadData()
        //        speakersHeight.constant = speakersTV.contentSize.height + 40
        //        minusBTN.isHidden = false
        //
        //        for item in ConferenceModel
        //        {
        //            print(item.speakerName)
        //            print(item.speakerImage)
        //        }
    }
    
    @IBAction func minusSpeakerBTNTapped(_ sender: UIButton)
    {
        //        ConferenceModel.removeLast()
        //        speakersTV.reloadData()
        //        speakersHeight.constant = speakersTV.contentSize.height + 40
        //        if ConferenceModel.count == 1
        //        { minusBTN.isHidden = true
        //        }
    }
    
    @IBAction func addSpeakerImageTapped(_ sender: UIButton)
    {
        //        let imagePicker = OpalImagePickerController()
        //        imagePicker.maximumSelectionsAllowed = 1
        //        presentOpalImagePickerController(imagePicker, animated: true,
        //                                         select:
        //            { (assets) in
        //                self.ConferenceModel[sender.tag].speakerImage = getUIImage(asset: assets[0])
        //                imagePicker.dismiss(animated: true, completion: nil)
        //                self.speakersTV.reloadData()
        //        }, cancel:
        //            {
        //                //Cancel
        //        })
    }
    
    private func setUpInterface()
    {
        let format: String = "dd-MM-yyyy"
        guard let content = content else { return }
        clearData()
        
        // updating UI
        if let images = content.event_images, !images.isEmpty {
            eventImagesURLArray = images
            eventImages = images as [AnyObject]
        }
        if let sponsors = content.sponers, !sponsors.isEmpty{
            var sponsorObject = Json()
            for item in sponsors {
                sponsorObject["id"] = item.id
                sponsorObject["name"] = item.username
                sponsorObject["created_at"] = item.created_at
                sponsorObject["image"] = item.image
                sponsorObject["admin_id"] = ""
                let object = ADSponsorListModel.getValues(from: sponsorObject)
                object.isSelected = true
                selectedSponsors.append(object)
                selectedSponsorsIds.append(item.id!)
                
            }
            
        }
        
        eventTitleTextView.text = content.title
        descriptionTextView.text = content.description
        addressLBL.text = content.address
        
        slotNoTF.text = content.center_name //content.slots_number?.description
        exhibitionsCenterId = content.slots_number!
        
        startDateLBL.text = fullDateFrom(content.start_date, format: format)
        endDateLBL.text = fullDateFrom(content.end_date, format: format)
        
        var organiserString = String()
        for items in content.assignedmembers!
        {
            organiserString = organiserString + items.username! + ", "
        }
        organisorsNameLBL.text = organiserString
        
        if content.ticket_type == 1 {
            /// paid tickets
            paidTicketBTN.backgroundColor = blueColor
            paidTicketBTN.setTitleColor(.white, for: .normal)
            
            freeTicketBTN.backgroundColor = .white
            freeTicketBTN.setTitleColor(.darkGray, for: .normal)
            ticketPriceViewHeight.constant = 70
            
        }else if content.ticket_type == 0{
            /// free tickets
            paidTicketBTN.backgroundColor = .white
            paidTicketBTN.setTitleColor(.darkGray, for: .normal)
            
            freeTicketBTN.backgroundColor = blueColor
            freeTicketBTN.setTitleColor(.white, for: .normal)
            ticketPriceViewHeight.constant = 1
            
        }
        ticketPriceLBL.text = content.price.unwrap
        
        if let memmbers = content.assignedmembers {
            assignedMembers = memmbers
        }
        sponsersCV.reloadData()
        eventPicsCV.reloadData()
        
    }
    
    /// clearing all variables
    private func clearData()
    {
        selectedSponsors.removeAll()
        selectedSponsersImages.removeAll()
        selectedSponsorsIds.removeAll()
        eventImagesURLArray.removeAll()
        eventImages.removeAll()
    }
    
    @IBAction func startDateBTNTapped(_ sender: UIButton)
    {
        datePicker.show("Choose Start Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", defaultDate: Date(), minimumDate: Date(), maximumDate: nil, datePickerMode: .date) { (chosenDate) in
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy"
            if let date = chosenDate {
                self.startDate = date
                self.startDateLBL.text = dateFormatter.string(from: date)
            }
        }
    }
    
    @IBAction func endDateBTNTapped(_ sender: UIButton)
    {
        datePicker.show("Choose End Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", defaultDate: Date(), minimumDate: startDate, maximumDate: nil, datePickerMode: .date) { (chosenDate) in
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy"
            if let date = chosenDate {
                self.endDateLBL.text = dateFormatter.string(from: date)
            }
        }
    }
    
    
    @IBAction func exhibitionCenterTapped(_ sender: UIButton) {
        print("chooseExhibitionCenterTapped")
        if exhibitionCenters.isEmpty {
            webCallExhibitionCenters()
        }else {
            showSideOption(presentOn: self, delegate: self, source: exhibitionCenters)
        }

    }
    
    // Delegate from selection option (right side)
    func sideOptionSelected(data: PLSideOptionsModel, indexPath: IndexPath) {
        print("sideOptionSelected:",data.name,"ID",data.id)
        slotNoTF.text = data.name
        exhibitionsCenterId = data.id
        
    }

    
    
    @IBAction func ChangeLocationBTNTapped(_ sender: BaseButton)
    {
        let storyBoard = UIStoryboard(name: "EXOrganiser", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXOrgEditAddressVC") as! EXOrgEditAddressVC
        editEventParameter = getParameter()
        nextVC.editEventParameter = self.editEventParameter
        nextVC.isFromAdminEdit = true
        self.present(nextVC, animated: true, completion: nil)
    }
    
    func locationChanged()
    {
        content?.address = self.editEventParameter["address"] as? String ?? ""
        content?.lat = self.editEventParameter["lat"] as? String ?? ""
        content?.lon = self.editEventParameter["lon"] as? String ?? ""
        content?.location = self.editEventParameter["location"] as? String ?? ""
        addressLBL.text = content?.address
        print(self.editEventParameter)
    }
    
    
    @IBAction func addOrganiserBTNTapped(_ sender: UIButton)
    {
            let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXAddOrganiserVC") as! EXAddOrganiserVC
            nextVC.contentArray = fullOrganisors
            nextVC.delegate = self
//            nextVC.selectedOrganisors = self.selectedOrganisors
//            nextVC.selectedOrganisorsImages = self.selectedOrganisorsImages
            self.present(nextVC, animated: true, completion: nil)
    }
    
    
    @IBAction func saveBTNTapped(_ sender: BaseButton)
    {
        if validateData()
        {
            editEventParameter = getParameter()
            webPostEventDetails()
        }
    }
    
    private func getParameter() -> Json
    {
        var params = Json()
        selectedPics.removeAll()
        for item in eventImages {
            if let image = item as? UIImage {
                selectedPics.append(image)
            }
        }
        params["title"] = self.eventTitleTextView.text
        params["description"] = self.descriptionTextView.text
        params["start_date"] = getfullDateInCommonFormat(self.startDateLBL.text, format: "MMM dd, yyyy")
        params["end_date"] = getfullDateInCommonFormat(self.endDateLBL.text, format: "MMM dd, yyyy")
        params["sponsers"] = self.selectedSponsorsIds
        params["sad_num"] = "123456"
        params["pics"] = selectedPics // images for multipart posting
        params["assign"] = [Int]().description // not changing
        params["entry_type"] = content?.ticket_type // not changing
        params["category_id"] = content?.category_id // not changing
//        params["slots_num"] = Int(self.slotNoTF.text!) // not changing
//        params["price"] = self.ticketPriceLBL.text! // not changing
        params["EventCategory"] = content?.category // not chagning
        // for location showing on map
        params["address"] = content?.address
        params["lat"] = content?.lat
        params["lon"] = content?.lon
        params["location"] = content?.location
        
        params["slots_num"] = selectedExhibitionIndex//Int(self.slotNoTF.text!) -> selected exhibition center id
        params["center_name"] = self.slotNoTF.text!

        return params
    }
    
    private func validateData() -> Bool {
        let bannerTitle: String = "Mandatory!"
        
        if  BaseValidator.isNotEmpty(string: eventTitleTextView.text) &&
            BaseValidator.isNotEmpty(string: descriptionTextView.text) &&
            BaseValidator.isNotEmpty(string: startDateLBL.text) &&
            BaseValidator.isNotEmpty(string: endDateLBL.text) {
            
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM dd, yyyy"
            let start = formatter.date(from: startDateLBL.text!)
            let end = formatter.date(from: endDateLBL.text!)
            
            if start!.compare(end!) == .orderedDescending {
                bannerWith(title: bannerTitle, andMessage: "End date is earlier than start date")
                return false
            }
            
            return true
        }else {
            if !BaseValidator.isNotEmpty(string: eventTitleTextView.text) {
                bannerWith(title: bannerTitle, andMessage: "Enter event title")
                return false
            }
            
            if !BaseValidator.isNotEmpty(string: descriptionTextView.text) {
                bannerWith(title: bannerTitle, andMessage: "Enter description")
                return false
            }
            
            if !BaseValidator.isNotEmpty(string: startDateLBL.text) {
                bannerWith(title: bannerTitle, andMessage: "Enter start date")
                return false
            }
            
            if !BaseValidator.isNotEmpty(string: endDateLBL.text) {
                bannerWith(title: bannerTitle, andMessage: "Enter end date")
                return false
            }
        }
        return false
    }
    
    
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    {
        let listVC = self.presentingViewController?.presentingViewController as! EXMyEventsVC
        listVC.isRequiredReload = true
        listVC.dismiss(animated: true, completion: nil)
    }
    
    
    // delegate of image picker
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        eventImages = eventImages + images
        eventPicsCV.reloadData()
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    // Delegate of sponsor selection vc
    func didSelectSponsors(selectedList: [ADSponsorListModel], correspondingImages images: [UIImage], from fullList: [ADSponsorListModel]) {
        
        fullSponsorsUpdated.removeAll()
        selectedSponsors.removeAll()
        selectedSponsersImages.removeAll()
        selectedSponsorsIds.removeAll()
        var index: Int = 0
        if !selectedList.isEmpty {
            for item in selectedList {
                selectedSponsorsIds.append(item.id!)
                selectedSponsersImages.append(images[index])
                index += 1
            }
        }
        fullSponsorsUpdated = fullList
        selectedSponsors = selectedList
        sponsersCV.reloadData()
        
    }
    
    func didSelectOrganisors(selectedList: [ADOrganisorListModel], correspondingImages images: [UIImage], from fullList: [ADOrganisorListModel])
    {
        var organiserNameString = String()
        for item in selectedList
        {
            organiserIDArray.append(item.id!)
            organiserNameString = organiserNameString + ", \(item.username!)"
        }
        organisorsNameLBL.text = organiserNameString
        print(["abc":"def"].description)
        print("\(organiserIDArray.description)")
    }
}

// Mark:- Handling textfields
extension EXAdminEditEventVC: UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return true
    }
}

// CollectionView Delegates
extension EXAdminEditEventVC:UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    { if collectionView == eventPicsCV
    { return eventImages.count + 1 }
    else
    { return selectedSponsors.count+1 }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let addEventCell = collectionView.dequeueReusableCell(withReuseIdentifier: "addEventCell", for: indexPath) as! addEventCell
        let addPhotoCell = collectionView.dequeueReusableCell(withReuseIdentifier: "addphotoCell", for: indexPath)
        picturesCVHeight.constant = eventPicsCV.contentSize.height
        sponsersCVHeight.constant = sponsersCV.contentSize.height
        if collectionView == eventPicsCV
        {
            /// photo adding cell
            if indexPath.item == 0
            { return addPhotoCell
            }
            else
            {
                // load image from url array
                if eventImagesURLArray.count >= indexPath.item {
                    addEventCell.cellImageView.kf.setImage(with: URL(string: baseURLImage + (eventImages[indexPath.item - 1] as! String)), placeholder:#imageLiteral(resourceName: "3.jpg") , options: nil, progressBlock: nil, completionHandler: nil)
                }else{
                    // load image from image array
                    addEventCell.cellImageView.image = (eventImages[indexPath.item-1] as! UIImage)
                }
                addEventCell.cellDeleteBTN.isHidden = false
                addEventCell.cellDeleteBTN.tag = indexPath.item - 1
                addEventCell.cellDeleteBTN.addTarget(self, action: #selector(deleteBTNTapped(_:)), for: .touchUpInside)
                return addEventCell
            }
        }
        else
        {
            if indexPath.item == 0
            { return addPhotoCell
            }
            else
            {
                
                addEventCell.cellImageView.kf.setImage(with: URL(string: baseURLImage + selectedSponsors[indexPath.item - 1].image!), placeholder:#imageLiteral(resourceName: "3.jpg") , options: nil, progressBlock: nil, completionHandler:  { (imageRcvd, errorRecvd, cacheRcvd, urlRcvd) in
                    self.selectedSponsersImages.append(addEventCell.cellImageView.image!)
                })
                return addEventCell
            }
        }
    }
    
    @objc func deleteBTNTapped(_ sender: UIButton){
        // delete from url array
        if eventImagesURLArray.count >= sender.tag + 1 {
            if  eventImagesURLArray.count > sender.tag {
                postDeleteImageWeb(imagePath: eventImagesURLArray[sender.tag], index: sender.tag)
            }
        }else {
            // delete from image array
            if !eventImages.isEmpty && eventImages.count > sender.tag {
                eventImages.remove(at: sender.tag)
            }
        }
        eventPicsCV.reloadData()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if indexPath.item == 0
        {
            if collectionView == eventPicsCV
            {
                let imagePicker = OpalImagePickerController()
                imagePicker.imagePickerDelegate = self
                imagePicker.maximumSelectionsAllowed = 5 - eventImages.count
                present(imagePicker, animated: true, completion: nil)
            }
            else
            {
                webGetSponsorsList()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.bounds.width/3, height: (collectionView.bounds.width/3)*0.857)
    }
    
}

// Speaker Table View Delegates
extension EXAdminEditEventVC:UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return (self.content?.confernecedetails.count) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConferenceCell") as! ConferenceCell
        cell.speakerNameTF.tag = indexPath.row
        cell.speakerImage.tag = indexPath.row
        cell.imageBTN.tag = indexPath.row
        cell.speakerNameTF.text = content?.confernecedetails[indexPath.row].name
        cell.speakerImage.kf.setImage(with: URL(string: content!.confernecedetails[indexPath.row].image))
        print(content!.confernecedetails[indexPath.row].image)
        self.speakersHeight.constant = 40 + self.speakersTV.contentSize.height
        return cell
    }
}

// Mark:- Handling web services
extension EXAdminEditEventVC
{
    private func webGetCurrentEventDetails()
    {
        var parameter = Json()
        parameter.updateValue(self.eventId, forKey: "event_id")
        
        Webservices.getMethodWith(url: URLEventsList, parameter: parameter) { (isSucceeded, response) in
            
            guard let array:JsonArray = response["Data"] as? Array, let object: Json = array[0] as? Json, !object.isEmpty else { return }
            self.content = ADFuturePresentPastModel.getValues(from: object)
            
            if self.content?.category_id! == 1
            {
                for items in (self.content?.confernecedetails)!
                {
                    let model = ConferenceCellModel()
                    model.speakerName = items.name
                    
                    self.ConferenceModel.append(model)
                    let url = URL(string:baseURL+imageURL+items.image)
                    print(url!)
                    items.image = String(describing: url!)
                    self.speakersTV.reloadData()
                }
            }
            else
            { self.speakersHeight.constant = 0
            }
            self.setUpInterface()
        }
    }
    
    func webCallOrganisors()
    {
        let userID = UserDefaults.standard.string(forKey: "UserID")!
        Webservices.getMethod(url: "ExhibizWeb/backend/api/get/orgainzers/" + userID)
        { (isSucceeded, response) in
            guard let array:JsonArray = response["Data"] as? Array, !array.isEmpty else { return }
            self.fullOrganisors = ADOrganisorListModel.getValues(from: array)
        }
        
    }
    
    private func webGetSponsorsList()
    {
        Webservices.getMethod(url: URLSponsorsList + "\(String(describing: content!.admin_id!))")
        { (isSucceeded, response) in
            guard let array:JsonArray = response["Data"] as? Array, !array.isEmpty else { return }
            self.fullSponsorsUpdated = ADSponsorListModel.getValues(from: array)
            
            for item in self.selectedSponsors
            {
                if let index = self.fullSponsorsUpdated.index(where: { $0.id == item.id })
                {
                    self.fullSponsorsUpdated[index].isSelected = true
                }
            }
            
            BaseThread.asyncMain
            {
                let storyBoard = UIStoryboard(name: "EXAdmin", bundle: nil)
                let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXAddOrganiserVC") as! EXAddOrganiserVC
                nextVC.titleLBLString = "Sponsered By"
                nextVC.contentType = .sponsor
                nextVC.delegate = self
                nextVC.contentArray = self.fullSponsorsUpdated
                nextVC.selectedSponsors = self.selectedSponsors
                nextVC.selectedSponsorsImages = self.selectedSponsersImages
                self.present(nextVC, animated: true, completion: nil)
            }
        }
    }
    
    private func webCallExhibitionCenters(){
        let userID = UserDefaults.standard.string(forKey: "UserID")!
        Webservices.getMethod(url: URLExhibitionCentersList + userID)
        { (isSucceeded, response) in
            
            
            guard let array:JsonArray = response["Data"] as? JsonArray, !array.isEmpty else { return }
            
            self.exhibitionCenters = PLSideOptionsModel.getData(fromArray: array)
            
        }
        
    }

    private func postDeleteImageWeb(imagePath: String, index: Int) {
        var parameter = Json()
        parameter.updateValue(eventId, forKey: "event_id")
        parameter.updateValue(imagePath, forKey: "image")
        Webservices.postMethod(url: URLDeleteEventImage, parameter: parameter) { (isSucceeded, response) in
            self.eventImagesURLArray.remove(at: index)
            self.eventImages.remove(at: index)
            self.eventPicsCV.reloadData()
        }
    }
    
    
}


// Mark:- Handling web service
extension EXAdminEditEventVC
{
    private func webPostEventDetails()
    {
        var parameter = editEventParameter
        debugPrint("editEventParameter",parameter as AnyObject)
        
        var imageParameter = [String:[UIImage]]()
        if let eventImages = parameter["pics"] as? [UIImage]
        {
            imageParameter["pics[]"] = eventImages
        }
        parameter.removeValue(forKey: "pics")
        parameter["category_id"] =  (editEventParameter["category_id"] as? Int)?.description
        parameter["entry_type"] = (editEventParameter["entry_type"] as? Int)?.description
        parameter["slots_num"] = (editEventParameter["slots_num"] as? Int)?.description
        parameter["sponsers"] =  (editEventParameter["sponsers"] as? [Int])?.description
        
        if organiserIDArray.isEmpty
        {
            parameter["assign"] = (editEventParameter["assign"] as? [Int])?.description
        }
        else
        {
            parameter["assign"] = organiserIDArray.description
        }
        
        debugPrint("parameter:",parameter)
        
        Webservices.postMethodMultiPartImage(url: "ExhibizWeb/backend/api/edit/events/" + eventId.description, parameter: parameter, imageParameter: imageParameter) { (isSucceeded, response) in
            
            if isSucceeded {
                let message = response["Message"] as? String ?? "Event Updated"
                bannerWithMessage(message: message)
                BaseThread.asyncMainDelay(seconds: 2,
                {
                    self.dismissVC()
                })
            }
        }
        
    }
    
    private func dismissVC()
    {
        let myEventsVC = self.presentingViewController?.presentingViewController as! EXMyEventsVC
        myEventsVC.isRequiredReload = true
        self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
    }
}

