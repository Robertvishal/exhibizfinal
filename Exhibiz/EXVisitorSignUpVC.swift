//
//  EXVisitorSignUpVC.swift
//  Exhibiz
//
//  Created by Appzoc on 22/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import BRYXBanner
import CountryPickerView



class EXVisitorSignUpVC: UIViewController, CountryPickerViewDelegate, CountryPickerViewDataSource{

    @IBOutlet var usernameTF: UITextField!
    @IBOutlet var passwordTF: UITextField!
    @IBOutlet var emailTF: UITextField!
    @IBOutlet var mobileNoTF: UITextField!
    
    @IBOutlet var interestBTN: BaseButton!
    @IBOutlet var userTypeBTN: BaseButton!
    
    var selectedInterest = 1
    var userType = UserType.Visitor
    
    var countryPhoneCode = ""
var cpv = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    
    
    
    @IBOutlet var conView: CountryPickerView!
    
    
    
    
    
    
    @IBOutlet var countryBtn: UIButton!
    //@IBOutlet weak var ContryPickerInView: CountryPickerView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
      
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        setContryList()
        super.viewWillAppear(true)
        
        conView.delegate = self
        conView.dataSource = self
    
    
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nextBTNTapped(_ sender: UIButton)
    {
        if isValid()
        { registerWebCall()
        }
    }
    
    @IBAction func CountryCodeTap(_ sender: UIButton) {
        
        
        self.conView.showCountriesList(from: self)
        
    }
    
    func setContryList()
     {
    
    
    
    
    let w = self.cpv.frame.width
    let h = self.cpv.frame.height
    cpv = CountryPickerView(frame: CGRect(x: 0, y: 0, width: w, height: h))
    
    
    conView.frame = CGRect(x: 0, y: 0, width: 0, height: 0)

    countryPhoneCode = self.cpv.selectedCountry.phoneCode
    
        countryBtn.setTitle(countryPhoneCode, for: .normal)
    
    }
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
    
    
    let message = "Name: \(country.name) \nCode: \(country.code) \nPhone: \(country.phoneCode)"
    
    
    debugPrint(message)
    print(country,message)
    
    countryPhoneCode = country.phoneCode
         countryBtn.setTitle(country.phoneCode, for: .normal)
    
    
    }
    
    

    
    
    
    @IBAction func userTypeBTNTapped(_ sender: BaseButton)
    {
        let optionMenu = UIAlertController(title: "Choose user type", message: nil, preferredStyle: .actionSheet)
        
        let exhibitor = UIAlertAction(title: "Exhibitor", style: .default, handler:
        { (alert: UIAlertAction!) -> Void in
           self.userType = .Exhibitor
           self.interestBTN.isHidden = true
           self.selectedInterest = 0
           sender.setTitle("Exhibitor", for: .normal)
           self.usernameTF.placeholder = "Company Name"
        })
        
        let visitor = UIAlertAction(title: "Visitor", style: .default, handler:
        { (alert: UIAlertAction!) -> Void in
            self.userType = .Visitor
            self.interestBTN.isHidden = false
            sender.setTitle("Visitor", for: .normal)
            self.usernameTF.placeholder = "Name"
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        optionMenu.addAction(exhibitor)
        optionMenu.addAction(visitor)
        optionMenu.addAction(cancel)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    @IBAction func interestBTNTapped(_ sender: UIButton)
    { let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXVisitorInterestVC") as! EXVisitorInterestVC
      nextVC.selectedInterest = selectedInterest
      self.present(nextVC, animated: true, completion: nil)
    }
    
    func isValid() -> Bool
    { if usernameTF.text!.isEmpty
      { bannerWithMessage(message: "Enter Name")
        return false
      }
      else if passwordTF.text!.isEmpty
      { bannerWithMessage(message: "Enter Password")
        return false
      }
      else if emailTF.text!.isEmpty
      { bannerWithMessage(message: "Enter Email")
        return false
      }
      else if !BaseValidator.isValid(email: emailTF.text!)
      { bannerWithMessage(message: "Enter valid Email")
        return false
      }
      else if mobileNoTF.text!.isEmpty
      { bannerWithMessage(message: "Enter Mobile Number")
        return false
      }
      else if !BaseValidator.isValid(digits: mobileNoTF.text!)
      { bannerWithMessage(message: "Enter valid Mobile Number")
        return false
      }
      else if userType == .Visitor && selectedInterest == 0
      { bannerWithMessage(message: "Choose your area of interest")
        return false
      }
    else if countryPhoneCode == ""
    { bannerWithMessage(message: "Choose your country code")
        return false
    }
      else
      { return true
      }
    }
    
    func registerWebCall()
    {
        var parameter = [String:Any]()
        parameter["email"] = emailTF.text!
        parameter["password"] = passwordTF.text!
        parameter["device_id"] = UserDefaults.standard.string(forKey: "DeviceToken")
        parameter["os"] = "I"
        parameter["phone"] = mobileNoTF.text!
        parameter["signuptype"] = "0"
        
        if userType == .Visitor
        {
            parameter["interest"] = selectedInterest
            parameter["username"] = usernameTF.text!
            parameter["usertype"] = "[5]"
        }
        else
        {
            parameter["company_name"] = usernameTF.text!
            parameter["usertype"] = "[4]"
        }
        
        let mo = countryPhoneCode + mobileNoTF.text!
        
        let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXVisitorOTPVC") as! EXVisitorOTPVC
        nextVC.parameterToPass = parameter
        nextVC.mob = mo
        
        self.present(nextVC, animated: true, completion: nil)
        //otp

//        Webservices.postMethod(url: registrationApi, parameter: parameter)
//        { (isFetched, resultData) in
//            if isFetched
//            {
//                bannerWithMessage(message: resultData["Message"] as? String ?? "")
//                self.dismiss(animated: true, completion: nil)
//            }
//        }
    }
}

extension EXVisitorSignUpVC : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if  textField == mobileNoTF
        {
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            
            
            
            
            return string == numberFiltered
        }
        
        return true
        
    }

}

