//
//  EXViewExhibitionCenterVC.swift
//  Exhibiz
//
//  Created by Robert on 19/11/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit

class EXViewExhibitionCenterVC: UIViewController,UIWebViewDelegate {

    
    @IBOutlet weak var exhibitionWebView: UIWebView!
    
    final var URLexhibitionCenter: String = ""
    
    private let activity = ActivityIndicator()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("URLexhibitionCenter:",URLexhibitionCenter)
        if !URLexhibitionCenter.isEmpty {
            let url = URL(string: URLexhibitionCenter)
            let urlRequest = URLRequest(url: url!)
            exhibitionWebView.loadRequest(urlRequest)

        }else {
            let alert = UIAlertController(title: "Network Error", message: "Could not load exhibition center", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Cancel", style: .default, handler:
            { (alert) in
                self.dismiss(animated: true, completion: nil)
            })
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)

        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool
    {
        let urlString = String(describing: request.url!)
        print("urlString:",urlString)
        return true
    }
    
    func webViewDidStartLoad(_ webView: UIWebView)
    {
        activity.start()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        activity.stop()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        activity.stop()
        let alert = UIAlertController(title: "Network Error", message: "Error occured while connecting", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Cancel", style: .default, handler:
        { (alert) in
            self.dismiss(animated: true, completion: nil)
        })
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }

}


