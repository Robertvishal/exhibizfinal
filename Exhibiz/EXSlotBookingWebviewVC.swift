//
//  EXSlotBookingWebviewVC.swift
//  Exhibiz
//
//  Created by Akhil Chandran on 04/11/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit


class EXSlotBookingWebviewVC: UIViewController,UIWebViewDelegate {
    
    @IBOutlet weak var SlotWB: UIWebView!
    
    let activity = ActivityIndicator()
    
    final var eventDetails = EventListModel()
    final var content: ADFuturePresentPastModel?
    final var isFromOrganisor: Bool = false // if from organizor uses- content othervice -> eventDetails
    
    var bookingURL = "http://signitivebeta.com/exhibizWeb/slot_booking_beta"

    private let bookingIdExtractor: String = "id="
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if isFromOrganisor {
            bookingURL = (content?.center_url)!
        }else {
            bookingURL = eventDetails.slot_booking_url
        }
        
        if !eventDetails.slot_booking_url.isEmpty {
            let url = URL(string: eventDetails.slot_booking_url)
            let urlRequest = URLRequest(url: url!)
            SlotWB.loadRequest(urlRequest)

        }else {
            bannerWith(title: "Error !", andMessage: "Can't Load Page")
        }
        
        let tapRecognizer = UITapGestureRecognizer(target: view, action: Selector(("handleSingleTap:")))
        tapRecognizer.numberOfTapsRequired = 1
         SlotWB.addGestureRecognizer(tapRecognizer)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func BackTap(_ sender: UIButton) {
        
        self.dismiss(animated: false, completion: nil)
        
    }

    
    @objc func handleSingleTap(recognizer: UITapGestureRecognizer) {
        //Do something here with the gesture
        
        print("done")
        
        //segueToSlotDetailsVC()
        
    }
    
    
    @IBAction func BackTapped(_ sender: UIButton) {
        
        
    }

    
    func webViewDidStartLoad(_ webView: UIWebView)
    {
        activity.start()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        activity.stop()

//        guard let currentURL = webView.request?.url, currentURL != URL(string: eventDetails.slot_booking_url) else {
//            return
//        }
//        let urlString = currentURL.absoluteString
//        print("urlString:",urlString)
//
//        var slotId: Int = 0
//        if urlString.contains(bookingIdExtractor) {
//            slotId = Int(urlString.components(separatedBy: bookingIdExtractor).last ?? "0") ?? 0
//        }
//        
//        if isFromOrganisor {
//            organisorHandler(slotId: slotId)
//        }else {
//            exhibitorHandler(slotId: slotId)
//        }

    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool
    {
        //id
        var slotId: Int = 0
        let urlString = String(describing: request.url!)
        print(urlString)
        if urlString.contains("?id=")
        {
            if urlString.contains(bookingIdExtractor) {
                slotId = Int(urlString.components(separatedBy: bookingIdExtractor).last ?? "0") ?? 0
                
                if isFromOrganisor {
                    organisorHandler(slotId: slotId)
                }else {
                    exhibitorHandler(slotId: slotId)
                }
            }
            
           
        }
        
        return true
    }
    
    
    private func exhibitorHandler(slotId: Int) {
        
        //guard let slotInfo = eventDetails.slots.filter({$0.id == slotId }).first else { return }
        if eventDetails.slots.isEmpty {
            bannerWithMessage(message: "Slots Empty")

            return
        }
        print("arraylimit-exhibitorHandler",eventDetails.slots.count)
        let arrayLimit = eventDetails.slots.count - 1
        let slotInfo = eventDetails.slots[0.randomInt(max: arrayLimit)]//.filter({$0.id == slotId }).first else { return }
        
        
        BaseThread.asyncMain {
            let storyBoard = UIStoryboard(name: "EXExhibitor", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXExSlotDetailVC") as! EXExSlotDetailVC
            nextVC.slotDetails = slotInfo
            nextVC.eventTitle = self.eventDetails.title
            nextVC.eventDescription = self.eventDetails.description
            self.present(nextVC, animated: true, completion: nil)
            
        }
        return
        
        if slotInfo.status == 0 //eventDetails.slots[indexPath.item].status != 0
        {
            bannerWithMessage(message: "This slot is already booked")
        }
        else if (self.eventDetails.categoryID != 2) && (slotInfo.price == "" || slotInfo.size == "")
            //        else if (self.eventDetails.categoryID != 2) && (eventDetails.slots[indexPath.item].price == "" || eventDetails.slots[indexPath.item].size == "")

        {
            bannerWithMessage(message: "Slot details not added for booking")
        }
        else
        {
            BaseThread.asyncMain {
                let storyBoard = UIStoryboard(name: "EXExhibitor", bundle: nil)
                let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXExSlotDetailVC") as! EXExSlotDetailVC
                nextVC.slotDetails = slotInfo
                nextVC.eventTitle = self.eventDetails.title
                nextVC.eventDescription = self.eventDetails.description
                self.present(nextVC, animated: true, completion: nil)

            }
            
        }

    }
    
    
    
    
    private func organisorHandler(slotId: Int) {
        

      
        
        
        
        guard let content = content else { return }
        //guard let slotInfo = content.slots?.filter({$0.id == slotId }).first else { return }

        
        if content.slots!.isEmpty {
            bannerWithMessage(message: "Slots Empty")

            return
        }
        
        print("arraylimit-organisorHandler",content.slots!.count)

        let arrayLimit = content.slots!.count - 1
        let slotInfo = content.slots![0.randomInt(max: arrayLimit)]
        
        
        BaseThread.asyncMain {
            let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXBookedSlotDetailVC") as! EXBookedSlotDetailVC
            //nextVC.slotDetail = (self.content?.slots?[indexPath.item])!
            nextVC.address = self.eventDetails.address //self.content!.address!
            nextVC.startDate = self.eventDetails.startDate //self.content!.start_date!
            nextVC.endDate = self.eventDetails.endDate //self.content!.end_date!
            nextVC.eventType = self.eventDetails.category //self.content!.category!
            
            self.present(nextVC, animated: true, completion: nil)
        }
        return
        
        
        if self.content?.category_id! == 2 && (slotInfo.status == 0)
        {
            BaseThread.asyncMain {
                let storyBoard = UIStoryboard(name: "EXOrganiser", bundle: nil)
                let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXBookSpaceVC") as! EXBookSpaceVC
                
                let slotDetail = SlotModel()
                slotDetail.image = slotInfo.image!//(self.content?.slots?[indexPath.item].image)!
                slotDetail.id = slotInfo.id! //(self.content?.slots?[indexPath.item].id)!
                slotDetail.size = slotInfo.size!.description//String((self.content?.slots?[indexPath.item].size)!)
                slotDetail.price = slotInfo.price!.description//String((self.content?.slots?[indexPath.item].price)!)
                
                nextVC.slotDetails = slotDetail
                nextVC.eventTitle = self.eventDetails.title //self.content!.title!
                nextVC.eventDescription = self.eventDetails.description //self.content!.description!

                self.present(nextVC, animated: true, completion: nil)
            }
        }
        else if slotInfo.status != 0//(self.content?.slots?[indexPath.item].status != 0)
        {
            BaseThread.asyncMain {
                let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXBookedSlotDetailVC") as! EXBookedSlotDetailVC
                //nextVC.slotDetail = (self.content?.slots?[indexPath.item])!
                nextVC.address = self.eventDetails.address //self.content!.address!
                nextVC.startDate = self.eventDetails.startDate //self.content!.start_date!
                nextVC.endDate = self.eventDetails.endDate //self.content!.end_date!
                nextVC.eventType = self.eventDetails.category //self.content!.category!

                self.present(nextVC, animated: true, completion: nil)
            }
        }
        else if slotInfo.price == 0.0 || slotInfo.size == 0.0 //self.content?.slots?[indexPath.item].price == 0.0 || self.content?.slots?[indexPath.item].size == 0.0
        {
            
            bannerWithMessage(message: "Slot details not added for booking")
        }
        else
        {
            BaseThread.asyncMain {
                let storyBoard = UIStoryboard(name: "EXOrganiser", bundle: nil)
                let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXBookSpaceVC") as! EXBookSpaceVC
                
                let slotDetail = SlotModel()
                slotDetail.image = slotInfo.image!//(self.content?.slots?[indexPath.item].image)!
                slotDetail.id = slotInfo.id!//(self.content?.slots?[indexPath.item].id)!
                slotDetail.size = slotInfo.size!.description//String((self.content?.slots?[indexPath.item].size)!)
                slotDetail.price = slotInfo.price!.description//String((self.content?.slots?[indexPath.item].price)!)
                
                nextVC.slotDetails = slotDetail
                nextVC.eventTitle = self.eventDetails.title//self.content!.title!
                nextVC.eventDescription = self.eventDetails.description //self.content!.description!

                self.present(nextVC, animated: true, completion: nil)
            }
        }
        
    }

    
    
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        activity.stop()
        let alert = UIAlertController(title: "Network Error", message: "Error occured while connecting", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Cancel", style: .default, handler:
        { (alert) in
            self.dismiss(animated: true, completion: nil)
        })
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    private func segueToSlotDetailsVC() {
        BaseThread.asyncMain {
            let storyBoard = UIStoryboard(name: "EXExhibitor", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXExSlotDetailVC") as! EXExSlotDetailVC
            //nextVC.slotDetails = self.eventDetails.slots[0]
            nextVC.eventTitle = self.eventDetails.title
            nextVC.eventDescription = self.eventDetails.description
            self.present(nextVC, animated: true, completion: nil)
        }
    }
    
    
    
}

extension String {
    var doubleValue: Double {
        return Double(self)!
    }
}

extension Int {
    func randomInt(max: Int) -> Int {
        let randomNumber = self + Int(arc4random_uniform(UInt32(max - self + 1)))
        print("randomNumber:",randomNumber)
        
        return randomNumber
    }
}

/*
 
 
 ////////////////////
 /////////////////////
 if eventDetails.categoryID == 2 && slotInfo.status == 0
 {
 let storyBoard = UIStoryboard(name: "EXOrganiser", bundle: nil)
 let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXBookSpaceVC") as! EXBookSpaceVC
 
 let slotDetail = SlotModel()
 slotDetail.image = slotInfo.image//(self.content?.slots?[indexPath.item].image)!
 slotDetail.id = slotInfo.id //(self.content?.slots?[indexPath.item].id)!
 slotDetail.size = String(slotInfo.size)//String((self.content?.slots?[indexPath.item].size)!)
 slotDetail.price = String(slotInfo.price)//String((self.content?.slots?[indexPath.item].price)!)
 
 nextVC.slotDetails = slotDetail
 nextVC.eventTitle = eventDetails.title //self.content!.title!
 nextVC.eventDescription = eventDetails.description //self.content!.description!
 self.present(nextVC, animated: true, completion: nil)
 }
 else if slotInfo.status != 0//(self.content?.slots?[indexPath.item].status != 0)
 {
 let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXBookedSlotDetailVC") as! EXBookedSlotDetailVC
 //nextVC.slotDetail = (self.content?.slots?[indexPath.item])!
 nextVC.address = eventDetails.address //self.content!.address!
 nextVC.startDate = eventDetails.startDate //self.content!.start_date!
 nextVC.endDate = eventDetails.endDate //self.content!.end_date!
 nextVC.eventType = eventDetails.category //self.content!.category!
 self.present(nextVC, animated: true, completion: nil)
 }
 else if slotInfo.price.doubleValue == 0.0 || slotInfo.size.doubleValue == 0.0 //self.content?.slots?[indexPath.item].price == 0.0 || self.content?.slots?[indexPath.item].size == 0.0
 {
 
 bannerWithMessage(message: "Slot details not added for booking")
 }
 else
 {
 let storyBoard = UIStoryboard(name: "EXOrganiser", bundle: nil)
 let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXBookSpaceVC") as! EXBookSpaceVC
 
 let slotDetail = SlotModel()
 slotDetail.image = slotInfo.image!//(self.content?.slots?[indexPath.item].image)!
 slotDetail.id = slotInfo.id!//(self.content?.slots?[indexPath.item].id)!
 slotDetail.size = String(slotInfo.size!)//String((self.content?.slots?[indexPath.item].size)!)
 slotDetail.price = String(slotInfo.price!)//String((self.content?.slots?[indexPath.item].price)!)
 
 nextVC.slotDetails = slotDetail
 nextVC.eventTitle = eventDetails.title//self.content!.title!
 nextVC.eventDescription = eventDetails.description //self.content!.description!
 self.present(nextVC, animated: true, completion: nil)
 }

 
 */


/*
webviewdidload

guard let slotInfo = eventDetails.slots.filter({$0.id == slotId }).first else { return }



if /*self.content?.category_id!*/eventDetails.categoryID == 2 && slotInfo.status == 0//(self.content?.slots?[indexPath.item].status == 0)
{
    let storyBoard = UIStoryboard(name: "EXOrganiser", bundle: nil)
    let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXBookSpaceVC") as! EXBookSpaceVC
    
    let slotDetail = SlotModel()
    slotDetail.image = slotInfo.image//(self.content?.slots?[indexPath.item].image)!
    slotDetail.id = slotInfo.id //(self.content?.slots?[indexPath.item].id)!
    slotDetail.size = String(slotInfo.size)//String((self.content?.slots?[indexPath.item].size)!)
    slotDetail.price = String(slotInfo.price)//String((self.content?.slots?[indexPath.item].price)!)
    
    nextVC.slotDetails = slotDetail
    nextVC.eventTitle = eventDetails.title //self.content!.title!
    nextVC.eventDescription = eventDetails.description //self.content!.description!
    self.present(nextVC, animated: true, completion: nil)
}
else if slotInfo.status != 0//(self.content?.slots?[indexPath.item].status != 0)
{
    let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXBookedSlotDetailVC") as! EXBookedSlotDetailVC
    //nextVC.slotDetail = (self.content?.slots?[indexPath.item])!
    nextVC.address = eventDetails.address //self.content!.address!
    nextVC.startDate = eventDetails.startDate //self.content!.start_date!
    nextVC.endDate = eventDetails.endDate //self.content!.end_date!
    nextVC.eventType = eventDetails.category //self.content!.category!
    self.present(nextVC, animated: true, completion: nil)
}
else if slotInfo.price == 0.0 || slotInfo.size == 0.0 //self.content?.slots?[indexPath.item].price == 0.0 || self.content?.slots?[indexPath.item].size == 0.0
{
    
    bannerWithMessage(message: "Slot details not added for booking")
}
else
{
    let storyBoard = UIStoryboard(name: "EXOrganiser", bundle: nil)
    let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXBookSpaceVC") as! EXBookSpaceVC
    
    let slotDetail = SlotModel()
    slotDetail.image = slotInfo.image!//(self.content?.slots?[indexPath.item].image)!
    slotDetail.id = slotInfo.id!//(self.content?.slots?[indexPath.item].id)!
    slotDetail.size = String(slotInfo.size!)//String((self.content?.slots?[indexPath.item].size)!)
    slotDetail.price = String(slotInfo.price!)//String((self.content?.slots?[indexPath.item].price)!)
    
    nextVC.slotDetails = slotDetail
    nextVC.eventTitle = eventDetails.title//self.content!.title!
    nextVC.eventDescription = eventDetails.description //self.content!.description!
    self.present(nextVC, animated: true, completion: nil)
}
*/


