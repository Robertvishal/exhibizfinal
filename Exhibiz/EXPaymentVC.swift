//
//  EXPaymentVC.swift
//  Exhibiz
//
//  Created by Appzoc on 25/04/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit

enum PaymentFrom
{
    case slotBookingOR
    case slotBookingEX
    case ticketBookingEX
    case ticketBookingVS
}

class EXPaymentVC: UIViewController,UIWebViewDelegate
{
    @IBOutlet var contentWebView: UIWebView!
    
    let activity = ActivityIndicator()
    var url = ""
    var paymentFrom = PaymentFrom.slotBookingOR
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let url = URL(string: self.url)
        let urlRequest = URLRequest(url: url!)
        contentWebView.loadRequest(urlRequest)
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    {
        let confirmationVC = UIAlertController(title: "Exhbiz", message: "Do you want to Exit?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
            
            confirmationVC.dismiss(animated: false, completion: nil)
            self.dismiss(animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (action) in
            confirmationVC.dismiss(animated: true, completion: nil)
        }
        confirmationVC.addAction(okAction)
        confirmationVC.addAction(cancelAction)
        self.present(confirmationVC, animated: true)

        
        
    }
    
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool
    {
        let urlString = String(describing: request.url!)
        print(urlString)
        if urlString.contains("status=paid&message=Succeeded")
        {
            activity.stop()
            
            switch paymentFrom
            {
                case .slotBookingOR:
                    slotBookingORSuccess()
                case .slotBookingEX:
                    slotBookingEXSuccess()
                case .ticketBookingEX:
                    ticketBookingEXSuccess()
                case .ticketBookingVS:
                    ticketBookingVSSuccess()
            }
        }
        else if urlString.contains("status")
        {
            activity.stop()
            let alert = UIAlertController(title: "Payment Failed", message: "Try again to complete payment", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .default, handler:
            { (alert) in
                self.dismiss(animated: true, completion:nil)
            })
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
        return true
    }
    
    func webViewDidStartLoad(_ webView: UIWebView)
    {
        activity.start()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        activity.stop()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        activity.stop()
        let alert = UIAlertController(title: "Network Error", message: "Error occured while connecting", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Cancel", style: .default, handler:
        { (alert) in
            self.dismiss(animated: true, completion: nil)
        })
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
}

// Handling Payment Events
extension EXPaymentVC
{
    func slotBookingORSuccess()
    {
        let alert = UIAlertController(title: "Payment Successful", message: nil, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler:
        { (alert) in
            let previousVC = self.presentingViewController as? EXPaymentSelectVC
            self.dismiss(animated: true, completion:
                {
                    previousVC?.dismissToList()
            })
        })
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    func slotBookingEXSuccess()
    {
        let alert = UIAlertController(title: "Payment Successful", message: nil, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler:
        { (alert) in
            let previousVC = self.presentingViewController as? EXExChoosePaymentVC
            self.dismiss(animated: true, completion:
            {
                    previousVC?.dismissToList()
            })
        })
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    func ticketBookingEXSuccess()
    {
        
    }
    
    func ticketBookingVSSuccess()
    {
        let alert = UIAlertController(title: "Payment Successful", message: nil, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler:
        { (alert) in
            let previousVC = self.presentingViewController as? EXVSTicketDetailsVC
            self.dismiss(animated: true, completion:
            {
                    previousVC?.dismissToList()
            })
        })
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
}















