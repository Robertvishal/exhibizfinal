//
//  PLSideOptionsVC.swift
//  Popular
//
//  Created by Appzoc on 17/07/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import UIKit
import SideMenu

public func showSideOption(presentOn: UIViewController ,delegate: SideOptionsDelegate?, source: [PLSideOptionsModel], filtertype:SideOptionFilterType = .anywhere ,indexPath: IndexPath? = nil) {
    let optionsVC = UIStoryboard(name: "SideOptions", bundle: nil).instantiateViewController(withIdentifier: "PLSideOptionsVC") as! PLSideOptionsVC
    let sourceCaptilized = source.map({
        PLSideOptionsModel(sourceName: $0.name.capitalized, sourceID: $0.id)
    })
    optionsVC.tableSource = sourceCaptilized.sorted(by: { $0.name < $1.name })
    optionsVC.delegate = delegate
    optionsVC.selectedIndexPath = indexPath
    optionsVC.filterType = filtertype
    
    //let sideMenuVC = self.storyboard!.instantiateViewController(withIdentifier: "EXAdminSideMenuVC") as! EXAdminSideMenuVC
    
    /*let SideMenuController = UISideMenuNavigationController(rootViewController: optionsVC)
    SideMenuController.navigationBar.isHidden = true
    SideMenuController.leftSide = false
    
    presentOn.present(SideMenuController, animated: true, completion: nil)*/

    
    let sideMenuController = UISideMenuNavigationController(rootViewController: optionsVC)
    sideMenuController.leftSide = false
    sideMenuController.navigationBar.isHidden = true
    SideMenuManager.menuRightNavigationController = sideMenuController
    SideMenuManager.menuPresentMode = .menuSlideIn
    presentOn.present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    
}
////2nd


// Mark:- Optios data source Model
public struct PLSideOptionsModel {
    var id: Int = 0
    var name: String = ""
    var url : String = ""
    
    init() {
    }
    
    init(object: Json?,idKey:String = "id",nameKey:String = "name",urlKey:String = "url" ,isStringId:Bool = false) { // json means [String: Any]
        if let object = object {
            if isStringId {
                self.url = object[idKey] as? String ?? ""
                self.id = Int(object[idKey] as? String ?? "0")!
            }else{
                self.id = object[idKey] as? Int ?? 0
            }
            self.name = object[nameKey] as? String ?? ""
            self.url = object[urlKey] as? String ?? ""
        }
        else{
            self.id = 0
            self.name = ""
        }
    }
    
    init(sourceName:String,sourceID:Int, sourceURL: String = "") {
        self.id = sourceID
        self.name = sourceName
        self.url = sourceURL
    }
    
    static func getData(fromArray: JsonArray, idKey:String = "id", nameKey:String = "name",urlKey:String = "url", isStringId:Bool = false) -> [PLSideOptionsModel]{
        var result: [PLSideOptionsModel] = []
        for item in fromArray {
            result.append(PLSideOptionsModel.init(object: (item as! Json), idKey: idKey, nameKey: nameKey, urlKey: urlKey, isStringId: isStringId))
        }
        return result
    }
    
}


// Mark:- Optios table Cell
class PLSideOptionsTVC: UITableViewCell {
    
    @IBOutlet var listingLabel: UILabel!
    @IBOutlet var radioButtonImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

public enum SideOptionFilterType: Int {
    case anywhere
    case startsWith
}


// Mark:- Delegate
public protocol SideOptionsDelegate: class {
    func sideOptionSelected(data: PLSideOptionsModel, indexPath: IndexPath)
}

// Mark:- Optios viewcontroller
class PLSideOptionsVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var searchView: UIView!
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var listingTable: UITableView!
    @IBOutlet var NoDataLBL: UILabel!
    
    
    // Data receiving properties
    final var tableSource:[PLSideOptionsModel] = []
    final var delegate:SideOptionsDelegate?
    final var selectedIndexPath: IndexPath?
    final var filterType: SideOptionFilterType = .anywhere
    
    fileprivate var filteredSource:[PLSideOptionsModel] = []{
        didSet{
            if filteredSource.count == 0{
                NoDataLBL.isHidden = false
            }else{
                NoDataLBL.isHidden = true
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchTextField.delegate = self
        filteredSource = tableSource
        
        listingTable.reloadData()
        setUpInterface()
    }
    
    func setUpInterface(){
        searchView.layer.borderWidth = 1
        searchView.layer.borderColor = UIColor(red: 56/255, green: 56/255, blue: 56/255, alpha: 0.4).cgColor
        searchView.layer.cornerRadius = 15
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func searchAction(_ sender: Any) {
        
    }
    
    @IBAction func clearAction(_ sender: Any) {
    }
    
    @IBAction func applyFilterAction(_ sender: Any) {
    }
    
    
    @IBAction func valueChanged(_ sender: UITextField) {
        return
        if let _ = sender.text{
            if sender.text == "" || sender.text == nil {
                filteredSource = tableSource
            }else{
                if filterType == .anywhere {
                    filteredSource = tableSource.filter { $0.name.range(of: searchTextField.text!, options: .caseInsensitive) != nil }
                }else {
                    let pattern = "\\b" + NSRegularExpression.escapedPattern(for: searchTextField.text!)
                    filteredSource = tableSource.filter { $0.name.range(of: pattern, options: [.regularExpression, .caseInsensitive]) != nil }
                }
            }
            listingTable.reloadData()
        }
    }
}

//MARK:- Table Delegate and DataSource
extension PLSideOptionsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PLSideOptionsTVC", for: indexPath) as? PLSideOptionsTVC else { return UITableViewCell() }
        
        if selectedIndexPath != nil {
            if selectedIndexPath == indexPath {
                cell.listingLabel.text = filteredSource[indexPath.row].name
                cell.isSelected = true
            }else{
                cell.listingLabel.text = filteredSource[indexPath.row].name
                cell.isSelected = false
            }
        }else{
            cell.listingLabel.text = filteredSource[indexPath.row].name
            cell.isSelected = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        tableView.deselectRow(at: indexPath, animated: false)
        searchTextField.resignFirstResponder()
        if let delegateVC = self.delegate{
            let cell = tableView.cellForRow(at: indexPath) as! PLSideOptionsTVC
            // calculated indexpath from not filtered array and selected data from filtered array
            for (index,item) in tableSource.enumerated(){
                if cell.listingLabel.text! == item.name{
                    let selectedIndexFromNotFilteredSource = IndexPath(row: index, section: 0)
                    delegateVC.sideOptionSelected(data: filteredSource[indexPath.row], indexPath: selectedIndexFromNotFilteredSource)
                }
            }
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}




