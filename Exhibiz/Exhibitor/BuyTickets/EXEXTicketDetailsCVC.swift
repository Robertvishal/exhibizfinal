//
//  EXEXTicketDetailsCVC.swift
//  Exhibiz
//
//  Created by Appzoc on 12/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit

class EXEXTicketDetailsCVC: UICollectionViewCell
{
    
    @IBOutlet var ticketCountLBL: BaseLabel!
    
    override func awakeFromNib()
    {
        setUpInterface()
    }
    
    func setUpInterface(){
        ticketCountLBL.layer.cornerRadius = ticketCountLBL.frame.width / 2
        ticketCountLBL.layer.masksToBounds = true
    }
}
