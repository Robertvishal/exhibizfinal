//
//  EXExTicketsListVC.swift
//  Exhibiz
//
//  Created by Appzoc on 03/05/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit

class EXTicketListTVC: UITableViewCell
{
    @IBOutlet var nameLBL: UILabel!
    @IBOutlet var cancelBTN: UIButton!
}

enum TicketType
{
    case byStander
    case eventTicket
}

class EXExTicketsListVC: UIViewController
{
    @IBOutlet var contentTV: UITableView!
    @IBOutlet var noTicketLBL: UILabel!
    
    var location = ""
    var exhibitionDetail:ExhibitionListModel!
    var ticketType = TicketType.byStander
    
    var isReloadRequired = true
    var contentArray = [[String:Any]]()
    
    var url = ""
    var deleteUrl = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        switch ticketType
        {
        case .byStander:
            url = "ExhibizWeb/backend/api/get/bistander_tickets/\(self.exhibitionDetail.eventID)/\(self.exhibitionDetail.id)/\(UserDefaults.standard.string(forKey: "UserID")!)"
            deleteUrl = "ExhibizWeb/backend/api/cancel/bistandertickets/"
        case .eventTicket:
            url = "ExhibizWeb/backend/api/get/usertickets/\(self.exhibitionDetail.eventID)/\(UserDefaults.standard.string(forKey: "UserID")!)"
            deleteUrl = "ExhibizWeb/backend/api/cancel/tickets/"
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if isReloadRequired
        {
            getUserTickets()
            isReloadRequired = false
        }
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buyTicketBTNTapped(_ sender: UIButton)
    {
        let storyBoard = UIStoryboard(name: "EXExhibitor", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXEXTicketDetailsVC") as! EXEXTicketDetailsVC
        nextVC.eventLocationLBLString = self.location
        nextVC.exhibitionDetails = self.exhibitionDetail
        nextVC.ticketType = self.ticketType
        self.present(nextVC, animated: true, completion: nil)
    }
    
    @IBAction func cancelTicketBTNTapped(_ sender: UIButton)
    {
        let actionSheet = UIAlertController(title: "Confirm", message: "Do you want to cancel ticket", preferredStyle: .actionSheet)
        
        let yes = UIAlertAction(title: "Yes", style: .destructive)
        { (action) in
            self.deleteTicket(ticketID: (self.contentArray[sender.tag])["id"] as? String ?? "")
        }
        let no = UIAlertAction(title: "No", style: .cancel, handler: nil)
        actionSheet.addAction(yes)
        actionSheet.addAction(no)
        self.present(actionSheet, animated: true, completion: nil)
    }
}
// TableView Delegates
extension EXExTicketsListVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return contentArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! EXTicketListTVC
        cell.nameLBL.text = (contentArray[indexPath.row])["name"] as? String ?? ""
        cell.cancelBTN.tag = indexPath.row
        
        if indexPath.row  % 2 == 0
        { cell.backgroundColor = UIColor.white
        }
        else
        { cell.backgroundColor = UIColor(red:0.85, green:0.85, blue:0.85, alpha:1.0)
        }
        return cell
    }
}
// WebService
extension EXExTicketsListVC
{
    func getUserTickets()
    {
        Webservices.getMethod(url: self.url)
        { (isFetched, resultData) in
            if isFetched
            {
                let data = resultData["Data"] as? [[String:Any]] ?? [[String:Any]]()
                self.contentArray = data
                self.contentTV.reloadData()
                self.contentArray.isEmpty ? (self.noTicketLBL.isHidden = false) : (self.noTicketLBL.isHidden = true)
            }
        }
    }
    
    func deleteTicket(ticketID:String)
    {
        print("\(deleteUrl)\(ticketID)")
        Webservices.postMethod(url: "\(deleteUrl)\(ticketID)", parameter: [String:Any]())
        { (isFetched, resultData) in
            if isFetched
            {
                self.getUserTickets()
            }
        }
    }
}

