//
//  EXExCareerFairDetailVC.swift
//  Exhibiz
//
//  Created by Appzoc on 22/03/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import ImageSlideshow

class EXExCareerFairDetailVC: UIViewController
{
    @IBOutlet var slideShowView: ImageSlideshow!
    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var descriptionLBL: UILabel!
    @IBOutlet var addressLBL: UILabel!
    @IBOutlet var startDayLBL: UILabel!
    @IBOutlet var endDayLBL: UILabel!
    @IBOutlet var startDateLBL: UILabel!
    @IBOutlet var endDateLBL: UILabel!
    @IBOutlet var startingDayLBL: UILabel!
    @IBOutlet var endingDayLBL: UILabel!
    @IBOutlet var startTimeLBL: UILabel!
    @IBOutlet var endTimeLBL: UILabel!
    @IBOutlet var locationLBL: UILabel!
    @IBOutlet var emailLBL: UILabel!
    @IBOutlet var keywordsLBL: UILabel!
    @IBOutlet var eventTypeLBL: UILabel!
    @IBOutlet var ticketTypeLBL: UILabel!
    @IBOutlet var ticketPriceLBL: UILabel!
    
    @IBOutlet var vacanciesTV: UITableView!
    @IBOutlet var cancelTicketHeight: NSLayoutConstraint!
    @IBOutlet var vacanciesHeight: NSLayoutConstraint!
    
    
    var detailPageFrom = DetailPageFrom.allEvents
    var exhibitionDetail:ExhibitionListModel!
    var location = ""
    var sponcers = [AssignedMemberModel]()
    var categoryID = 0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        showRequiredViews()
        
        var imageSourceArray = [KingfisherSource]()
        for item in exhibitionDetail.exhibitionImages
        {
            imageSourceArray.append(KingfisherSource(urlString: "\(baseURL)\(imageURL)\(item)")!)
        }
        slideShowView.contentScaleMode = .scaleAspectFill
        slideShowView.setImageInputs(imageSourceArray)
        slideShowView.slideshowInterval = 2
        
        titleLBL.text = exhibitionDetail.title
        descriptionLBL.text = exhibitionDetail.description
        addressLBL.text = self.location
        startDayLBL.text = exhibitionDetail.startDate2.day
        endDayLBL.text = exhibitionDetail.endDate2.day
        startDateLBL.text = exhibitionDetail.startDate2.date
        endDateLBL.text = exhibitionDetail.endDate2.date
        //        startingDayLBL.text =
        //        endingDayLBL.text =
        startTimeLBL.text = exhibitionDetail.startTime
        endTimeLBL.text = exhibitionDetail.endTime
        locationLBL.text = exhibitionDetail.details.website
        emailLBL.text = exhibitionDetail.details.email
        keywordsLBL.text = String(exhibitionDetail.tags)
        //        eventTypeLBL.text =
        ticketTypeLBL.text = String(exhibitionDetail.ticketType)
        ticketPriceLBL.text = exhibitionDetail.price
    }

    override func viewDidAppear(_ animated: Bool)
    {
        self.vacanciesHeight.constant = self.vacanciesTV.contentSize.height
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func byStanderBTNTapped(_ sender: UIButton)
    {
        let storyBoard = UIStoryboard(name: "EXExhibitor", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXExTicketsListVC") as! EXExTicketsListVC
        nextVC.exhibitionDetail = self.exhibitionDetail
        nextVC.location = self.location
        nextVC.ticketType = .byStander
        self.present(nextVC, animated: true, completion: nil)
    }
    
    @IBAction func buyTicketBTNTapped(_ sender: UIButton)
    {
        bannerWithMessage(message: "Coming Soon")
        return
        
        let storyBoard = UIStoryboard(name: "EXExhibitor", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXExTicketsListVC") as! EXExTicketsListVC
        nextVC.exhibitionDetail = self.exhibitionDetail
        nextVC.location = self.location
        nextVC.ticketType = .eventTicket
        self.present(nextVC, animated: true, completion: nil)
    }
    
    @IBAction func announceBTNTapped(_ sender: BaseButton)
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let announceVC = storyBoard.instantiateViewController(withIdentifier: "EXCreateAnnounceVC") as! EXCreateAnnounceVC
        announceVC.eventID = String(describing: exhibitionDetail.eventID)
        announceVC.userType = .Exhibitor
        self.present(announceVC, animated: true, completion: nil)
    }
    
    func showRequiredViews()
    {
        //        switch detailPageFrom
        //        {
        //        case .allEvents:
        //            self.buyTicketHeight.constant = 48
        //            self.cancelTicketHeight.constant = 0
        //            self.callRateBTN.setBackgroundImage(#imageLiteral(resourceName: "Call"), for: .normal)
        //
        //        case .myEventsPast:
        //            self.buyTicketHeight.constant = 0
        //            self.cancelTicketHeight.constant = 0
        //            self.callRateBTN.setBackgroundImage(#imageLiteral(resourceName: "ratingStar"), for: .normal)
        //
        //        case .myEventsPresent:
        //            self.buyTicketHeight.constant = 0
        //            self.cancelTicketHeight.constant = 100
        //            self.callRateBTN.setBackgroundImage(#imageLiteral(resourceName: "Call"), for: .normal)
        //
        //        case .myEventsFuture:
        //            self.buyTicketHeight.constant = 0
        //            self.cancelTicketHeight.constant = 100
        //            self.callRateBTN.setBackgroundImage(#imageLiteral(resourceName: "Call"), for: .normal)
        //            }
    }
    
}

// CollectionView Delegates
extension EXExCareerFairDetailVC:UICollectionViewDataSource,UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return sponcers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! sponcerCellCVC
        let url = URL(string: "\(baseURL)\(imageURL)\(sponcers[indexPath.row].image)")
        cell.sponcerImage.kf.setImage(with: url, placeholder: UIImage(named: "noimage.jpg"))
        return cell
    }
}

extension EXExCareerFairDetailVC:UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return exhibitionDetail.details.career.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! uploadCareerFairCell
        cell.positionLBL.text = exhibitionDetail.details.career[indexPath.row].name
        cell.vacancyLBL.text = exhibitionDetail.details.career[indexPath.row].vacany
        return cell
    }
    
}










