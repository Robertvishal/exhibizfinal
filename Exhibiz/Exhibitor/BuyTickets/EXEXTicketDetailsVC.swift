//
//  EXEXTicketDetailsVC.swift
//  Exhibiz
//
//  Created by Appzoc on 12/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import ImageSlideshow
import WTCarouselFlowLayout

class EXEXTicketDetailsVC: UIViewController
{
    @IBOutlet var eventSlideShow: ImageSlideshow!
    @IBOutlet var eventTitleLBL: UILabel!
    @IBOutlet var eventLocationLBL: UILabel!
    @IBOutlet var eventStartDateLBL: UILabel!
    @IBOutlet var eventEndDateLBL: UILabel!
    @IBOutlet var eventTicketSelectionCV: UICollectionView!
    @IBOutlet var eventTicketPrice: UILabel!
    
    // Data variables
    var maximumTickets = 7
    private let defaultCVC = UICollectionViewCell()
    private let ticketLabelBGBlue = #colorLiteral(red: 0.1450980392, green: 0.4588235294, blue: 0.7725490196, alpha: 1)
    var eventLocationLBLString  = ""
    final var chosenTickets = 1
    final var unitPrice = 50
    var exhibitionDetails = ExhibitionListModel(data: [String:Any]())
    var location = ""
    var ticketType = TicketType.byStander
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        var imageSourceArray = [KingfisherSource]()
        for item in exhibitionDetails.exhibitionImages
        {
            imageSourceArray.append(KingfisherSource(urlString: "\(baseURL)\(imageURL)\(item)")!)
        }
        eventSlideShow.contentScaleMode = .scaleAspectFill
        eventSlideShow.setImageInputs(imageSourceArray)
        eventSlideShow.slideshowInterval = 2
        unitPrice = Int(exhibitionDetails.price) ?? 0
        
        setUpFlowLayout()
        setUpInterface()
        
    }
    
    func setUpInterface()
    {
        //let locationString = eventLocationLBLString.replacingOccurrences(of: ", ", with: "\n")
        eventLocationLBL.text = eventLocationLBLString
        eventTicketPrice.text = String(unitPrice)
    }
    
    func setUpFlowLayout() {
        
        let layout = self.eventTicketSelectionCV.collectionViewLayout as! WTCarouselFlowLayout
        layout.scrollDirection = .horizontal
        layout.itemSpacing = 0
        layout.sideItemScale = 0.85
        layout.sideItemAlpha = 0.6
        layout.sideItemBaselineType = .center
        layout.sideItemOffset = 0.0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let indexPath = IndexPath(item: 0, section: 0)
        eventTicketSelectionCV.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
        setTicketPrice(of: indexPath)
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func confirmBTNTapped(_ sender: BaseButton)
    {
        let storyBoard = UIStoryboard(name: "EXExhibitor", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXTicketDetailsSecondVC") as! EXTicketDetailsSecondVC
        nextVC.ticketNumber = chosenTickets
        nextVC.eventID = String(exhibitionDetails.eventID)
        nextVC.ticketAmount = eventTicketPrice.text!
        nextVC.ticketType = self.ticketType
        nextVC.exhibitionDetail = self.exhibitionDetails
        self.present(nextVC, animated: true, completion: nil)
    }
}


// CollectionView Delegates
extension EXEXTicketDetailsVC: UICollectionViewDataSource, UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return maximumTickets
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cellOptional = collectionView.dequeueReusableCell(withReuseIdentifier: "EXEXTicketDetailsCVC", for: indexPath) as? EXEXTicketDetailsCVC
        guard let cell = cellOptional else { return defaultCVC }
        print("IndexPathCVC",indexPath.section, indexPath.item)
        let ticketNumber = indexPath.item + 1
        cell.ticketCountLBL.text = String(ticketNumber)
        cell.ticketCountLBL.backgroundColor = ticketLabelBGBlue
        cell.ticketCountLBL.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        print("didSelectItemAt",indexPath)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        setTicketPrice(of: indexPath)
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let layout = self.eventTicketSelectionCV.collectionViewLayout as! WTCarouselFlowLayout
        let width = layout.itemSize.width + layout.minimumLineSpacing
        let index = Int(floor((scrollView.contentOffset.x - width / 2) / width) + 1)
        setTicketPrice(of: IndexPath(item: index, section: 0))
    }
    
    func setTicketPrice(of indexPath:IndexPath) {
        
        guard let cell = eventTicketSelectionCV.cellForItem(at: indexPath) as? EXEXTicketDetailsCVC else { return }
        chosenTickets = Int(cell.ticketCountLBL.text ?? "0")!
        eventTicketPrice.text = String(chosenTickets * unitPrice)
    }
    
    func ChangeTicketLabelInterface(isSelected: Bool)
    {
        
    }
}




