//
//  EXAllEventsExhibitorVC.swift
//  Exhibiz
//
//  Created by Appzoc on 14/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import SideMenu

class EXAllEventsExhibitorVC: UIViewController
{
    @IBOutlet var contentTV: UITableView!
    @IBOutlet var noEventsLBL: UILabel!
    
    var eventList = [EventListModel]()
    var isReloadRequired:Bool = false
    var listURL = "ExhibizWeb/backend/api/events?exhibitor=1"
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if isReloadRequired
        { webCall()
          isReloadRequired = false
        }
    }
    
    func webCall()
    {
        Webservices.getMethod(url: listURL)
        { (isFetched, resultData) in
            if isFetched
            {
                self.eventList.removeAll()
                for item in resultData["Data"] as! [[String:Any]]
                {
                    let eventListModel = EventListModel()
                    self.eventList.append(eventListModel.createModel(data: item))
                    debugPrint(eventListModel)
                }
                self.contentTV.reloadData()
                self.contentTV.scrollTableToTop()
            }
            else
            {   self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    
    
    @IBAction func sideMenuTapped(_ sender: UIButton)
    {
        let sideMenuVC = self.storyboard!.instantiateViewController(withIdentifier: "EXExhibitorSideMenuVC") as! EXExhibitorSideMenuVC
        let SideMenuController = UISideMenuNavigationController(rootViewController: sideMenuVC)
        SideMenuController.navigationBar.isHidden = true
        SideMenuController.leftSide = true
        self.present(SideMenuController, animated: true, completion: nil)
    }
    
    @IBAction func searchBTNTapped(_ sender: UIButton)
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let searchVC = storyBoard.instantiateViewController(withIdentifier: "EXSearchVC") as! EXSearchVC
        searchVC.eventList = self.eventList
        searchVC.fromUser = .Exhibitor
        self.present(searchVC, animated: true, completion: nil)
    }
    
    @IBAction func filterBTNTapped(_ sender: UIButton)
    {
        let optionMenu = UIAlertController(title: "Choose Type of event", message: nil, preferredStyle: .actionSheet)
        
        let allEvents = UIAlertAction(title: "All Events", style: .default, handler:
        { (alert: UIAlertAction!) -> Void in
            self.listURL = "ExhibizWeb/backend/api/events?exhibitor=1"
            self.webCall()
        })
        
        let exhibition = UIAlertAction(title: "Exhibition", style: .default, handler:
        { (alert: UIAlertAction!) -> Void in
            self.listURL = "ExhibizWeb/backend/api/events?eventtype=3"
            self.webCall()
        })
        
        let careerFair = UIAlertAction(title: "Career Fair", style: .default, handler:
        { (alert: UIAlertAction!) -> Void in
            self.listURL = "ExhibizWeb/backend/api/events?eventtype=2"
            self.webCall()
        })
        
        let Cancel = UIAlertAction(title: "Cancel", style: .cancel, handler:nil)
        
        optionMenu.addAction(exhibition)
        optionMenu.addAction(careerFair)
        optionMenu.addAction(allEvents)
        optionMenu.addAction(Cancel)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
}

// TableView Delegates
extension EXAllEventsExhibitorVC:UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if eventList.isEmpty
        { noEventsLBL.isHidden = false
        }
        else
        { noEventsLBL.isHidden = true
        }
        return eventList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventsCell") as! EXEventListCell
        cell.titleLBL.text = eventList[indexPath.row].title
        cell.dateLBL.text = eventList[indexPath.row].day
        cell.monthLBL.text = String(eventList[indexPath.row].month.prefix(3))
        cell.placeLBL.text = eventList[indexPath.row].address
//        cell.priceBTN.setTitle(eventList[indexPath.row].price, for: .normal)
        cell.bannerTagBTN.setTitle(eventList[indexPath.row].category, for: .normal)
        
        let url = URL(string: "\(baseURL)\(imageURL)\(eventList[indexPath.row].eventImages[0])")
        cell.eventImage.kf.setImage(with: url, placeholder: UIImage(named: "noimage.jpg"))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: false)
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "EXExEventDetailPage") as! EXExEventDetailPage
        nextVC.eventDetails = eventList[indexPath.row]
        self.present(nextVC, animated: true, completion: nil)
    }
    
    func selectedFromSearch(eventModel:EventListModel)
    {
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "EXExEventDetailPage") as! EXExEventDetailPage
        nextVC.eventDetails = eventModel
        self.present(nextVC, animated: true, completion: nil)
    }
}
