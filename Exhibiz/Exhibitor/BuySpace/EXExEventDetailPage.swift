//
//  EXExEventDetailPage.swift
//  Exhibiz
//
//  Created by Appzoc on 06/03/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import ImageSlideshow
import Kingfisher

class OrganisersCVC: UICollectionViewCell
{
    @IBOutlet var nameLBL: UILabel!
    @IBOutlet var organiserImage: UIImageView!
}

class SponsersCVC: UICollectionViewCell
{
    @IBOutlet var sponserImage: UIImageView!
}

class SlotsCVC: UICollectionViewCell
{
    @IBOutlet var slotNoLBL: UILabel!
    @IBOutlet var logoImage: UIImageView!
}


class EXExEventDetailPage: UIViewController
{
    @IBOutlet var organisersCV: UICollectionView!
    @IBOutlet var sponsorsCV: UICollectionView!
    @IBOutlet var slotCV: UICollectionView!
    
    @IBOutlet var slideShowView: ImageSlideshow!
    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var descriptionLBL: UILabel!
    @IBOutlet var addressLBL: UILabel!
    @IBOutlet var fromDay: UILabel!
    @IBOutlet var toDay: UILabel!
    @IBOutlet var fromDateLBL: UILabel!
    @IBOutlet var toDateLBL: UILabel!
    @IBOutlet var eventTypeLBL: UILabel!
    @IBOutlet var slotNoLBL: UILabel!
    @IBOutlet var paidBTN: BaseButton!
    @IBOutlet var freeBTN: BaseButton!
    @IBOutlet var priceAmtLBL: UILabel!
    
    @IBOutlet var slotsHeadingLBL: UILabel!
    
    @IBOutlet var bookSlotsBTN: BaseButton!
    @IBOutlet var ticketPriceViewHeight: NSLayoutConstraint!
    @IBOutlet var slotCVHeight: NSLayoutConstraint!
    
    var isFromMySpace = false
    var eventDetails = EventListModel()
    var imageLinkArray:[String] = []
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        var imageSourceArray = [KingfisherSource]()
        for item in eventDetails.eventImages
        {
            imageSourceArray.append(KingfisherSource(urlString: "\(baseURL)\(imageURL)\(item)", placeholder:UIImage(named: "noimage.jpg"))!)
            imageLinkArray.append(item)
        }
        slideShowView.contentScaleMode = .scaleAspectFill
        slideShowView.setImageInputs(imageSourceArray)
        slideShowView.slideshowInterval = 2
        
        titleLBL.text = eventDetails.title
        descriptionLBL.text = eventDetails.description
        addressLBL.text = eventDetails.address
//        fromDay.text = eventDetails.title
//        toDay.text = eventDetails.title
        fromDateLBL.text = eventDetails.startDate
        toDateLBL.text = eventDetails.endDate
//        eventTypeLBL.text = eventDetails.title
        slotNoLBL.text = String(eventDetails.slots.count)
        
        if eventDetails.ticketType == 1
        {
            ticketPriceViewHeight.constant = 70
            paidBTN.backgroundColor = blueColor
            paidBTN.setTitleColor(UIColor.white, for: .normal)
            freeBTN.backgroundColor = UIColor.white
            freeBTN.setTitleColor(UIColor.darkGray, for: .normal)
        }
        else
        {
            ticketPriceViewHeight.constant = 0
            freeBTN.backgroundColor = blueColor
            freeBTN.setTitleColor(UIColor.white, for: .normal)
            paidBTN.backgroundColor = UIColor.white
            paidBTN.setTitleColor(UIColor.darkGray, for: .normal)
        }
        self.priceAmtLBL.text = eventDetails.price
        self.setUpTapGesture()
        self.setEventType()
        
    }
    
    @IBAction func openMapTapped(_ sender: UIButton) {
        self.openInMap()
    }
    
    
    @IBAction func bookSlotTapped(_ sender: UIButton) {
        
        
        bannerWithMessage(message: "Slot booking is under construction")
        
//        let formatter = DateFormatter()
//        formatter.dateFormat = "dd-MM-yyyy"
//        let todayy = Date()
//        //let now = formatter.string(from: todayy)
//
//        let EndDate = formatter.date(from: eventDetails.endDate)
//        if todayy > EndDate!
//        {
//
//
//            bannerWithMessage(message: "Exhibition Closed")
//
//        }else {
//
//
//            let vc = UIStoryboard(name: "SlotBookingWebview", bundle: nil).instantiateViewController(withIdentifier: "EXSlotBookingWebviewVC") as! EXSlotBookingWebviewVC
//            vc.eventDetails = self.eventDetails
//            self.present(vc, animated: true, completion: nil)
//
//        }

    }
    
    
    
    func setEventType()
    {
        if eventDetails.categoryID == 1
        { eventTypeLBL.text = "Conference"
        }
        if eventDetails.categoryID == 2
        { eventTypeLBL.text = "Career Fair"
        }
        if eventDetails.categoryID == 3
        { eventTypeLBL.text = "Exhibition"
        }
    }
    
    
    
    override func viewDidAppear(_ animated: Bool)
    {
        
        if isFromMySpace {
            slotCVHeight.constant = slotCV.contentSize.height
            slotsHeadingLBL.isHidden = false
            slotCV.isHidden = false
            
            bookSlotsBTN.isHidden = true
        }else {
            slotCVHeight.constant = 0
            slotsHeadingLBL.isHidden = true
            slotCV.isHidden = true

            bookSlotsBTN.isHidden = false
        }
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func uploadBTNTapped(_ sender: BaseButton)
    {
       
    }
    
    func setUpTapGesture(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        slideShowView.addGestureRecognizer(tapGesture)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer){
        print("Gesture Tapped")
          slideShowView.presentFullScreenController(from: self)
//        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EXImageViewerVC") as! EXImageViewerVC
//        vc.sourceType = .link
//        vc.linkSource = imageLinkArray
//        self.present(vc, animated: true, completion: nil)
    }
    
    func openInMap(){
        let latitude = eventDetails.lat
        let longitude = eventDetails.lon
        let mapAppURL = "comgooglemaps://?center=\(latitude),\(longitude)&zoom=18"
        let browserURL = "https://www.google.com/maps/@\(latitude),\(longitude),18z"
        if let _ = URL(string: "comgooglemaps://"), UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!){
            UIApplication.shared.openURL(URL(string: "\(mapAppURL)")!)
        }else if UIApplication.shared.canOpenURL(URL(string: "\(browserURL)")!){
            UIApplication.shared.openURL(URL(string: "\(browserURL)")!)
        }
    }
    
}

// CollectionView Delegates
extension EXExEventDetailPage:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == organisersCV
        { return eventDetails.assignedmembers.count
        }
        else if collectionView == sponsorsCV
        { return eventDetails.sponers.count
        }
        else // slots
        { return isFromMySpace ? eventDetails.slots.count : 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == organisersCV
        { let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! OrganisersCVC
          cell.nameLBL.text = eventDetails.assignedmembers[indexPath.item].username
          let url = URL(string: "\(baseURL)\(imageURL)\(eventDetails.assignedmembers[indexPath.item].image)")
          cell.organiserImage.kf.setImage(with: url, placeholder: UIImage(named: "noimage.jpg"))
          return cell
        }
        
        else if collectionView == sponsorsCV
        { let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SponsersCVC
          let url = URL(string: "\(baseURL)\(imageURL)\(eventDetails.sponers[indexPath.item].image)")
          cell.sponserImage.kf.setImage(with: url, placeholder: UIImage(named: "noimage.jpg"))
          return cell
        }
            
        else // slots
        { let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SlotsCVC
          cell.slotNoLBL.text = String(indexPath.item+1)
          
          if isFromMySpace && eventDetails.slots[indexPath.item].exhibition_id == 0
          {
            cell.logoImage.image = nil
          }
          else
          {
            if (eventDetails.slots[indexPath.item].status != 0)
            {
                let url = URL(string: "\(baseURL)\(imageURL)\(eventDetails.slots[indexPath.item].booking_image)")
                cell.logoImage.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "noimage.jpg"))
            }
          }

          if (self.eventDetails.categoryID != 2) && (eventDetails.slots[indexPath.item].price == "" || eventDetails.slots[indexPath.item].size == "")
          { cell.backgroundColor = UIColor.lightGray
          }
          else
          { cell.backgroundColor = UIColor.white
          }
          return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == slotCV
        {
            if isFromMySpace
            {
                if eventDetails.slots[indexPath.item].exhibition_id != 0
                {
                    bannerWithMessage(message: "Exhibition already added in this slot")
                }
                else
                {
                    let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXCreateExhibitionVC") as! EXCreateExhibitionVC
                    nextVC.eventID = eventDetails.id
                    nextVC.slotID = eventDetails.slots[indexPath.item].id
                    nextVC.categoryID = eventDetails.categoryID
                    nextVC.eventAddress = eventDetails.address
                    self.present(nextVC, animated: true, completion: nil)
                }
            }
            else
            {
                if eventDetails.slots[indexPath.item].status != 0
                { bannerWithMessage(message: "This slot is already booked")
                }
                else if (self.eventDetails.categoryID != 2) && (eventDetails.slots[indexPath.item].price == "" || eventDetails.slots[indexPath.item].size == "")
                { bannerWithMessage(message: "Slot details not added for booking")
                }
                else
                { let storyBoard = UIStoryboard(name: "EXExhibitor", bundle: nil)
                    let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXExSlotDetailVC") as! EXExSlotDetailVC
                    nextVC.slotDetails = eventDetails.slots[indexPath.item]
                    nextVC.eventTitle = eventDetails.title
                    nextVC.eventDescription = eventDetails.description
                    self.present(nextVC, animated: true, completion: nil)
                }
            }
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView == organisersCV
        { return CGSize(width: 80, height: 100)
        }
        else if collectionView == sponsorsCV
        { return CGSize(width: 120, height: 100)
        }
        else
        { return CGSize(width: (slotCV.bounds.width-25)/4, height: (slotCV.bounds.width-25)/4)
        }
    }
    
}
