//
//  EXExSlotDetailVC.swift
//  Exhibiz
//
//  Created by Appzoc on 07/03/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import Kingfisher
import ImageSlideshow


class EXExSlotDetailVC: UIViewController
{
    @IBOutlet var slideShowView: ImageSlideshow!
    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var descriptionLBL: UILabel!
    @IBOutlet var priceLBL: UILabel!
    @IBOutlet var sizeLBL: UILabel!
    
    var slotDetails = SlotModel()
    var eventTitle = ""
    var eventDescription = ""
    
    override func viewDidLoad()
    {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTap))
        slideShowView.addGestureRecognizer(gestureRecognizer)
        
        super.viewDidLoad()
        slideShowView.contentScaleMode = .scaleAspectFill
        slideShowView.setImageInputs([KingfisherSource(urlString: "\(baseURL)\(imageURL)\(slotDetails.image)",
                                        placeholder: UIImage(named: "noimage.jpg"))!])
        titleLBL.text = eventTitle
        descriptionLBL.text = eventDescription
        priceLBL.text = slotDetails.price + " SR"
        sizeLBL.text = slotDetails.size + " sq ft"
    }
    
    @objc func didTap()
    {
        slideShowView.presentFullScreenController(from: self)
    }
    
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addDetailsBTNTapped(_ sender: UIButton)
    {
        let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXExBookSpaceVC")
        self.present(nextVC, animated: true, completion: nil)
    }
    
}
