//
//  EXExhibitorScanner.swift
//  Exhibiz
//
//  Created by Akhil Chandran on 19/10/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import Alamofire

class EXExhibitorScanner: UIViewController {
    
    var idFromqr = ""
    
    
    var isReloadRequired = true

    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        if isReloadRequired
        {
            isReloadRequired = false
            self.openScanner()
        }
        
        
    }

    lazy var readerVC: QRCodeReaderViewController =
        {
            let builder = QRCodeReaderViewControllerBuilder {
                $0.reader = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
            }
            return QRCodeReaderViewController(builder: builder)
    }()
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func openScanner()
    {
        readerVC.completionBlock =
            { (result: QRCodeReaderResult?) in
                self.readerVC.dismiss(animated: true, completion: nil)
                if result?.value != nil
                {
                    print(result!.value)
                    
                    self.idFromqr = (result!.value as? String ?? "")!
                   
                }
                
        }
        
        // Presents the readerVC as modal form sheet
        readerVC.modalPresentationStyle = .popover
        present(readerVC, animated: true, completion: nil)
    }
    
    @IBAction func DismisBtnTapped(_ sender: UIButton) {
        
        self.dismiss(animated: false, completion: nil)
        
    }
    
    @IBAction func saveToWebTapped(_ sender: Any) {
        
        
        if  self.idFromqr == ""
        {
            bannerWithMessage(message: "Please scan the user again")

        }
        else
        {
          callweb()
        }
        
        
    }
    
    
    
    func callweb()
    {
        
        
        let ur = "https://signitivebeta.com/exhibizWeb/backend/api/addVisitorsHistory"
        let k = UserDefaults.standard.string(forKey: "UserID")!
        
     
        
        let activity = ActivityIndicator()
        activity.start()
        
        
        Alamofire.request(ur, method: .post, parameters: ["user_id":idFromqr,"exhibitor_id":k,"event_id":"0"],encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                activity.stop()
                print(response)
                let result = response.result.value as! [String:Any]
                print(result)
                
                let Errorcode = result["errorcode"] as? String ?? "1"
                let Message = result["msg"] as? String ?? "Error occured while fetching data"
                
                
                 if Message != "success"
                 {
                    
                    bannerWithMessage(message: "Success")
                 }
                 else
                 {
                    bannerWithMessage(message: "Please scan the user again")
                  }
                
                break
            case .failure(let error):
                
                print(error)
                activity.stop()
            }
        }
        
        
    }
    
    

}
