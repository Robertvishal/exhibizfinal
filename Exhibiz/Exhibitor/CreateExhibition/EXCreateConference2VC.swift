//
//  EXCreateConference2VC.swift
//  Exhibiz
//
//  Created by Appzoc on 20/03/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import PlaceholderTextView
import OpalImagePicker


class EXCreateConference2VC: UIViewController,OpalImagePickerControllerDelegate,UITextFieldDelegate
{
    @IBOutlet var companyTV: PlaceholderTextView!
    @IBOutlet var informationTV: PlaceholderTextView!
    @IBOutlet var addressTV: PlaceholderTextView!
    @IBOutlet var emailTV: PlaceholderTextView!
    
    @IBOutlet var companyImage: UIImageView!
    @IBOutlet var imageBTN: UIButton!
    @IBOutlet var contentTV: UITableView!
    @IBOutlet var contentTVHeight: NSLayoutConstraint!
    
    
    var postParameter = [String:Any]()
    var eventID = 0
    var slotID = 0
    var eventAddress = ""
    var selectedImages = [UIImage]()
    
    var ConferenceModel = [ConferenceCellModel()]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    @IBAction func imageBTNTapped(_ sender: UIButton)
    {
        let imagePicker = OpalImagePickerController()
        imagePicker.maximumSelectionsAllowed = 1
        imagePicker.imagePickerDelegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage])
    {
        companyImage.image = images[0]
        selectedImages.append(images[0])
        imageBTN.setTitle("", for: .normal)
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addDetailsBTNTapped(_ sender: BaseButton)
    {
        if companyImage.image == nil
        { bannerWithMessage(message: "Choose Company Image")
        }
        else if companyTV.text.isEmpty
        { bannerWithMessage(message: "Enter Company name")
        }
        else if informationTV.text.isEmpty
        { bannerWithMessage(message: "Enter Location")
        }
        else if addressTV.text.isEmpty
        { bannerWithMessage(message: "Enter Address")
        }
        else if emailTV.text.isEmpty
        { bannerWithMessage(message: "Enter Email")
        }
        else if CareerFairModelValidation() == false
        { bannerWithMessage(message: "Enter positions and vacancies")
        }
        else
        {
            //            let date = Date()
            //            let createdDate = DateFormatter()
            //            createdDate.dateFormat = "dd-MM-yyyy"
            //
            //            postParameter["name"] = companyTV.text
            //            postParameter["created_user"] = UserDefaults.standard.string(forKey: "UserID")
            //            postParameter["website"] = ""
            //            postParameter["location"] = locationTV.text
            //            postParameter["email"] = emailTV.text
            //            postParameter["address"] = addressTV.text
            //            postParameter["info"] = ""
            //            postParameter["phone"] = ""
            //            postParameter["created_at"] = createdDate.string(from: date)
            //
            //            let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXUploadExhibitionVC") as! EXUploadExhibitionVC
            //            nextVC.postParameter = self.postParameter
            //            nextVC.selectedImages = self.selectedImages
            //            nextVC.eventAddress = self.eventAddress
            //            nextVC.eventID = String(self.eventID)
            //            nextVC.slotID = String(self.slotID)
            //            self.present(nextVC, animated: true, completion: nil)
        }
    }
    
    func CareerFairModelValidation() -> Bool
    {
//        for items in ConferenceModel
//        { if items.position == "" || items.vacancy == ""
//        {return false}
//        }
        return true
    }
    
    
    
    @IBAction func addVacancyBTNTapped(_ sender: UIButton)
    {
        ConferenceModel.append(ConferenceCellModel())
        contentTV.reloadData()
        contentTVHeight.constant = contentTV.contentSize.height
        
        for item in ConferenceModel
        {
            print(item.speakerName)
        }
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        let cell = contentTV.cellForRow(at: IndexPath(row: textField.tag, section: 0)) as! ConferenceCell
        if textField == cell.speakerNameTF
        { ConferenceModel[textField.tag].speakerName = textField.text!
        }
    }
    
}

//Table View Delegates
extension EXCreateConference2VC:UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return ConferenceModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConferenceCell") as! ConferenceCell
        cell.speakerNameTF.tag = indexPath.row
        return cell
    }
}
