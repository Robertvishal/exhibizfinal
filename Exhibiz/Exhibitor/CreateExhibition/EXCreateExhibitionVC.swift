//
//  EXCreateExhibitionVC.swift
//  Exhibiz
//
//  Created by Appzoc on 20/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import OpalImagePicker
import DatePickerDialog
import PlaceholderTextView

class EXCreateExhibitionVC: UIViewController,OpalImagePickerControllerDelegate
{
    @IBOutlet var eventTitleTextView: PlaceholderTextView!
    @IBOutlet var descriptionTextView: UITextViewFixed!
    @IBOutlet var tagsKeywords: UITextField!
    @IBOutlet var startDateLBL: UILabel!
    @IBOutlet var endDateLBL: UILabel!
    @IBOutlet var startTimeLBL: UILabel!
    @IBOutlet var endTimeLBL: UILabel!
    @IBOutlet var exhibitionTypeTF: UITextField!
    @IBOutlet var ticketPriceTF: UITextField!
    
    @IBOutlet var freeTicketBTN: BaseButton!
    @IBOutlet var paidTicketBTN: BaseButton!
    
    @IBOutlet var exhibitionPicsCV: UICollectionView!
    
    @IBOutlet var picturesCVHeight: NSLayoutConstraint!
    @IBOutlet var ticketPriceViewHeight: NSLayoutConstraint!
    @IBOutlet var exhibitionTitleLBL: UILabel!
    
    
    var eventID = 0
    var slotID = 0
    var categoryID = 0
    var eventAddress = ""
    var ticketType = 0
    
    var postParameter = [String:Any]()
    var selectedImages = [UIImage]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        ticketPriceViewHeight.constant = 1
        
        if categoryID == 1
        { exhibitionTypeTF.text = "Conference"
        }
        else if categoryID == 2
        { exhibitionTypeTF.text = "Career Fair"
          exhibitionTitleLBL.text = "Company Name"
          eventTitleTextView.placeholder = "Enter Company name"
        }
        else if categoryID == 3
        { exhibitionTypeTF.text = "Exhibition"
          exhibitionTitleLBL.text = "Exhibition Title"
        }
        else
        { exhibitionTypeTF.text = "Error on fetching event type"
        }
    }
    
    @IBAction func freeTicketBTNTapped(_ sender: BaseButton)
    {   ticketPriceViewHeight.constant = 1
        freeTicketBTN.backgroundColor = blueColor
        freeTicketBTN.setTitleColor(UIColor.white, for: .normal)
        paidTicketBTN.backgroundColor = UIColor.white
        paidTicketBTN.setTitleColor(UIColor.darkGray, for: .normal)
        ticketType = 0
    }
    
    @IBAction func paidTicketBTNTapped(_ sender: BaseButton)
    {   ticketPriceViewHeight.constant = 70
        paidTicketBTN.backgroundColor = blueColor
        paidTicketBTN.setTitleColor(UIColor.white, for: .normal)
        freeTicketBTN.backgroundColor = UIColor.white
        freeTicketBTN.setTitleColor(UIColor.darkGray, for: .normal)
        ticketType = 1
    }
    
    @IBAction func nextBTNTapped(_ sender: BaseButton)
    {
        if isValid()
        {
            if categoryID == 2
            {   let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXCreateCareerFair2VC") as! EXCreateCareerFair2VC
                nextVC.eventID = self.eventID
                nextVC.slotID = self.slotID
                nextVC.postParameter = self.postParameter
                nextVC.eventAddress = self.eventAddress
                nextVC.selectedImages = self.selectedImages
                self.present(nextVC, animated: true, completion: nil)
            }
            else if categoryID == 3
            {   let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXCreateExhibition2VC") as! EXCreateExhibition2VC
                nextVC.eventID = self.eventID
                nextVC.slotID = self.slotID
                nextVC.postParameter = self.postParameter
                nextVC.eventAddress = self.eventAddress
                nextVC.selectedImages = self.selectedImages
                self.present(nextVC, animated: true, completion: nil)
            }
            
        }
    }
    @IBAction func deleteImageBTNTapped(_ sender: UIButton)
    {
        selectedImages.remove(at: sender.tag)
        exhibitionPicsCV.reloadData()
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func startDateTapped(_ sender: UIButton)
    {
        DatePickerDialog().show( "Choose start date", doneButtonTitle: "Done",
                                 cancelButtonTitle: "Cancel",
                                 defaultDate: Date(), minimumDate: Date(),
                                 datePickerMode: .date)
        {
        (date) -> Void in
            if date != nil
            {
                let selectedDate = DateFormatter()
                selectedDate.dateFormat = "dd-MM-yyyy"
                self.startDateLBL.text = selectedDate.string(from: date!)
                self.postParameter["start_date"] = selectedDate.string(from: date!)
            }
        }
    }
    
    @IBAction func endDateTapped(_ sender: UIButton)
    { DatePickerDialog().show( "Choose end date", doneButtonTitle: "Done",
                               cancelButtonTitle: "Cancel",
                               defaultDate: Date(), minimumDate: Date(),
                               datePickerMode: .date)
        {
        (date) -> Void in
            if date != nil
            {
                let selectedDate = DateFormatter()
                selectedDate.dateFormat = "dd-MM-yyyy"
                self.endDateLBL.text = selectedDate.string(from: date!)
                self.postParameter["end_date"] = selectedDate.string(from: date!)
            }
        }
    }
    
    @IBAction func startTimeTapped(_ sender: UIButton)
    { DatePickerDialog().show( "Choose start time", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .time)
        {
        (date) -> Void in
            if date != nil
            {
                let selectedDate = DateFormatter()
                selectedDate.dateFormat = "h:mm a"
                let postTime = DateFormatter()
                postTime.dateFormat = "HH:mm:ss"
                self.startTimeLBL.text = selectedDate.string(from: date!)
                self.postParameter["start_time"] = postTime.string(from: date!)
            }
        }
    }
    
    @IBAction func endTimeTapped(_ sender: UIButton)
    { DatePickerDialog().show( "Choose end time", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .time)
        {
        (date) -> Void in
            if date != nil
            {
                let selectedDate = DateFormatter()
                selectedDate.dateFormat = "h:mm a"
                let postTime = DateFormatter()
                postTime.dateFormat = "HH:mm:ss"
                self.endTimeLBL.text = selectedDate.string(from: date!)
                self.postParameter["end_time"] = postTime.string(from: date!)
            }
        }
    }
}

// CollectionView Delegates
extension EXCreateExhibitionVC:UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,
    UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    { return selectedImages.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if indexPath.item == 0
        {   let addEventCell = collectionView.dequeueReusableCell(withReuseIdentifier: "addphotoCell", for: indexPath)
            return addEventCell
        }
        else
        {
            let addEventCell = collectionView.dequeueReusableCell(withReuseIdentifier: "addEventCell", for: indexPath) as! addEventCell
            addEventCell.cellImageView.image = selectedImages[indexPath.item-1]
            addEventCell.cellDeleteBTN.tag = indexPath.item-1
            return addEventCell
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    { if indexPath.item == 0
      {
        let imagePicker = OpalImagePickerController()
        imagePicker.imagePickerDelegate = self
        imagePicker.maximumSelectionsAllowed = 5 - selectedImages.count
        present(imagePicker, animated: true, completion: nil)
      }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.bounds.width/3, height: (collectionView.bounds.width/3)*0.857)
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage])
    {
        selectedImages = selectedImages + images
        exhibitionPicsCV.reloadData()
        picker.dismiss(animated: true, completion: nil)
    }
    
}

// Field Validation
extension EXCreateExhibitionVC
{
    func isValid() -> Bool
    {
        if eventTitleTextView.text.isEmpty
        { bannerWithMessage(message: "Enter title")
          return false
        }
        else if descriptionTextView.text.isEmpty
        { bannerWithMessage(message: "Enter description")
          return false
        }
        else if selectedImages.isEmpty
        { bannerWithMessage(message: "Choose Exhibition Images")
          return false
        }
        else if tagsKeywords.text!.isEmpty
        { bannerWithMessage(message: "Enter keywords")
          return false
        }
        else if startDateLBL.text!.isEmpty
        { bannerWithMessage(message: "Choose start date")
          return false
        }
        else if endDateLBL.text!.isEmpty
        { bannerWithMessage(message: "Choose end date")
          return false
        }
        else if startTimeLBL.text!.isEmpty
        { bannerWithMessage(message: "Choose start time")
          return false
        }
        else if endTimeLBL.text!.isEmpty
        { bannerWithMessage(message: "Choose end time")
          return false
        }
        else if exhibitionTypeTF.text!.isEmpty
        { bannerWithMessage(message: "Enter exhibition type")
          return false
        }
        else if ticketType == 1 && ticketPriceTF.text!.isEmpty
        { bannerWithMessage(message: "Enter ticker price")
          return false
        }
        else
        {
          postParameter["title"] = eventTitleTextView.text
          postParameter["description"] = descriptionTextView.text
          postParameter["tags"] = tagsKeywords.text
          postParameter["event_type"] = String(self.categoryID)
          postParameter["ticket_type"] = String(ticketType)
          postParameter["price"] = ticketPriceTF.text
          print(postParameter)
          return true
        }
    }
}

//title:Hero Cycles
//description:cycles exhibition
//tags:tags
//start_date:15-03-2018
//end_date:20-03-2018
//start_time:10:00:00
//end_time:12:00:00
//event_type:1
//ticket_type:1
//price:500



