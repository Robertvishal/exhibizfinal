//
//  EXCreateExhibition2VC.swift
//  Exhibiz
//
//  Created by Appzoc on 21/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import PlaceholderTextView
import OpalImagePicker

class EXCreateExhibition2VC: UIViewController,OpalImagePickerControllerDelegate
{
    @IBOutlet var companyTV: PlaceholderTextView!
    @IBOutlet var websiteTV: PlaceholderTextView!
    @IBOutlet var addressTV: PlaceholderTextView!
    @IBOutlet var emailTV: PlaceholderTextView!
    
    @IBOutlet var companyImage: UIImageView!
    @IBOutlet var imageBTN: UIButton!
    
    var postParameter = [String:Any]()
    var eventID = 0
    var slotID = 0
    var eventAddress = ""
    var selectedImages = [UIImage]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.companyTV.text = UserDefaults.standard.string(forKey: "Username")
        self.emailTV.text = UserDefaults.standard.string(forKey: "UserEmail")
    }
    
    @IBAction func imageBTNTapped(_ sender: UIButton)
    {
        let imagePicker = OpalImagePickerController()
        imagePicker.maximumSelectionsAllowed = 1
        imagePicker.imagePickerDelegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage])
    {
        companyImage.image = images[0]
        selectedImages.append(images[0])
        imageBTN.setTitle("", for: .normal)
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addDetailsBTNTapped(_ sender: BaseButton)
    {
        if companyTV.text.isEmpty
        { bannerWithMessage(message: "Enter Company name")
        }
        else if websiteTV.text.isEmpty
        { bannerWithMessage(message: "Enter Website")
        }
        else if addressTV.text.isEmpty
        { bannerWithMessage(message: "Enter Address")
        }
        else if emailTV.text.isEmpty
        { bannerWithMessage(message: "Enter Email")
        }
        else
        {
            let date = Date()
            let createdDate = DateFormatter()
            createdDate.dateFormat = "dd-MM-yyyy"
            
            postParameter["name"] = companyTV.text
            postParameter["created_user"] = UserDefaults.standard.string(forKey: "UserID")
            postParameter["website"] = websiteTV.text
            postParameter["location"] = ""
            postParameter["email"] = emailTV.text
            postParameter["address"] = addressTV.text
            postParameter["info"] = ""
            postParameter["phone"] = ""
            postParameter["created_at"] = createdDate.string(from: date)
            
            let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXUploadExhibitionVC") as! EXUploadExhibitionVC
            nextVC.postParameter = self.postParameter
            nextVC.selectedImages = self.selectedImages
            nextVC.eventAddress = self.eventAddress
            nextVC.eventID = String(self.eventID)
            nextVC.slotID = String(self.slotID)
            self.present(nextVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
}

//name:Company Name
//created_user:59
//website:www.appzoc.com
//location:kaloor
//email:appzoc@gmail.com
//address:kaloor, kerala
//info:information
//phone:123456789
//created_at:15-03-2018





