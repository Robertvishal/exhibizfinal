//
//  EXCreateCareerFair2VC.swift
//  Exhibiz
//
//  Created by Appzoc on 20/03/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import PlaceholderTextView
import OpalImagePicker

class CareerFairCell: UITableViewCell
{
    @IBOutlet var positionTF: UITextField!
    @IBOutlet var vacancyTF: UITextField!
}

class CareerFairCellModel
{
    var position = ""
    var vacancy = ""
}


class EXCreateCareerFair2VC: UIViewController,OpalImagePickerControllerDelegate,UITextFieldDelegate
{
    @IBOutlet var companyTV: PlaceholderTextView!
    @IBOutlet var locationTV: PlaceholderTextView!
    @IBOutlet var addressTV: PlaceholderTextView!
    @IBOutlet var emailTV: PlaceholderTextView!
    
    @IBOutlet var companyImage: UIImageView!
    @IBOutlet var imageBTN: UIButton!
    @IBOutlet var contentTV: UITableView!
    @IBOutlet var contentTVHeight: NSLayoutConstraint!
    
    @IBOutlet var minusBTN: UIButton!
    
    var postParameter = [String:Any]()
    var eventID = 0
    var slotID = 0
    var eventAddress = ""
    var selectedImages = [UIImage]()
    
    var CareerFairModel = [CareerFairCellModel()]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        minusBTN.isHidden = true
        self.companyTV.text = postParameter["title"] as! String
        self.emailTV.text = UserDefaults.standard.string(forKey: "UserEmail")
    }
    
    @IBAction func imageBTNTapped(_ sender: UIButton)
    {
        let imagePicker = OpalImagePickerController()
        imagePicker.maximumSelectionsAllowed = 1
        imagePicker.imagePickerDelegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage])
    {
        companyImage.image = images[0]
        selectedImages.append(images[0])
        imageBTN.setTitle("", for: .normal)
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addDetailsBTNTapped(_ sender: BaseButton)
    {
        if companyTV.text.isEmpty
        { bannerWithMessage(message: "Enter Company name")
        }
        else if locationTV.text.isEmpty
        { bannerWithMessage(message: "Enter Location")
        }
        else if addressTV.text.isEmpty
        { bannerWithMessage(message: "Enter Address")
        }
        else if emailTV.text.isEmpty
        { bannerWithMessage(message: "Enter Email")
        }
        else if CareerFairModelValidation() == false
        { bannerWithMessage(message: "Enter positions and vacancies")
        }
        else
        {
            let date = Date()
            let createdDate = DateFormatter()
            createdDate.dateFormat = "dd-MM-yyyy"

            postParameter["name"] = companyTV.text
            postParameter["created_user"] = UserDefaults.standard.string(forKey: "UserID")
            postParameter["website"] = ""
            postParameter["location"] = locationTV.text
            postParameter["email"] = emailTV.text
            postParameter["address"] = addressTV.text
            postParameter["info"] = ""
            postParameter["phone"] = ""
            postParameter["created_at"] = createdDate.string(from: date)
            
            var positionNameArray = [String]()
            var vacancyArray = [String]()
            
            for items in CareerFairModel
            {
                positionNameArray.append(items.position)
                vacancyArray.append(items.vacancy)
            }
            print(positionNameArray)
            print(vacancyArray)
            postParameter["positionname"] = positionNameArray
            postParameter["vacany"] = vacancyArray
            
            let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXUploadCareerFairVC") as! EXUploadCareerFairVC
            nextVC.postParameter = self.postParameter
            nextVC.selectedImages = self.selectedImages
            nextVC.eventAddress = self.eventAddress
            nextVC.eventID = String(self.eventID)
            nextVC.slotID = String(self.slotID)
            self.present(nextVC, animated: true, completion: nil)
        }
    }
    
    func CareerFairModelValidation() -> Bool
    {
        for items in CareerFairModel
        { if items.position == "" || items.vacancy == ""
            {return false}
        }
        return true
    }
    
    
    
    @IBAction func addVacancyBTNTapped(_ sender: UIButton)
    {
        CareerFairModel.append(CareerFairCellModel())
        contentTV.reloadData()
        contentTVHeight.constant = contentTV.contentSize.height
        minusBTN.isHidden = false
    }
    
    @IBAction func minusVacancyBTNTapped(_ sender: UIButton)
    {
        CareerFairModel.removeLast()
        contentTV.reloadData()
        contentTVHeight.constant = contentTV.contentSize.height
        if CareerFairModel.count == 1
        { minusBTN.isHidden = true
        }
    }
    
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        let cell = contentTV.cellForRow(at: IndexPath(row: textField.tag, section: 0)) as! CareerFairCell
        if textField == cell.positionTF
        { CareerFairModel[textField.tag].position = textField.text!
        }
        else
        { CareerFairModel[textField.tag].vacancy = textField.text!
        }
    }
    
}

//Table View Delegates
extension EXCreateCareerFair2VC:UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return CareerFairModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CareerFairCell") as! CareerFairCell
        cell.positionTF.tag = indexPath.row
        cell.vacancyTF.tag = indexPath.row
        cell.positionTF.text = CareerFairModel[indexPath.row].position
        cell.vacancyTF.text = CareerFairModel[indexPath.row].vacancy
        return cell
    }
}






