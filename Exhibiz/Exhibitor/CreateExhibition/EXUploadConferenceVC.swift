//
//  EXUploadConferenceVC.swift
//  Exhibiz
//
//  Created by Appzoc on 22/03/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import ImageSlideshow

class EXUploadConferenceVC: UIViewController
{
    @IBOutlet var slideShowView: ImageSlideshow!
    @IBOutlet var exhibitionTitleLBL: UILabel!
    @IBOutlet var descriptionLBL: UILabel!
    @IBOutlet var locationLBL: UILabel!
    @IBOutlet var startDateLBL: UILabel!
    @IBOutlet var endDateLBL: UILabel!
    @IBOutlet var startTimeLBL: UILabel!
    @IBOutlet var endTimeLBL: UILabel!
    @IBOutlet var informationLBL: UILabel!
    @IBOutlet var emailLBL: UILabel!
    @IBOutlet var tagsLBL: UILabel!
    @IBOutlet var eventTypeLBL: UILabel!
    @IBOutlet var ticketTypeLBL: UILabel!
    @IBOutlet var ticketPriceLBL: UILabel!
    
    var postParameter = [String:Any]()
    var selectedImages = [UIImage]()
    var eventID = ""
    var slotID = ""
    var eventAddress = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        slideShowView.contentScaleMode = .scaleAspectFill
        slideShowView.setImageInputs([ImageSource(image: selectedImages[0])])
        
        exhibitionTitleLBL.text = postParameter["title"] as? String ?? ""
        descriptionLBL.text = postParameter["description"] as? String ?? ""
        locationLBL.text = postParameter[""] as? String ?? ""
        startDateLBL.text = postParameter["start_date"] as? String ?? ""
        endDateLBL.text = postParameter["end_date"] as? String ?? ""
        startTimeLBL.text = postParameter["start_time"] as? String ?? ""
        endTimeLBL.text = postParameter["end_time"] as? String ?? ""
        informationLBL.text = postParameter["website"] as? String ?? ""
        emailLBL.text = postParameter["email"] as? String ?? ""
        tagsLBL.text = postParameter["tags"] as? String ?? ""
        ticketTypeLBL.text = (postParameter["ticket_type"] as! String == "1") ? "Paid" : "Free"
        ticketPriceLBL.text = (postParameter["price"] as! String == "") ? "Free" : postParameter["price"] as! String
        locationLBL.text = self.eventAddress
        
        if postParameter["event_type"] as! String == "1"
        { eventTypeLBL.text = "Conference"
        }
        else if postParameter["event_type"] as! String == "2"
        { eventTypeLBL.text = "Career Fair"
        }
        else if postParameter["event_type"] as! String == "3"
        { eventTypeLBL.text = "Exhibition"
        }
        else
        { eventTypeLBL.text = "Error Fetching event type"
        }
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func uploadBTNTapped(_ sender: UIButton)
    {
        Webservices.postMethodMultiPartImage(url: "ExhibizWeb/backend/api/add/exhibition/\(eventID)/\(slotID)",
            parameter: self.postParameter,
            imageParameter: ["images[]":selectedImages])
        { (isFetched, ResultData) in
            if isFetched
            { self.dismissToList()
            }
        }
    }
    
    func dismissToList()
    {
        let alert = UIAlertController(title: "Added Successfully",
                                      message: "You can view your Exhibitions from My Exhibitions",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok",
                                      style: .default,
                                      handler:
            { _ in
                self.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
}
