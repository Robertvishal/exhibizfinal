//
//  EXExBuySlotVC.swift
//  Exhibiz
//
//  Created by Robert on 17/11/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import WebKit

class EXExBuySlotVC: UIViewController, WKNavigationDelegate, WKUIDelegate {

    @IBOutlet weak var slotWebView: WKWebView!
    
    final var URLslotSelection = "http://signitivebeta.com/exhibizWeb/slot_booking_beta/"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        slotWebView.uiDelegate = self
        slotWebView.navigationDelegate = self
        if #available(iOS 10.0, *) {
            slotWebView.configuration.dataDetectorTypes = .trackingNumber
        } else {
            // Fallback on earlier versions
        }
        loadSlotURL()
        
    }
    
    private func loadSlotURL(){
        let url = URL(string: URLslotSelection)!
        slotWebView.load(URLRequest(url: url))

    }

    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        print("didCommit")
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        print("decidePolicyFor navigationAction",navigationAction)
        
        if(navigationAction.navigationType == .other)
        {
            if navigationAction.request.url != nil
            {
                //do what you need with url
                
            }
            decisionHandler(.cancel)
            return
        }
        decisionHandler(.allow)

    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("didFinishNavigatin",navigation)
    }
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        print("runJavaScriptAlertPanelWithMessage",message)
        completionHandler()
    }
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (String?) -> Void) {
        print("runJavaScriptTextInputPanelWithPrompt")
        completionHandler("")
    }
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        print("didReceiveServerRedirectForProvisionalNavigation")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    

}
