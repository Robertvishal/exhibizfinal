//
//  EXMySpacesVC.swift
//  Exhibiz
//
//  Created by Appzoc on 14/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import SideMenu

class EXMySpacesVC: UIViewController
{
    @IBOutlet var contentTV: UITableView!
    @IBOutlet var noEventsLBL: UILabel!
    
    var eventList = [EventListModel]()
    var isReloadRequired = true
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if isReloadRequired
        { webCall()
          isReloadRequired = false
        }
    }
    
    func webCall()
    {
        self.eventList.removeAll()
        Webservices.getMethod(url: "ExhibizWeb/backend/api/events/"+String(describing: UserDefaults.standard.string(forKey: "UserID")!)+"?type=4")
        { (isFetched, resultData) in
            if isFetched
            {
               // print("ExhibizWeb/backend/api/events/"+String(describing: UserDefaults.standard.string(forKey: "UserID")!)+"?type=4")
              //  print(resultData)
                let item = (resultData["Data"] as! [[String:Any]])[0]
                
//                for item in item["past"] as! [[String:Any]]
//                {   let eventListModel = EventListModel()
//                    self.eventList.append(eventListModel.createModel(data: item))
//                }
                
                for item in item["present"] as! [[String:Any]]
                {   let eventListModel = EventListModel()
                    self.eventList.append(eventListModel.createModel(data: item))
                }
                
                for item in item["future"] as! [[String:Any]]
                {   let eventListModel = EventListModel()
                    self.eventList.append(eventListModel.createModel(data: item))
                }
                self.contentTV.reloadData()
            }
        }
    }
    
    @IBAction func sideMenuTapped(_ sender: UIButton)
    {
        let sideMenuVC = self.storyboard!.instantiateViewController(withIdentifier: "EXExhibitorSideMenuVC") as! EXExhibitorSideMenuVC
        let SideMenuController = UISideMenuNavigationController(rootViewController: sideMenuVC)
        SideMenuController.navigationBar.isHidden = true
        SideMenuController.leftSide = true
        self.present(SideMenuController, animated: true, completion: nil)
    }
    
    @IBAction func filterBTNTapped(_ sender: UIButton)
    {
        let optionMenu = UIAlertController(title: "Choose Type of event", message: nil, preferredStyle: .actionSheet)
        
        let allEvents = UIAlertAction(title: "All Events", style: .default, handler:
        { (alert: UIAlertAction!) -> Void in
        })
        
        let exhibition = UIAlertAction(title: "Exhibition", style: .default, handler:
        { (alert: UIAlertAction!) -> Void in
        })
        
        let careerFair = UIAlertAction(title: "Career Fair", style: .default, handler:
        { (alert: UIAlertAction!) -> Void in
        })
        
        let conference = UIAlertAction(title: "Conference", style: .default, handler:
        { (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(exhibition)
        optionMenu.addAction(careerFair)
        optionMenu.addAction(conference)
        optionMenu.addAction(allEvents)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
}

// TableView Delegates
extension EXMySpacesVC:UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if eventList.isEmpty
        { noEventsLBL.isHidden = false
        }
        else
        { noEventsLBL.isHidden = true
        }
        return eventList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventsCell") as! EXEventListCell
        cell.titleLBL.text = eventList[indexPath.row].title
        cell.dateLBL.text = eventList[indexPath.row].day
        cell.monthLBL.text = String(eventList[indexPath.row].month.prefix(3))
        cell.placeLBL.text = eventList[indexPath.row].address
        //        cell.priceBTN.setTitle(eventList[indexPath.row].price, for: .normal)
        
        cell.bannerTagBTN.setTitle(eventList[indexPath.row].category, for: .normal)
        let url = URL(string: "\(baseURL)\(imageURL)\(eventList[indexPath.row].eventImages[0])")
        cell.eventImage.kf.setImage(with: url, placeholder: UIImage(named: "noimage.jpg"))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: false)
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "EXExEventDetailPage") as! EXExEventDetailPage
        nextVC.eventDetails = eventList[indexPath.row]
        nextVC.isFromMySpace = true
        self.present(nextVC, animated: true, completion: nil)
    }
}
