//
//  EXMyExhibitionsVC.swift
//  Exhibiz
//
//  Created by Appzoc on 14/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import SideMenu

class EXMyExhibitionsVC: UIViewController
{
    @IBOutlet var navigationTitleLBL: UILabel!
    @IBOutlet var slideView: UIView!
    @IBOutlet var liveBTN: UIButton!
    @IBOutlet var contentTV: UITableView!
    @IBOutlet var noEventsLBL: UILabel!
    
    var pastEventArray = [EventListModel]()
    var presentEventArray = [EventListModel]()
    var futureEventArray = [EventListModel]()
    var contentArray = [EventListModel]()
    var selectedTab = 1
    
    var isReloadRequired = true
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if isReloadRequired
        {
          setInitialFrame()
          webCall()
          isReloadRequired = false
        }
    }
    
    func webCall()
    {
        Webservices.getMethod(url: "ExhibizWeb/backend/api/events/"+String(describing: UserDefaults.standard.string(forKey: "UserID")!)+"?type=4")
        { (isFetched, resultData) in
            if isFetched
            {
                //print("ExhibizWeb/backend/api/events/"+String(describing: UserDefaults.standard.string(forKey: "UserID")!)+"?type=4")
                //print(resultData)
                let item = (resultData["Data"] as! [[String:Any]])[0]
                
                for item in item["past"] as! [[String:Any]]
                {   let eventListModel = EventListModel()
                    self.pastEventArray.append(eventListModel.createModel(data: item))
                }
                
                for item in item["present"] as! [[String:Any]]
                {   let eventListModel = EventListModel()
                    self.presentEventArray.append(eventListModel.createModel(data: item))
                }
                
                for item in item["future"] as! [[String:Any]]
                {   let eventListModel = EventListModel()
                    self.futureEventArray.append(eventListModel.createModel(data: item))
                }
                self.contentArray = self.presentEventArray
                self.contentTV.reloadData()
            }
        }
    }
    
    
    @IBAction func liveBTNTapped(_ sender: UIButton)
    { animateView(sender: sender)
      selectedTab = 1
      self.contentArray = self.presentEventArray
      self.contentTV.reloadData()
    }
    
    @IBAction func futureBTNTapped(_ sender: UIButton)
    { animateView(sender: sender)
      selectedTab = 2
      self.contentArray = self.futureEventArray
      self.contentTV.reloadData()
    }
    
    @IBAction func pastBTNTapped(_ sender: UIButton)
    { animateView(sender: sender)
      selectedTab = 3
      self.contentArray = self.pastEventArray
      self.contentTV.reloadData()
    }
    
    @IBAction func sideMenuTapped(_ sender: UIButton)
    {
        let sideMenuVC = self.storyboard!.instantiateViewController(withIdentifier: "EXExhibitorSideMenuVC") as! EXExhibitorSideMenuVC
        
        let SideMenuController = UISideMenuNavigationController(rootViewController: sideMenuVC)
        SideMenuController.navigationBar.isHidden = true
        SideMenuController.leftSide = true
        
        self.present(SideMenuController, animated: true, completion: nil)
    }
    
    @IBAction func filterBTNTapped(_ sender: UIButton)
    {
        let optionMenu = UIAlertController(title: "Choose Type of event", message: nil, preferredStyle: .actionSheet)
        
        let allEvents = UIAlertAction(title: "All Events", style: .default, handler:
        { (alert: UIAlertAction!) -> Void in
        })
        
        let exhibition = UIAlertAction(title: "Exhibition", style: .default, handler:
        { (alert: UIAlertAction!) -> Void in
        })
        
        let careerFair = UIAlertAction(title: "Career Fair", style: .default, handler:
        { (alert: UIAlertAction!) -> Void in
        })
        
        let conference = UIAlertAction(title: "Conference", style: .default, handler:
        { (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(exhibition)
        optionMenu.addAction(careerFair)
        optionMenu.addAction(conference)
        optionMenu.addAction(allEvents)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    @IBAction func addEventBTNTapped(_ sender: UIButton)
    { let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXEventSelectVC")
      self.present(nextVC, animated: true, completion: nil)
    }
}

// Commom Function
extension EXMyExhibitionsVC
{
    func animateView(sender:UIButton)
    { UIView.animate(withDuration: 0.5, animations:{ self.slideView.frame = CGRect(x: sender.frame.minX, y: sender.frame.maxY, width: sender.frame.width,
                                                                                   height: sender.frame.height)})
    }
    
    func setInitialFrame()
    { self.slideView.frame = CGRect(x: liveBTN.frame.minX, y: liveBTN.frame.maxY, width: liveBTN.frame.width,
                                    height: liveBTN.frame.height)
    }
}

// TableView Delegates
extension EXMyExhibitionsVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if contentArray.isEmpty
        { noEventsLBL.isHidden = false
        }
        else
        { noEventsLBL.isHidden = true
        }
        return contentArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventCell") as! EXEventListCell
        cell.titleLBL.text = contentArray[indexPath.row].title
        cell.dateLBL.text = contentArray[indexPath.row].day
        cell.monthLBL.text = String(contentArray[indexPath.row].month.prefix(3))
        cell.placeLBL.text = contentArray[indexPath.row].address
        
        cell.bannerTagBTN.setTitle(contentArray[indexPath.row].category, for: .normal)
        
        let url = URL(string: "\(baseURL)\(imageURL)\(contentArray[indexPath.row].eventImages[0])")
        cell.eventImage.kf.setImage(with: url, placeholder: UIImage(named: "noimage.jpg"))
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    { return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: false)
        let storyBoard = UIStoryboard(name: "EXExhibitor", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXExExhibitionListVC") as! EXExExhibitionListVC
        
        if selectedTab == 1
        { nextVC.eventID = presentEventArray[indexPath.row].id
        nextVC.detailPageFrom = DetailPageFrom.myEventsPresent
        nextVC.categoryID = presentEventArray[indexPath.row].categoryID
        }
        if selectedTab == 2
        { nextVC.eventID = futureEventArray[indexPath.row].id
        nextVC.detailPageFrom = DetailPageFrom.myEventsFuture
        nextVC.categoryID = futureEventArray[indexPath.row].categoryID
        }
        if selectedTab == 3
        { nextVC.eventID = pastEventArray[indexPath.row].id
        nextVC.detailPageFrom = DetailPageFrom.myEventsPast
        nextVC.categoryID = pastEventArray[indexPath.row].categoryID
        }
        nextVC.location = contentArray[indexPath.row].address
        nextVC.sponcers = contentArray[indexPath.row].sponers
        self.present(nextVC, animated: true, completion: nil)
    }
    
}
