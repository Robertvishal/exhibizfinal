//
//  EXImageViewerVC.swift
//  Exhibiz
//
//  Created by Appzoc on 01/08/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit

class EXImageViewerVC: UIViewController {
    
    var sourceType:SourceType = .link
    
    var imageSource:[UIImage] = []
    var linkSource:[String] = []
    
    @IBOutlet var collectionRef: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func closeBTNTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}


extension EXImageViewerVC: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch sourceType {
            case .link:
                return linkSource.count
            case .image:
                return imageSource.count
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EXImageViewerCVC", for: indexPath) as! EXImageViewerCVC
        switch sourceType {
        case .link:
            let imgURL = URL(string: baseURLImage + "\(linkSource[indexPath.row])")
            cell.imageView.kf.setImage(with: imgURL, placeholder: UIImage(named: "noimage.jpg"))
        case .image:
            cell.imageView.image = imageSource[indexPath.row]
        }
        return cell
    }
}


extension EXImageViewerVC{
    enum SourceType{
        case link
        case image
    }
}


class EXImageViewerCVC: UICollectionViewCell{
    @IBOutlet var imageView: UIImageView!
}




