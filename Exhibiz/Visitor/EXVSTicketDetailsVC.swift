//
//  EXVSTicketDetailsVC.swift
//  Exhibiz
//
//  Created by Appzoc on 05/03/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import ABSteppedProgressBar


class EXTicketDetailsCVC: UICollectionViewCell
{
    @IBOutlet var nameTF: UITextField!
    @IBOutlet var ageTF: UITextField!
    @IBOutlet var emailTF: UITextField!
    @IBOutlet var mobileTF: UITextField!
    @IBOutlet var maleBTN: BaseButton!
    @IBOutlet var femaleBTN: BaseButton!
    

    @IBAction func maleBTNTapped(_ sender: BaseButton)
    {
        maleBTN.backgroundColor = blueColor
        maleBTN.setTitleColor(UIColor.white, for: .normal)
        femaleBTN.backgroundColor = UIColor.white
        femaleBTN.setTitleColor(UIColor.darkGray, for: .normal)
    }
    @IBAction func femaleBTNTapped(_ sender: BaseButton)
    {
        femaleBTN.backgroundColor = blueColor
        femaleBTN.setTitleColor(UIColor.white, for: .normal)
        maleBTN.backgroundColor = UIColor.white
        maleBTN.setTitleColor(UIColor.darkGray, for: .normal)
    }
    
}

class TicketDetail
{
    var name = ""
    var age = ""
    var email = ""
    var mobile = ""
    var isMale = true
}


class EXVSTicketDetailsVC: UIViewController
{
    @IBOutlet var contentCV: UICollectionView!
    @IBOutlet var stepperView: ABSteppedProgressBar!
    @IBOutlet var contentSV: UIScrollView!
    @IBOutlet var nextBTN: BaseButton!
    
    
    var ticketNumber = 0
    var ticketArray = [TicketDetail]()
    var eventID = ""
    var ticketAmount = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        contentCV.shouldIgnoreScrollingAdjustment = true
        stepperView.numberOfPoints = ticketNumber
        
        for _ in 1...ticketNumber
        { ticketArray.append(TicketDetail())
        }
        contentCV.reloadData()
    }
    
    @IBAction func nameChanged(_ sender: UITextField)
    { ticketArray[sender.tag].name = sender.text!
    }
    
    @IBAction func ageChanged(_ sender: UITextField)
    { ticketArray[sender.tag].age = sender.text!
    }
    
    @IBAction func emailChanged(_ sender: UITextField)
    { ticketArray[sender.tag].email = sender.text!
    }
    
    @IBAction func mobileChanged(_ sender: UITextField)
    { ticketArray[sender.tag].mobile = sender.text!
    }
    
    @IBAction func maleBTNTapped(_ sender: BaseButton)
    { ticketArray[sender.tag].isMale = true
    }
    
    @IBAction func femaleBTNTapped(_ sender: BaseButton)
    { ticketArray[sender.tag].isMale = false
    }
    
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nextBTNTapped(_ sender: UIButton)
    {
        if isValid(index: stepperView.currentIndex)
        {
            if stepperView.currentIndex == ticketNumber-2
            {
                nextBTN.setTitle("Pay Now", for: .normal)
                stepperView.currentIndex = stepperView.currentIndex+1
                contentCV.scrollToItem(at: IndexPath(item: stepperView.currentIndex, section: 0), at: .centeredHorizontally, animated: true)
            }
            else if stepperView.currentIndex < ticketNumber-1
            {
                nextBTN.setTitle("Next", for: .normal)
                stepperView.currentIndex = stepperView.currentIndex+1
                contentCV.scrollToItem(at: IndexPath(item: stepperView.currentIndex, section: 0), at: .centeredHorizontally, animated: true)
            }
            else
            {
                var ticketPostArray = [[String:String]]()
                for items in ticketArray
                {
                    ticketPostArray.append(["name":items.name,
                                            "age":items.age,
                                            "email":items.email,
                                            "mobile":items.mobile,
                                            "gender":items.isMale ? "M" : "F",
                                            "created_at":getCreatedDate()])
                }
                print(ticketPostArray)
                
                //let userdetails = ["userdetails":ticketPostArray]
                
                var parameter = [String:String]()
                
                if let data = try? JSONSerialization.data(withJSONObject: ticketPostArray, options: []),
                    let str = String(data: data, encoding: .utf8)
                {
                    print(str)
                    parameter["userdetails"] = str
                }
                
                parameter["event_id"] = self.eventID
                parameter["exhibition_id"] = "0"
                parameter["tickettype"] = self.ticketAmount == "0" ? "0" : "1"
                let intPrice = Int(Float(self.ticketAmount)!) * 100
                parameter["ticketamount"] = String(intPrice)
                parameter["description"] = "Ticket Booking"
                parameter["status"] = self.ticketAmount == "0" ? "1" : "0"
                
                print(parameter)
                Webservices.postMethod(url: "ExhibizWeb/backend/api/ticket_booking/\(UserDefaults.standard.string(forKey: "UserID")!)",
                                       parameter: parameter, CompletionHandler:
                    { (isFetched, resultData) in
                        if isFetched
                        {
                            if parameter["tickettype"] == "0"
                            {
                                self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                            }
                            else
                            {
                                let data = resultData["Data"] as? [String:Any] ?? [String:Any]()
                                let url = data["url"] as? String ?? ""
                                let nextVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EXPaymentVC") as! EXPaymentVC
                                nextVc.url = url
                                nextVc.paymentFrom = .ticketBookingVS
                                self.present(nextVc, animated: true, completion: nil)
                            }
                        }
                        else
                        {
                            
                            
                        }
                })
            }
        }
    }
    
    
    func dismissToList()
    {
        let alert = UIAlertController(title: "Ticket has been booked",
                                      message: "You can view your tickets from My Events",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok",
                                      style: .default,
                                      handler:
            { _ in
                self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func isValid(index:Int) -> Bool
    {
        if ticketArray[index].name.isEmpty
        { bannerWithMessage(message: "Enter Name")
          return false
        }
        else if ticketArray[index].age.isEmpty
        { bannerWithMessage(message: "Enter Age")
          return false
        }
        else if ticketArray[index].email.isEmpty
        {
            bannerWithMessage(message: "Enter Email")
            return false
        }
        else if !BaseValidator.isValid(email: ticketArray[index].email)
        {
            bannerWithMessage(message: "Enter valid Email")
            return false
        }
        else if ticketArray[index].mobile.isEmpty
        {
            bannerWithMessage(message: "Enter Mobile")
          return false
        }
        else
        {
            
            if BaseValidator.isValid(digits: ticketArray[index].age) {
                
                if BaseValidator.isValid(digits: ticketArray[index].mobile) {
                    return true
                }else {
                    bannerWithMessage(message: "Enter Valid Mobile")
                    return false
                }

            }else {
                bannerWithMessage(message: "Enter Valid Age")
                return false

            }
            

        }
        
    }
    
}


// Collection View Delegates
extension EXVSTicketDetailsVC:UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return ticketNumber
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! EXTicketDetailsCVC
       cell.nameTF.text = ticketArray[indexPath.item].name
       cell.ageTF.text = ticketArray[indexPath.item].age
       cell.emailTF.text = ticketArray[indexPath.item].email
       cell.mobileTF.text = ticketArray[indexPath.item].mobile
        
       if ticketArray[indexPath.item].isMale
       { cell.maleBTN.backgroundColor = blueColor
         cell.maleBTN.setTitleColor(UIColor.white, for: .normal)
         cell.femaleBTN.backgroundColor = UIColor.white
         cell.femaleBTN.setTitleColor(UIColor.darkGray, for: .normal)
       }
       else
       { cell.femaleBTN.backgroundColor = blueColor
         cell.femaleBTN.setTitleColor(UIColor.white, for: .normal)
         cell.maleBTN.backgroundColor = UIColor.white
         cell.maleBTN.setTitleColor(UIColor.darkGray, for: .normal)
       }

       cell.nameTF.tag = indexPath.item
       cell.ageTF.tag = indexPath.item
       cell.emailTF.tag = indexPath.item
       cell.mobileTF.tag = indexPath.item
       cell.maleBTN.tag = indexPath.item
       cell.femaleBTN.tag = indexPath.item
       return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: contentCV.bounds.width, height: contentCV.bounds.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        stepperView.currentIndex = indexPath.item
    }
}


