//
//  EXCareerFairDetailVC.swift
//  Exhibiz
//
//  Created by Appzoc on 22/03/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import ImageSlideshow
import Kingfisher

class EXCareerFairDetailVC: UIViewController
{
    @IBOutlet var slideShowView: ImageSlideshow!
    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var descriptionLBL: UILabel!
    @IBOutlet var addressLBL: UILabel!
    @IBOutlet var startDayLBL: UILabel!
    @IBOutlet var endDayLBL: UILabel!
    @IBOutlet var startDateLBL: UILabel!
    @IBOutlet var endDateLBL: UILabel!
    @IBOutlet var startingDayLBL: UILabel!
    @IBOutlet var endingDayLBL: UILabel!
    @IBOutlet var startTimeLBL: UILabel!
    @IBOutlet var endTimeLBL: UILabel!
    @IBOutlet var locationLBL: UILabel!
    @IBOutlet var emailLBL: UILabel!
    @IBOutlet var keywordsLBL: UILabel!
    @IBOutlet var eventTypeLBL: UILabel!
    @IBOutlet var ticketTypeLBL: UILabel!
    @IBOutlet var ticketPriceLBL: UILabel!
    
    @IBOutlet var buyTicketHeight: NSLayoutConstraint!
    @IBOutlet var cancelTicketHeight: NSLayoutConstraint!
    @IBOutlet var callRateBTN: UIButton!
    
    var detailPageFrom = DetailPageFrom.allEvents
    var exhibitionDetail:ExhibitionListModel!
    var location = ""
    var sponcers = [AssignedMemberModel]()
    var categoryID = 0
    
    @IBOutlet var vacanciesTV: UITableView!
    @IBOutlet var vacanciesHeight: NSLayoutConstraint!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        var imageSourceArray = [KingfisherSource]()
        for item in exhibitionDetail.exhibitionImages
        {
            imageSourceArray.append(KingfisherSource(urlString: "\(baseURL)\(imageURL)\(item)")!)
        }
        slideShowView.contentScaleMode = .scaleAspectFill
        slideShowView.setImageInputs(imageSourceArray)
        slideShowView.slideshowInterval = 2
        
        titleLBL.text = exhibitionDetail.title
        descriptionLBL.text = exhibitionDetail.description
        addressLBL.text = self.location
        startDayLBL.text = exhibitionDetail.startDate2.day
        endDayLBL.text = exhibitionDetail.endDate2.day
        startDateLBL.text = exhibitionDetail.startDate2.date
        endDateLBL.text = exhibitionDetail.endDate2.date
        //        startingDayLBL.text =
        //        endingDayLBL.text =
        startTimeLBL.text = exhibitionDetail.startTime
        endTimeLBL.text = exhibitionDetail.endTime
        locationLBL.text = exhibitionDetail.details.website
        emailLBL.text = exhibitionDetail.details.email
        keywordsLBL.text = String(exhibitionDetail.tags)
        setEventType()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        vacanciesHeight.constant = vacanciesTV.contentSize.height
    }
    
    func setEventType()
    {
        if self.categoryID == 1
        {
            self.eventTypeLBL.text = "Conference"
        }
        if self.categoryID == 2
        {
            self.eventTypeLBL.text = "Career Fair"
        }
        if self.categoryID == 3
        {
            self.eventTypeLBL.text = "Exhibition"
        }
    }
    
    
    func setTicketType()
    {   if String(exhibitionDetail.ticketType) == "1"
    { ticketTypeLBL.text = "Paid"
    }
    else
    { ticketTypeLBL.text = "Free"
        }
    }
    
    func setTicketPrice()
    {
        if exhibitionDetail.price == ""
        { ticketPriceLBL.text = "0"
        }
        else
        { ticketPriceLBL.text = exhibitionDetail.price
        }
    }
    
    @IBAction func callBTNPressed(_ sender: UIButton)
    {
        if sender.backgroundImage(for: .normal) == #imageLiteral(resourceName: "ratingStar")
        {
            
        }
        else
        {
            if let url = URL(string: "tel://\(exhibitionDetail.details.phone)"), UIApplication.shared.canOpenURL(url)
            {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }

}

// CollectionView Delegates
extension EXCareerFairDetailVC:UICollectionViewDataSource,UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return sponcers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! sponcerCellCVC
        let url = URL(string: "\(baseURL)\(imageURL)\(sponcers[indexPath.row].image)")
        cell.sponcerImage.kf.setImage(with: url, placeholder: UIImage(named: "noimage.jpg"))
        return cell
    }
}

// TableView Delegates
extension EXCareerFairDetailVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.exhibitionDetail.details.career.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! uploadCareerFairCell
        cell.positionLBL.text = self.exhibitionDetail.details.career[indexPath.row].name
        cell.vacancyLBL.text = self.exhibitionDetail.details.career[indexPath.row].vacany
        return cell
    }
}







