//
//  EXRateDetailVC.swift
//  Exhibiz
//
//  Created by Appzoc on 11/04/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import Kingfisher

class EXRateDetailVC: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    @IBOutlet var contentTV: UITableView!
    
    var eventDetails = EventListModel()
    var isReloadRequired = true
    var contentArray = [[String:Any]]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if isReloadRequired
        {
            self.isReloadRequired = false
            Webservices.getMethod(url: "ExhibizWeb/backend/api/rate/event/\(eventDetails.id)")
            { (isFetched, resultData) in
                if isFetched
                {
                    print(resultData)
                    self.contentArray = resultData["Data"] as? [[String:Any]] ?? [[String:Any]]()
                    self.contentTV.reloadData()
                }
                else
                {
                }
            }
        }
    }
    
    @IBAction func rateBTNTapped(_ sender: UIButton)
    {
        let popUpVC = self.storyboard!.instantiateViewController(withIdentifier: "EXRatePopUpVC") as! EXRatePopUpVC
        popUpVC.eventID = self.eventDetails.id
        self.present(popUpVC, animated: true, completion: nil)
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return contentArray.count+1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! rateCell1
            cell.eventTitle.text = eventDetails.title
            cell.eventDescription.text = eventDetails.description
            let url = URL(string: "\(baseURL)\(imageURL)\(eventDetails.eventImages[0])")
            cell.eventImage.kf.setImage(with: url, placeholder: UIImage(named: "noimage.jpg"))
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! rateCell2
            cell.userName.text = (contentArray[indexPath.row-1])["username"] as? String ?? ""
            cell.commentLBL.text = (contentArray[indexPath.row-1])["review"] as? String ?? ""
            cell.ratingView.rating = Double((contentArray[indexPath.row-1])["rating"] as? String ?? "0")!
            let url = URL(string: "\(baseURL)\(imageURL)\((contentArray[indexPath.row-1])["image"] as? String ?? "")")
            cell.userImage.kf.setImage(with: url, placeholder: UIImage(named: "noProfilePic.png"))

            return cell
        }
    }
}


class rateCell1: UITableViewCell
{
    @IBOutlet var eventImage: BaseImageView!
    @IBOutlet var eventTitle: UILabel!
    @IBOutlet var eventDescription: UILabel!
}

class rateCell2: UITableViewCell
{
    @IBOutlet var userName: UILabel!
    @IBOutlet var commentLBL: UILabel!
    @IBOutlet var userImage: BaseImageView!
    @IBOutlet var ratingView: FloatRatingView!
}
