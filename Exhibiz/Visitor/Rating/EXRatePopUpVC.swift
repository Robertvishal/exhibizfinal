//
//  EXRatePopUpVC.swift
//  Exhibiz
//
//  Created by Appzoc on 11/04/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import PlaceholderTextView

class EXRatePopUpVC: UIViewController
{
    @IBOutlet var ratingView: FloatRatingView!
    @IBOutlet var commentTextView: PlaceholderTextView!
    
    var eventID = 0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    @IBAction func backgroundTapped(_ sender: UITapGestureRecognizer)
    { self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneBTNTapped(_ sender: BaseButton)
    {
        if commentTextView.text.isEmpty
        {
            bannerWithMessage(message: "Enter your comments")
        }
        else
        {
            webCall()
        }
    }
    
    func webCall()
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        var parameter = [String:String]()
        parameter["event_id"] = String(self.eventID)
        parameter["review"] = commentTextView.text
        parameter["rating"] = String(Int(ratingView.rating))
        parameter["user_id"] = UserDefaults.standard.string(forKey: "UserID")
        parameter["created_at"] = dateFormatter.string(from:  Date())
        
        print(parameter)
        Webservices.postMethod(url: "ExhibizWeb/backend/api/rate/event", parameter: parameter)
        { (isFetched, result) in
            if isFetched
            {
                let previousVC = self.presentingViewController as! EXRateDetailVC
                previousVC.isReloadRequired = true
                previousVC.viewWillAppear(true)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    
}
