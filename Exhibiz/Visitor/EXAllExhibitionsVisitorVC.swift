//
//  EXAllExhibitionsVisitorVC.swift
//  Exhibiz
//
//  Created by Appzoc on 14/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import SideMenu

class EXAllExhibitionsVisitorVC: UIViewController
{
    
    @IBOutlet var navigationTitleLBL: UILabel!
    @IBOutlet var contentTV: UITableView!
    var eventID = 0
    var userID = UserDefaults.standard.string(forKey: "UserID")
    var eventList = [ExhibitionListModel]()
    var location = ""
    var sponcers = [AssignedMemberModel]()
    var detailPageFrom = DetailPageFrom.allEvents
    var eventDetails = EventListModel()
    var categoryID = 0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.contentTV.contentInset = UIEdgeInsetsMake(10, 0, 0, 0)
        //print(allExhibitionsList+"\(eventID)/\(String(describing: userID!))")
        
        if categoryID == 2
        { navigationTitleLBL.text = "Companies"
        }
        if categoryID == 3
        { navigationTitleLBL.text = "Exhibitions"
        }
        
        Webservices.getMethod(url: allExhibitionsList+"\(eventID)/0")
        { (isFetched, resultData) in
            if isFetched
            {
                for item in resultData["Data"] as! [[String:Any]]
                {
                    self.eventList.append(ExhibitionListModel(data: item))
                    debugPrint(self.eventList)
                }
                self.contentTV.reloadData()
            }
            else
            {   self.dismiss(animated: false, completion: nil)
            }
        }
    }
    
    @IBAction func sideMenuTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func searchBTNTapped(_ sender: UIButton)
    {
    }
    
}

// TableView Delegates
extension EXAllExhibitionsVisitorVC:UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return eventList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventsCell") as! EXEventListCell
        cell.titleLBL.text = eventList[indexPath.row].title
        cell.dateLBL.text = eventDetails.day
        cell.monthLBL.text = String(eventDetails.month.prefix(3))
        cell.placeLBL.text = self.location
        let price = eventList[indexPath.row].price == "" ? "Free" : "SR "+eventList[indexPath.row].price
        cell.priceBTN.setTitle(price, for: .normal)
        
        if eventList[indexPath.row].exhibitionImages.indices.contains(0)
        { let url = URL(string: "\(baseURL)\(imageURL)\(eventList[indexPath.row].exhibitionImages[0])")
          cell.eventImage.kf.setImage(with: url, placeholder: UIImage(named: "noimage.jpg"))
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if self.categoryID == 2 // CareerFair
        {
            tableView.deselectRow(at: indexPath, animated: false)
            let storyBoard = UIStoryboard(name: "EXVisitor", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXCareerFairDetailVC") as! EXCareerFairDetailVC
            nextVC.exhibitionDetail = eventList[indexPath.row]
            nextVC.location = self.location
            nextVC.sponcers = self.sponcers
            nextVC.detailPageFrom = self.detailPageFrom
            nextVC.categoryID = self.categoryID
            self.present(nextVC, animated: true, completion: nil)
        }
        if self.categoryID == 3 // Exhibition
        {
            tableView.deselectRow(at: indexPath, animated: false)
            let storyBoard = UIStoryboard(name: "EXVisitor", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXExhibitionDetailVC") as! EXExhibitionDetailVC
            nextVC.exhibitionDetail = eventList[indexPath.row]
            nextVC.location = self.location
            nextVC.sponcers = self.sponcers
            nextVC.detailPageFrom = self.detailPageFrom
            nextVC.categoryID = self.categoryID
            self.present(nextVC, animated: true, completion: nil)
        }
    }
}
