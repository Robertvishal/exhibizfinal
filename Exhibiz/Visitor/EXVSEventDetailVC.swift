//
//  EXVSEventDetailVC.swift
//  Exhibiz
//
//  Created by Appzoc on 28/03/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import ImageSlideshow
import Kingfisher

class EXVSEventDetailVC: UIViewController
{
    @IBOutlet var organisersCV: UICollectionView!
    @IBOutlet var sponsorsCV: UICollectionView!
    @IBOutlet var slotCV: UICollectionView!
    
    @IBOutlet var slideShowView: ImageSlideshow!
    @IBOutlet var ratingBTN: BaseButton!
    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var descriptionLBL: UILabel!
    @IBOutlet var addressLBL: UILabel!
    @IBOutlet var fromDay: UILabel!
    @IBOutlet var toDay: UILabel!
    @IBOutlet var fromDateLBL: UILabel!
    @IBOutlet var toDateLBL: UILabel!
    @IBOutlet var eventTypeLBL: UILabel!
    @IBOutlet var slotNoLBL: UILabel!
    @IBOutlet var sadadNoLBL: UILabel!
    @IBOutlet var paidBTN: BaseButton!
    @IBOutlet var freeBTN: BaseButton!
    @IBOutlet var priceAmtLBL: UILabel!
    
    @IBOutlet var exhibitionsTitleLBL: UILabel!
    @IBOutlet var ticketPriceViewHeight: NSLayoutConstraint!
    @IBOutlet var slotCVHeight: NSLayoutConstraint!
    @IBOutlet var cancelTicketViewHeight: NSLayoutConstraint!
    @IBOutlet var buyTicketHeight: NSLayoutConstraint!
    
    var detailPageFrom = DetailPageFrom.allEvents
    
    var isFromMyEvents = false
    var eventDetails = EventListModel()
    var imageLinkArray:[String] = []
    
    
    @IBOutlet weak var ticketView: UIView!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        var imageSourceArray = [KingfisherSource]()
        for item in eventDetails.eventImages
        {
            imageSourceArray.append(KingfisherSource(urlString: "\(baseURL)\(imageURL)\(item)", placeholder:UIImage(named: "noimage.jpg"))!)
            imageLinkArray.append(item)
        }
        slideShowView.contentScaleMode = .scaleAspectFill
        slideShowView.setImageInputs(imageSourceArray)
        slideShowView.slideshowInterval = 2
        
        titleLBL.text = eventDetails.title
        descriptionLBL.text = eventDetails.description
        addressLBL.text = eventDetails.address
        //        fromDay.text = eventDetails.title
        //        toDay.text = eventDetails.title
        fromDateLBL.text = eventDetails.startDate
        toDateLBL.text = eventDetails.endDate
        //        eventTypeLBL.text = eventDetails.title
        slotNoLBL.text = String(eventDetails.slots.count)
        
        
        
//        sadadNoLBL.text = eventDetails.sadadnumber
        self.setUpTapGesture()
        self.priceAmtLBL.text = eventDetails.price
        self.setEventType()
        self.setPaidView()
        self.setBuyCancelTicketView()
        self.setExhibitionTitle()
    }
    
    @IBAction func FloormapBtnTapped(_ sender: UIButton) {
        
        let storyBoard = UIStoryboard(name: "SlotBookingWebview", bundle: nil)
        
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXSlotBookingWebviewVC") as! EXSlotBookingWebviewVC
        
        self.present(nextVC, animated: true, completion: nil)

        
        
    }
    
    
    
    func setExhibitionTitle()
    {
        if eventDetails.categoryID == 2
        { exhibitionsTitleLBL.text = "Companies"
        }
        if eventDetails.categoryID == 3
        { exhibitionsTitleLBL.text = "Exhibitions"
        }
    }
    
    func setUpTapGesture(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        slideShowView.addGestureRecognizer(tapGesture)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer){
        print("Gesture Tapped")
        
        slideShowView.presentFullScreenController(from: self)
        
//        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EXImageViewerVC") as! EXImageViewerVC
//        vc.sourceType = .link
//        vc.linkSource = imageLinkArray
//        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func gotoMapTapped(_ sender: UIButton) {
        self.openInMap()
    }
    
    func openInMap(){
        let latitude = eventDetails.lat
        let longitude = eventDetails.lon
        let mapAppURL = "comgooglemaps://?center=\(latitude),\(longitude)&zoom=18"
        let browserURL = "https://www.google.com/maps/@\(latitude),\(longitude),18z"
        if let _ = URL(string: "comgooglemaps://"), UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!){
            UIApplication.shared.openURL(URL(string: "\(mapAppURL)")!)
        }else if UIApplication.shared.canOpenURL(URL(string: "\(browserURL)")!){
            UIApplication.shared.openURL(URL(string: "\(browserURL)")!)
        }
    }
    
    func setPaidView()
    {
        if eventDetails.ticketType == 1
        {
            ticketPriceViewHeight.constant = 70
            paidBTN.backgroundColor = blueColor
            paidBTN.setTitleColor(UIColor.white, for: .normal)
            freeBTN.backgroundColor = UIColor.white
            freeBTN.setTitleColor(UIColor.darkGray, for: .normal)
        }
        else
        {
            ticketPriceViewHeight.constant = 0
            freeBTN.backgroundColor = blueColor
            freeBTN.setTitleColor(UIColor.white, for: .normal)
            paidBTN.backgroundColor = UIColor.white
            paidBTN.setTitleColor(UIColor.darkGray, for: .normal)
        }
    }
    
    func setBuyCancelTicketView()
    {
        if isFromMyEvents
        {   self.cancelTicketViewHeight.constant = 100
            self.buyTicketHeight.constant = 0
        }
        else
        {   self.cancelTicketViewHeight.constant = 0
            self.buyTicketHeight.constant = 48
        }
    }
    
    func setEventType()
    {
        if eventDetails.categoryID == 1
        { eventTypeLBL.text = "Conference"
        }
        if eventDetails.categoryID == 2
        { eventTypeLBL.text = "Career Fair"
        }
        if eventDetails.categoryID == 3
        { eventTypeLBL.text = "Exhibition"
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
//        slotCVHeight.constant = slotCV.contentSize.height
        
        print(eventDetails.endDate)
        
        slotCVHeight.constant = 0
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let todayy = Date()
        //let now = formatter.string(from: todayy)
        
        if let EndDate = formatter.date(from: eventDetails.endDate) {
            if todayy > EndDate
            {
                buyTicketHeight.constant = 0
                cancelTicketViewHeight.constant = 0
            }else {
                
            }
        }else {
            formatter.dateFormat = "dd-MM-yyyy"
            let todayy = Date()
            let EndDate = formatter.date(from: eventDetails.endDate)
            
            if todayy > EndDate!
            {
                buyTicketHeight.constant = 0
                cancelTicketViewHeight.constant = 0
            }else {
                
            }

        }
        
        
        

        
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func viewAllExhibTapped(_ sender: BaseButton)
    {
        let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXAllExhibitionsVisitorVC") as! EXAllExhibitionsVisitorVC
        nextVC.eventID = eventDetails.id
        nextVC.location = eventDetails.address
        nextVC.sponcers = eventDetails.sponers
        nextVC.eventDetails = eventDetails
        nextVC.categoryID = eventDetails.categoryID
        Webservices.getMethod(url: allExhibitionsList+"\(eventDetails.id)/0")
        { (isFetched, resultData) in
            if isFetched
            { self.present(nextVC, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func ratingBTNTapped(_ sender: BaseButton)
    {
        let rateVC = self.storyboard!.instantiateViewController(withIdentifier: "EXRateDetailVC") as! EXRateDetailVC
        rateVC.eventDetails = self.eventDetails
        self.present(rateVC, animated: true, completion: nil)
    }
    
    
    @IBAction func buyTicketBTNTapped(_ sender: BaseButton)
    {
        let storyBoard = UIStoryboard(name: "EXVisitor", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXVSTicketSelectVC") as! EXVSTicketSelectVC
        nextVC.eventDetails = self.eventDetails
        self.present(nextVC, animated: true, completion: nil)
    }
    
    @IBAction func bookTicketBTNTapped(_ sender: BaseButton)
    {
        let storyBoard = UIStoryboard(name: "EXVisitor", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXVSTicketSelectVC") as! EXVSTicketSelectVC
        nextVC.eventDetails = self.eventDetails
        self.present(nextVC, animated: true, completion: nil)
    }
    
    @IBAction func cancelTicketBTNTapped(_ sender: BaseButton)
    {
        let storyBoard = UIStoryboard(name: "EXVisitor", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXVSCancelTicket") as! EXVSCancelTicket
        nextVC.eventID = self.eventDetails.id
        self.present(nextVC, animated: true, completion: nil)
    }
    
    @IBAction func callAsistanceTapped(_ sender: UIButton) {
        makeCall(to: 0)
    }
    
}

// CollectionView Delegates
extension EXVSEventDetailVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == organisersCV
        { return eventDetails.assignedmembers.count
        }
        else if collectionView == sponsorsCV
        { return eventDetails.sponers.count
        }
        else // slots
        { return eventDetails.slots.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == organisersCV
        { let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! OrganisersCVC
            cell.nameLBL.text = eventDetails.assignedmembers[indexPath.item].username
            let url = URL(string: "\(baseURL)\(imageURL)\(eventDetails.assignedmembers[indexPath.item].image)")
            cell.organiserImage.kf.setImage(with: url, placeholder: UIImage(named: "noProfilePic.png"))
            return cell
        }
            
        else if collectionView == sponsorsCV
        { let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SponsersCVC
            let url = URL(string: "\(baseURL)\(imageURL)\(eventDetails.sponers[indexPath.item].image)")
            cell.sponserImage.kf.setImage(with: url, placeholder: UIImage(named: "noProfilePic.png"))
            return cell
        }
            
        else // slots
        { let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SlotsCVC
            cell.slotNoLBL.text = String(indexPath.item+1)
            if (eventDetails.slots[indexPath.item].booking_image != "")
            {
                let url = URL(string: "\(baseURL)\(imageURL)\(eventDetails.slots[indexPath.item].booking_image)")
                cell.logoImage.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "noimage.jpg"))
            }
            
            if eventDetails.slots[indexPath.item].price == "" || eventDetails.slots[indexPath.item].size == ""
            { cell.backgroundColor = UIColor.lightGray
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == slotCV
        {
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView == organisersCV
        { return CGSize(width: 80, height: 100)
        }
        else if collectionView == sponsorsCV
        { return CGSize(width: 120, height: 100)
        }
        else
        { return CGSize(width: (slotCV.bounds.width-25)/4, height: (slotCV.bounds.width-25)/4)
        }
    }
    
}
