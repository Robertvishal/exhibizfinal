//
//  EXVisitorModels.swift
//  Exhibiz
//
//  Created by Appzoc on 24/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import Foundation

let registrationApi = "ExhibizWeb/backend/api/registration"

let loginApi = "ExhibizWeb/backend/api/login"
//let loginApi = "login"


// AllEvents List Model
class EventListModel
{
    var id = 0
    var adminID = 0
    var title = ""
    var description = ""
    var categoryID = 0
    var lat = ""
    var lon = ""
    var address = ""
    var slots = [SlotModel]()
    var sadadnumber = ""
    var startDate = ""
    var endDate = ""
    var days = ""
    var maximumTickets = 0
    var price = ""
    var ticketType = 0
    var createdAt = ""
    var category = ""
    var eventImages = [String]()
    var assignedmembers = [AssignedMemberModel]()
    var sponers =  [AssignedMemberModel]()
    var month = ""
    var day = ""
    var confernecedetails = [conferenceDetailsModel]()
    
    //// new change - for slot booking webview - in details page(from allevents)
    var slot_booking_url = "http://signitivebeta.com/exhibizWeb/slot_booking_beta"
    
    
    func createModel(data:[String:Any]) -> EventListModel
    {
        self.id = Int(data["id"] as? String ?? "0")!
        self.adminID = Int(data["admin_id"] as? String ?? "0")!
        self.title = data["title"] as? String ?? ""
        self.description = data["description"] as? String ?? ""
        self.categoryID = Int(data["category_id"] as? String ?? "0")!
        self.lat = data["lat"] as? String ?? ""
        self.lon = data["lon"] as? String ?? ""
        self.address = data["address"] as? String ?? ""
        self.sadadnumber = data["sadadnumber"] as? String ?? ""
        self.startDate = data["start_date"] as? String ?? ""
        self.endDate = data["end_date"] as! String //?? ""
        self.days = data["days"] as? String ?? ""
        self.maximumTickets = Int(data["maximum_tickets"] as? String ?? "0")!
        self.price = data["price"] as? String ?? ""
        self.ticketType = Int(data["ticket_type"] as? String ?? "0")!
        self.createdAt = data["created_at"] as? String ?? ""
        self.category = data["category"] as? String ?? ""
        self.month = data["month"] as? String ?? ""
        self.day = data["day"] as? String ?? ""
        self.eventImages = data["event_images"] as? [String] ?? [""]
        
        //self.slot_booking_url = data["slot_booking_url"] as? String ?? ""
        
        for item in data["slots"] as! [[String:Any]]
        {
            let slot = SlotModel()
            self.slots.append(slot.createModel(data: item))
        }
        
        for item in data["assignedmembers"] as! [[String:Any]]
        {
            let assignedmembers = AssignedMemberModel()
            self.assignedmembers.append(assignedmembers.createModel(data: item))
        }
        
        for item in data["sponers"] as! [[String:Any]]
        {
            let sponcers = AssignedMemberModel()
            self.sponers.append(sponcers.createModel(data: item))
        }
        
        for item in data["confernecedetails"] as? [[String:Any]] ?? [[String:Any]]()
        {
            let confernecedetails = conferenceDetailsModel()
            self.confernecedetails.append(confernecedetails.createModel(data: item))
        }
        
        return self
    }
}

class AssignedMemberModel
{
    var id = 0
    var username = ""
    var email = ""
    var password = ""
    var password2 = ""
    var contactno = ""
    var interest = 0
    var image = ""
    var createdAt = ""
    
    func createModel(data:[String:Any]) -> AssignedMemberModel
    {
        self.id = Int(data["id"] as? String ?? "0")!
        self.username = data["username"] as? String ?? ""
        self.email = data["email"] as? String ?? ""
        self.password = data["password"] as? String ?? ""
        self.password2 = data["password2"] as? String ?? ""
        self.contactno = data["contactno"] as? String ?? ""
        self.interest = Int(data["interest"] as? String ?? "0")!
        self.image = data["image"] as? String ?? ""
        self.createdAt = data["createdAt"] as? String ?? ""
        
        return self
    }
}

class SlotModel
{
    var id = 0
    var eventID = 0
    var size = ""
    var price = ""
    var image = ""
    var status = 0
    var exhibition_id = 0
    var createdAt = ""
    var booking_image = ""
    
    func createModel(data:[String:Any]) -> SlotModel
    {
        self.id = Int(data["id"] as? String ?? "0")!
        self.eventID = Int(data["event_id"] as? String ?? "0")!
        self.size = data["size"] as? String ?? ""
        self.price = (data["price"] as? String ?? "") == "" ? "0" : (data["price"] as? String ?? "")
        self.image = data["image"] as? String ?? ""
        self.status = Int(data["status"] as? String ?? "0")!
        self.exhibition_id = Int(data["exhibition_id"] as? String ?? "0")!
        self.createdAt = data["created_at"] as? String ?? ""
        self.booking_image = data["booking_image"] as? String ?? ""
        return self
    }
}

class conferenceDetailsModel
{
    var id = 0
    var event_id = 0
    var name = ""
    var image = ""
    
    func createModel(data:[String:Any]) -> conferenceDetailsModel
    {
        self.id = Int(data["id"] as? String ?? "0")!
        self.event_id = Int(data["event_id"] as? String ?? "0")!
        self.name = data["name"] as? String ?? ""
        self.image = data["image"] as? String ?? ""
        return self
    }
}


// All Exhibitions List Model
class ExhibitionListModel
{
    var id: Int
    var eventID: Int
    var exhibitionDetailID: Int
    var title: String
    var description: String
    var tags: String
    var startDate: String
    var endDate: String
    var totaldays: String
    var startTime: String
    var endTime: String
    var eventType: String
    var ticketType: Int
    var price: String
    var createdAt: String
    var exhibitionImages: [String]
    var startDate2: Date2
    var endDate2: Date2
    var userBookStatus: Bool
    var details: Details
    
    init(data:[String:Any])
    {
        self.id = Int(data["id"] as? String ?? "0")!
        self.eventID = Int(data["event_id"] as? String ?? "0")!
        self.exhibitionDetailID = Int(data["exhibition_detail_id"] as? String ?? "0")!
        self.title = data["title"] as? String ?? ""
        self.description = data["description"] as? String ?? ""
        self.tags = data["tags"] as? String ?? ""
        self.startDate = data["start_date"] as? String ?? ""
        self.endDate = data["end_date"] as? String ?? ""
        self.totaldays = data["totaldays"] as? String ?? ""
        self.startTime = data["start_time"] as? String ?? ""
        self.endTime = data["end_time"] as? String ?? ""
        self.eventType = data["event_type"] as? String ?? ""
        self.ticketType = Int(data["ticket_type"] as? String ?? "0")!
        self.price = data["price"] as? String ?? ""
        self.createdAt = data["created_at"] as? String ?? ""
        self.exhibitionImages = data["exhibition_images"] as? [String] ?? [String]()
        self.userBookStatus = data["user_book_status"] as? Bool ?? false
        
        self.startDate2 = Date2(data: data["start_date2"] as? [String : Any] ?? [String:Any]() )
        self.endDate2 = Date2(data: data["end_date2"] as? [String : Any] ?? [String:Any]() )
        self.details = Details(data: data["details"] as? [String : Any] ?? [String:Any]() )
    }
}

class Details
{
    var id: Int
    var name: String
    var createdUser: Int
    var website: String
    var location: String
    var email: String
    var address: String
    var info: String
    var phone: String
    var createdAt: String
    var exhibition = [Career]()
    var career = [Career]()
    var conferences = [Career]()
    
    init(data:[String:Any])
    {
        self.id = Int(data["id"] as? String ?? "0")!
        self.name = data["name"] as? String ?? ""
        self.createdUser = Int(data["created_user"] as? String ?? "0")!
        self.website = data["website"] as? String ?? ""
        self.location = data["location"] as? String ?? ""
        self.email = data["email"] as? String ?? ""
        self.address = data["address"] as? String ?? ""
        self.info = data["info"] as? String ?? ""
        self.phone = data["phone"] as? String ?? ""
        self.createdAt = data["createdAt"] as? String ?? ""
        
        for item in data["exhibition"] as? [[String:Any]] ?? [[String:Any]]()
        { self.exhibition.append(Career(data: item))
        }
        
        for item in data["career"] as? [[String:Any]] ?? [[String:Any]]()
        { self.career.append(Career(data: item))
        }
        
        for item in data["conferences"] as? [[String:Any]] ?? [[String:Any]]()
        { self.conferences.append(Career(data: item))
        }
    }
}

class Career
{
    var id: Int
    var exhibitionDetailID: Int
    var name: String
    var vacany: String
    var createdAt: String
    
    init(data:[String:Any])
    {
        self.id = Int(data["id"] as? String ?? "0")!
        self.exhibitionDetailID = Int(data["exhibition_detail_id"] as? String ?? "0")!
        self.name = data["name"] as? String ?? ""
        self.vacany = data["vacany"] as? String ?? ""
        self.createdAt = data["created_at"] as? String ?? ""
    }
}

class Date2
{
    var date: String
    var day: String
    
    init(data:[String:Any])
    {
        self.date = data["date"] as? String ?? ""
        self.day = data["day"] as? String ?? ""
    }
}


