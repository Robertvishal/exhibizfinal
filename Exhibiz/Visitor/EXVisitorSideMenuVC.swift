//
//  EXVisitorSideMenuVC.swift
//  Exhibiz
//
//  Created by Appzoc on 07/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import Kingfisher

class EXVisitorSideMenuVC: UIViewController
{
    @IBOutlet var contentTV: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    @IBAction func closeBTNTapped(_ sender: UIButton)
    {
        let previousVC = self.presentingViewController
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXEditProfileVC") as! EXEditProfileVC
        nextVC.isVisitor = true
        self.dismiss(animated: true, completion: { previousVC?.present(nextVC, animated: true, completion: nil) })
    }
}

// TableView Delagates
extension EXVisitorSideMenuVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 9
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell1") as! SideMenuCell1
        let cell2 = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell2") as! SideMenuCell2
        
        switch indexPath.row
        {
        case 0:
            cell1.nameLBL.text = UserDefaults.standard.string(forKey: "Username")
            
            let url = URL(string: "\(baseURL)\(imageURL)\(UserDefaults.standard.string(forKey: "ImageURL")!)")
            cell1.profileImageView.kf.setImage(with: url, placeholder: UIImage(named: "noProfilePic.png"))
            print("\(baseURL)\(imageURL)\(UserDefaults.standard.string(forKey: "ImageURL")!))")
            return cell1
        case 1:
            cell2.listNameLBL.text = "Events"
            cell2.listImageView.image = UIImage(named: "firstSM")
            return cell2
        case 2:
            cell2.listNameLBL.text = "My Events"
            cell2.listImageView.image = UIImage(named: "Myevents")
            return cell2
        case 3:
            cell2.listNameLBL.text = "My Annoucements"
            cell2.listImageView.image = UIImage(named: "secondSM")
            return cell2
        case 4:
            cell2.listNameLBL.text = "Get My QR"
            cell2.listImageView.image = UIImage(named: "qr-code-scan")
            return cell2
        case 5:
            cell2.listNameLBL.text = "Share"
            cell2.listImageView.image = UIImage(named: "thirdSM")
            return cell2
        case 6:
            cell2.listNameLBL.text = "Rate Us"
            cell2.listImageView.image = UIImage(named: "fourthSM")
            return cell2
        case 7:
            cell2.listNameLBL.text = "About Us"
            cell2.listImageView.image = UIImage(named: "fifthSM")
            return cell2
        case 8:
            cell2.listNameLBL.text = "Sign out"
            cell2.listImageView.image = UIImage(named: "sixthSM")
            return cell2
        default:
            return cell1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0
        { return 250
        }
        else
        { return 50
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let previousVC = self.presentingViewController
        let logoutVC = self.view.window?.rootViewController?.presentedViewController as? EXLoginVC
        
    switch indexPath.row
    {
      case 0:
      break
        
      case 1:
        let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXAllEventsVisitorVC") as! EXAllEventsVisitorVC
        nextVC.isReloadRequired = true
        self.dismiss(animated: true, completion: { previousVC?.present(nextVC, animated: true, completion: nil) })
        
      case 2:
        let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXMyEventsVisitorVC")
        self.dismiss(animated: true, completion: { previousVC?.present(nextVC, animated: true, completion: nil) })
        
      case 3:
          let storyBoard = UIStoryboard(name: "Main", bundle: nil)
          let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXAnnounceListVC") as! EXAnnounceListVC
          nextVC.userType = .Visitor
          self.dismiss(animated: true, completion: { previousVC?.present(nextVC, animated: true, completion: nil) })
//          self.dismiss(animated: true, completion: {ComingSoonBanner()})
       case 4:
        
       let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXVisitorQR") as! EXVisitorQR
       //nextVC.isReloadRequired = true
       self.dismiss(animated: true, completion: { previousVC?.present(nextVC, animated: true, completion: nil) })
        
        //self.dismiss(animated: true, completion: {commomShare(viewcontroller: previousVC!)})
        
      case 5:
        self.dismiss(animated: true, completion: {commomShare(viewcontroller: previousVC!)})
        
      case 6:
        self.dismiss(animated: true, completion: {
//            ComingSoonBanner()
            rateExhbiz()
        })
//        openAppStore()
        
      case 7:
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXAboutUsVC")
        self.dismiss(animated: true, completion: { previousVC?.present(nextVC, animated: true, completion: nil) })
        
      case 8:
        self.dismiss(animated: true, completion:
        {
            logoutVC?.dismiss(animated: true, completion: nil)
            logoutVC?.usernameTF.text = ""
            logoutVC?.passwordTF.text = ""
        })
        
      default:
        print("Default Case")
    }
    }
}

