//
//  EXAllEventsVisitorVC.swift
//  Exhibiz
//
//  Created by Appzoc on 07/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import SideMenu
import Kingfisher

class EXEventListCell: UITableViewCell
{
    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var placeLBL: UILabel!
    @IBOutlet var dateLBL: UILabel!
    @IBOutlet var monthLBL: UILabel!
    @IBOutlet var eventImage: UIImageView!
    @IBOutlet var priceBTN: BaseButton!
    @IBOutlet var bannerTagBTN: UIButton!
}


class EXAllEventsVisitorVC: UIViewController
{
    @IBOutlet var contentTV: UITableView!
    @IBOutlet var noEventsLBL: UILabel!
    
    var eventList = [EventListModel]()
    var isReloadRequired:Bool = true
    var listURL = "ExhibizWeb/backend/api/events"
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if isReloadRequired
        { webCall()
          isReloadRequired = false
        }
    }
    
    func webCall()
    {
        Webservices.getMethod(url: listURL)
        { (isFetched, resultData) in
            if isFetched
            {
                self.eventList.removeAll()
                for item in resultData["Data"] as! [[String:Any]]
                {
                    let eventListModel = EventListModel()
                    self.eventList.append(eventListModel.createModel(data: item))
                    debugPrint(eventListModel)
                }
                self.contentTV.reloadData()
                self.contentTV.scrollTableToTop()
            }
            else
            {  // self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func sideMenuTapped(_ sender: UIButton)
    {
        let sideMenuVC = self.storyboard!.instantiateViewController(withIdentifier: "EXVisitorSideMenuVC") as! EXVisitorSideMenuVC
        let SideMenuController = UISideMenuNavigationController(rootViewController: sideMenuVC)
        SideMenuController.navigationBar.isHidden = true
        SideMenuController.leftSide = true
        self.present(SideMenuController, animated: true, completion: nil)
    }
    
    @IBAction func searchBTNTapped(_ sender: UIButton)
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let searchVC = storyBoard.instantiateViewController(withIdentifier: "EXSearchVC") as! EXSearchVC
        searchVC.eventList = self.eventList
        searchVC.fromUser = .Visitor
        self.present(searchVC, animated: true, completion: nil)
    }
    
    @IBAction func filterBTNTapped(_ sender: UIButton)
    {
        let optionMenu = UIAlertController(title: "Choose Type of event", message: nil, preferredStyle: .actionSheet)
        
        let allEvents = UIAlertAction(title: "All Events", style: .default, handler:
        { (alert: UIAlertAction!) -> Void in
            self.listURL = "ExhibizWeb/backend/api/events"
            self.webCall()
        })
        
        let exhibition = UIAlertAction(title: "Exhibition", style: .default, handler:
        { (alert: UIAlertAction!) -> Void in
            self.listURL = "ExhibizWeb/backend/api/events?eventtype=3"
            self.webCall()
        })
        
        let careerFair = UIAlertAction(title: "Career Fair", style: .default, handler:
        { (alert: UIAlertAction!) -> Void in
            self.listURL = "ExhibizWeb/backend/api/events?eventtype=2"
            self.webCall()
        })
        
        let conference = UIAlertAction(title: "Conference", style: .default, handler:
        { (alert: UIAlertAction!) -> Void in
            self.listURL = "ExhibizWeb/backend/api/events?eventtype=1"
            self.webCall()
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        optionMenu.addAction(exhibition)
        optionMenu.addAction(careerFair)
        optionMenu.addAction(conference)
        optionMenu.addAction(allEvents)
        optionMenu.addAction(cancel)
        self.present(optionMenu, animated: true, completion: nil)
    }

}

// TableView Delegates
extension EXAllEventsVisitorVC:UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if eventList.isEmpty
        { noEventsLBL.isHidden = false
        }
        else
        { noEventsLBL.isHidden = true
        }
        return eventList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventsCell") as! EXEventListCell
        cell.titleLBL.text = eventList[indexPath.row].title
        cell.dateLBL.text = eventList[indexPath.row].day
        cell.monthLBL.text = String(eventList[indexPath.row].month.prefix(3))
        cell.placeLBL.text = eventList[indexPath.row].address
        
        
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let todayy = Date()
        //let now = formatter.string(from: todayy)
        

        print(eventList[indexPath.row].startDate)
        print("enddate:",eventList[indexPath.row].endDate)
        if let EndDate = formatter.date(from: eventList[indexPath.row].endDate) {
            if todayy > EndDate
            {
                
                cell.priceBTN.setTitle("CLOSED", for: .normal)
                
            }else {
                
                let price = (eventList[indexPath.row].price == "" || eventList[indexPath.row].price == "0") ? "Free" : "SR "+eventList[indexPath.row].price
                cell.priceBTN.setTitle(price, for: .normal)
                
            }

        }else {
            formatter.dateFormat = "dd-MM-yyyy"
            let todayy = Date()
            let EndDate = formatter.date(from: eventList[indexPath.row].endDate)
            if todayy > EndDate!
            {
                cell.priceBTN.setTitle("CLOSED", for: .normal)
                
            }else {
                
                let price = (eventList[indexPath.row].price == "" || eventList[indexPath.row].price == "0") ? "Free" : "SR "+eventList[indexPath.row].price
                cell.priceBTN.setTitle(price, for: .normal)
                
            }

            
        }
        
        
        
        
        cell.bannerTagBTN.setTitle(eventList[indexPath.row].category, for: .normal)
        
        let url = URL(string: "\(baseURL)\(imageURL)\(eventList[indexPath.row].eventImages[0])")
        cell.eventImage.kf.setImage(with: url, placeholder: UIImage(named: "noimage.jpg"))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if eventList[indexPath.row].categoryID == 1
        {
            tableView.deselectRow(at: indexPath, animated: false)
            let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXConferenceDetailVC") as! EXConferenceDetailVC
            nextVC.eventDetails = eventList[indexPath.row]
            self.present(nextVC, animated: true, completion: nil)
        }
        else
        {
            tableView.deselectRow(at: indexPath, animated: false)
            let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXVSEventDetailVC") as! EXVSEventDetailVC
            nextVC.eventDetails = eventList[indexPath.row]
            self.present(nextVC, animated: true, completion: nil)
        }
    }
    
    func selectedFromSearch(eventModel:EventListModel)
    {
        if eventModel.categoryID == 1
        {
            let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXConferenceDetailVC") as! EXConferenceDetailVC
            nextVC.eventDetails = eventModel
            self.present(nextVC, animated: true, completion: nil)
        }
        else
        {
            let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXVSEventDetailVC") as! EXVSEventDetailVC
            nextVC.eventDetails = eventModel
            self.present(nextVC, animated: true, completion: nil)
        }
    }
}










