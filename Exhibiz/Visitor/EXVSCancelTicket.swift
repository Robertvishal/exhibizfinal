//
//  EXVSCancelTicket.swift
//  Exhibiz
//
//  Created by Appzoc on 02/05/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import Kingfisher

class cancelTicketTVC: UITableViewCell
{
    @IBOutlet var nameLBL: UILabel!
    @IBOutlet var cancelBTN: UIButton!
}


class EXVSCancelTicket: UIViewController
{
    @IBOutlet var contentTV: UITableView!
    @IBOutlet var noTicketLBL: UILabel!
    @IBOutlet var qrCodeImage: UIImageView!
    
    var eventID = 0
    var isReloadRequired = true
    var contentArray = [[String:Any]]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if isReloadRequired
        {
            getUserTickets()
            isReloadRequired = false
        }
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelTicketBTNTapped(_ sender: UIButton)
    {
        let actionSheet = UIAlertController(title: "Confirm", message: "Do you want to cancel this booking", preferredStyle: .actionSheet)
        
        let yes = UIAlertAction(title: "Yes", style: .destructive)
        { (action) in
            self.deleteTicket(ticketID: (self.contentArray[sender.tag])["id"] as? String ?? "")
        }
        let no = UIAlertAction(title: "No", style: .cancel, handler: nil)
        actionSheet.addAction(yes)
        actionSheet.addAction(no)
        self.present(actionSheet, animated: true, completion: nil)
    }
}
// TableView Delegates
extension EXVSCancelTicket:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return contentArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let imageURL = URL(string: (baseURL + "ExhibizWeb/backend/" + ((contentArray[0])["qr_image"] as? String ?? "")))
        self.qrCodeImage.kf.setImage(with: imageURL, placeholder: #imageLiteral(resourceName: "noimage.jpg"))
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! cancelTicketTVC
        cell.nameLBL.text = (contentArray[indexPath.row])["name"] as? String ?? ""
        
        if (contentArray[indexPath.row])["status"] as? String ?? "" == "1"
        {
            cell.cancelBTN.tag = indexPath.row
            cell.nameLBL.textColor = UIColor.black
            cell.cancelBTN.setTitle("Cancel", for: .normal)
            cell.cancelBTN.setTitleColor(UIColor.black, for: .normal)
            cell.cancelBTN.isUserInteractionEnabled = true
        }
        else
        {
            cell.cancelBTN.tag = indexPath.row
            cell.nameLBL.textColor = UIColor.lightGray
            cell.cancelBTN.setTitle("Visited", for: .normal)
            cell.cancelBTN.setTitleColor(UIColor.lightGray, for: .normal)
            cell.cancelBTN.isUserInteractionEnabled = false
        }
        
        if indexPath.row  % 2 == 0
        { cell.backgroundColor = UIColor.white
        }
        else
        { cell.backgroundColor = UIColor(red:0.85, green:0.85, blue:0.85, alpha:1.0)
        }
        return cell
    }
}
// WebService
extension EXVSCancelTicket
{
    func getUserTickets()
    {
        Webservices.getMethod(url: "ExhibizWeb/backend/api/get/usertickets/\(self.eventID)/\(UserDefaults.standard.string(forKey: "UserID")!)")
        { (isFetched, resultData) in
            if isFetched
            {
                self.contentArray.removeAll()
                let data = resultData["Data"] as? [[String:Any]] ?? [[String:Any]]()
                for items in data
                {
                    if items["status"] as? String ?? "" != "0"
                    {
                        self.contentArray.append(items)
                    }
                }
                self.contentTV.reloadData()
                self.contentArray.isEmpty ? (self.noTicketLBL.isHidden = false) : (self.noTicketLBL.isHidden = true)
            }
        }
    }
    
    func deleteTicket(ticketID:String)
    {
        Webservices.postMethod(url: "ExhibizWeb/backend/api/cancel/tickets/\(ticketID)", parameter: [String:Any]())
        { (isFetched, resultData) in
            if isFetched
            {
                self.getUserTickets()
            }
        }
    }
}

