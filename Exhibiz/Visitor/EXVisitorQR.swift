//
//  EXVisitorQR.swift
//  Exhibiz
//
//  Created by Akhil Chandran on 15/10/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit

class EXVisitorQR: UIViewController {

    @IBOutlet weak var QR_IMV: UIImageView!
    var qrcodeImage: CIImage!
    override func viewDidLoad() {
        super.viewDidLoad()

        //let logoutVC = self.view.window?.rootViewController?.presentedViewController as? EXLoginVC
        
        let k = UserDefaults.standard.string(forKey: "UserID")!
        
        
        
        let data = k.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
        
        let filter = CIFilter(name: "CIQRCodeGenerator")
        
        filter?.setValue(data, forKey: "inputMessage")
        filter?.setValue("Q", forKey: "inputCorrectionLevel")
        
        
     
        
        let context:CIContext = CIContext.init(options: nil)
        let cgImage:CGImage = context.createCGImage((filter?.outputImage)!, from: (filter?.outputImage!.extent)!)!
        let image:UIImage = UIImage.init(cgImage: cgImage)
        
        QR_IMV.image = image
        
       
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        displayQRCodeImage()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BackTapped(_ sender: UIButton) {
        
        self.dismiss(animated: false, completion: nil)
    }
    
    
    func displayQRCodeImage() {
//        let scaleX = QR_IMV.frame.size.width / qrcodeImage.extent.size.width
//        let scaleY = QR_IMV.frame.size.height / qrcodeImage.extent.size.height
//
//        let transformedImage = qrcodeImage.transformed(by: CGAffineTransform(scaleX: scaleX, y: scaleY))
//
//        QR_IMV.image = UIImage(cgImage: transformedImage as! CGImage)
        
        
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
