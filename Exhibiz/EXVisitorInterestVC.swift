//
//  EXVisitorInterestVC.swift
//  Exhibiz
//
//  Created by Appzoc on 26/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit

//Area of Interest(1-co,2-ca,3-ex)
class EXVisitorInterestVC: UIViewController
{
    @IBOutlet var conferenceBTN: BaseButton!
    @IBOutlet var careerBTN: BaseButton!
    @IBOutlet var exhibitionBTN: BaseButton!
    
    var selectedInterest = 3
    var fromEdit = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if selectedInterest == 1
        {
            conferenceBTN.backgroundColor = blueColor
            conferenceBTN.setTitleColor(UIColor.white, for: .normal)
            careerBTN.backgroundColor = mercuryColor
            exhibitionBTN.backgroundColor = mercuryColor
            careerBTN.setTitleColor(UIColor.black, for: .normal)
            exhibitionBTN.setTitleColor(UIColor.black, for: .normal)
        }
        if selectedInterest == 2
        {
            conferenceBTN.backgroundColor = mercuryColor
            careerBTN.backgroundColor = blueColor
            exhibitionBTN.backgroundColor = mercuryColor
            
        }
        if selectedInterest == 3
        {
            conferenceBTN.backgroundColor = mercuryColor
            careerBTN.backgroundColor = mercuryColor
            exhibitionBTN.backgroundColor = blueColor
        }
    }
    
    @IBAction func nextBTNTapped(_ sender: UIButton)
    {
        if selectedInterest == 0
        { bannerWithMessage(message: "Choose your area of interest")
        }
        else if fromEdit == false
        { let nextVC = self.presentingViewController as! EXVisitorSignUpVC
          nextVC.selectedInterest = selectedInterest
          self.dismiss(animated: true, completion: nil)
        }
        else
        { let nextVC = self.presentingViewController as! EXEditProfileVC
          nextVC.selectedUserInterest = selectedInterest
          self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    @IBAction func conferenceBTNTapped(_ sender: UIButton)
    {
        
        
        if conferenceBTN.backgroundColor == blueColor
        {
            
            conferenceBTN.backgroundColor = mercuryColor
            conferenceBTN.setTitleColor(UIColor.black, for: .normal)
        }
        else
        {
            conferenceBTN.backgroundColor = blueColor
            conferenceBTN.setTitleColor(UIColor.white, for: .normal)
        }
        
        //conferenceBTN.backgroundColor = blueColor
        //conferenceBTN.setTitleColor(UIColor.white, for: .normal)
        //careerBTN.backgroundColor = mercuryColor
        //exhibitionBTN.backgroundColor = mercuryColor
        selectedInterest = 1
        
        //careerBTN.setTitleColor(UIColor.black, for: .normal)
        //exhibitionBTN.setTitleColor(UIColor.black, for: .normal)
    }
    @IBAction func careerBTNTapped(_ sender: UIButton)
    {
        
        
        if careerBTN.backgroundColor == blueColor
        {
            
            careerBTN.backgroundColor = mercuryColor
            careerBTN.setTitleColor(UIColor.black, for: .normal)
        }
        else
        {
            careerBTN.backgroundColor = blueColor
            careerBTN.setTitleColor(UIColor.white, for: .normal)
        }
        
        
        //conferenceBTN.backgroundColor = mercuryColor
       // careerBTN.backgroundColor = blueColor
        //careerBTN.setTitleColor(UIColor.white, for: .normal)
        //exhibitionBTN.backgroundColor = mercuryColor
        selectedInterest = 2
        
        //conferenceBTN.setTitleColor(UIColor.black, for: .normal)
        //exhibitionBTN.setTitleColor(UIColor.black, for: .normal)
    }
    @IBAction func exhibitionBTNTapped(_ sender: UIButton)
    {
        
        if exhibitionBTN.backgroundColor == blueColor
        {
            
            exhibitionBTN.backgroundColor = mercuryColor
            exhibitionBTN.setTitleColor(UIColor.black, for: .normal)
        }
        else
        {
            exhibitionBTN.backgroundColor = blueColor
            exhibitionBTN.setTitleColor(UIColor.white, for: .normal)
        }
        
        
        //conferenceBTN.backgroundColor = mercuryColor
        //careerBTN.backgroundColor = mercuryColor
       // exhibitionBTN.backgroundColor = blueColor
        //exhibitionBTN.setTitleColor(UIColor.white, for: .normal)
        selectedInterest = 3
        
        //conferenceBTN.setTitleColor(UIColor.black, for: .normal)
        //careerBTN.setTitleColor(UIColor.black, for: .normal)
    }
}
