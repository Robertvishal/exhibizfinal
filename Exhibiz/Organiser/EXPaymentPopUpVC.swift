//
//  EXPaymentPopUpVC.swift
//  Exhibiz
//
//  Created by Appzoc on 16/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit

enum PopUpFrom
{
    case Organiser
    case Exhibitor
}

class EXPaymentPopUpVC: UIViewController
{
    @IBOutlet var remainingDaysLBL: UILabel!
    
    var popUpFrom = PopUpFrom.Organiser
    var remainingDays = "15"
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        remainingDaysLBL.text = remainingDays + " days\nremaining"
    }
    
    @IBAction func doneBTNTapped(_ sender: UIButton)
    {
        switch popUpFrom
        {
        case .Organiser:
            let previousVC = self.presentingViewController as? EXPaymentSelectVC
            self.dismiss(animated: true, completion:
            {
                previousVC?.dismissToList()
            })
        case .Exhibitor:
            let previousVC = self.presentingViewController as? EXExChoosePaymentVC
            self.dismiss(animated: true, completion:
            {
                    previousVC?.dismissToList()
            })
        }
    }
}
