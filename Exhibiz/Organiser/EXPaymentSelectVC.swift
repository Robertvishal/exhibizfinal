//
//  EXPaymentSelectVC.swift
//  Exhibiz
//
//  Created by Appzoc on 16/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit

class EXPaymentSelectVC: UIViewController
{
    @IBOutlet var eventTypeLBL: UILabel!
    @IBOutlet var eventTitleLBL: UILabel!
    @IBOutlet var noOfDaysLBL: UILabel!
    @IBOutlet var pricePerDayLBL: UILabel!
    @IBOutlet var totalPriceLBL: UILabel!
    
    var parameter = [String:Any]()
    var companyImage = UIImage()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        let slotDetailVC = self.presentingViewController?.presentingViewController as! EXBookSpaceVC
        let eventDetailVC = self.presentingViewController?.presentingViewController?.presentingViewController as! EXEventDetailOrganiserVC
        eventTitleLBL.text = slotDetailVC.eventTitle
        eventTypeLBL.text = eventDetailVC.content?.category!
        pricePerDayLBL.text = slotDetailVC.slotDetails.price
        totalPriceLBL.text = slotDetailVC.slotDetails.price
        
        parameter["slot_id"] = "\(slotDetailVC.slotDetails.id)"
        parameter["event_id"] = "\(eventDetailVC.content!.id!)"
        
        let intPrice = Int(Float(totalPriceLBL.text!)!) * 100
        parameter["paymentamount"] = String(intPrice)
        print(parameter["paymentamount"]!)
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func directlyPayBTNTapped(_ sender: UIButton)
    {
        parameter["type"] = "1"
        parameter["status"] = "1"
        
        print(parameter)
        Webservices.postMethodMultiPartImage(url: "ExhibizWeb/backend/api/slots/booking/\(String(describing: parameter["slot_id"]!))",
            parameter: parameter,
            imageParameter: ["image":[companyImage]])
        { (isFetched, ResultData) in
            if isFetched
            {
                print(ResultData)
                self.dismissToList()
            }
        }
    }
    
    @IBAction func offlineBlockBTNTapped(_ sender: UIButton)
    {
        parameter["type"] = "2"
        parameter["status"] = "2"
        
        print(parameter)
        Webservices.postMethodMultiPartImage(url: "ExhibizWeb/backend/api/slots/booking/\(String(describing: parameter["slot_id"]!))",
            parameter: parameter,
            imageParameter: ["image":[companyImage]])
        { (isFetched, ResultData) in
            if isFetched
            {
                print(ResultData)
                let days = ResultData["days"] as? String ?? "0"
                let nextVC = UIStoryboard(name: "EXOrganiser", bundle: nil).instantiateViewController(withIdentifier: "EXPaymentPopUpVC") as! EXPaymentPopUpVC
                nextVC.remainingDays = days
                nextVC.popUpFrom = .Organiser
                self.present(nextVC, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func payNowBTNTapped(_ sender: UIButton)
    {
        parameter["type"] = "3"
        
        if parameter["paymentamount"] as! String == "0"
        {
            parameter["status"] = "1"
        }
        else
        {
            parameter["status"] = "0"
        }
        
        print(parameter)

        Webservices.postMethodMultiPartImage(url: "ExhibizWeb/backend/api/slots/booking/\(String(describing: parameter["slot_id"]!))",
            parameter: parameter,
            imageParameter: ["image":[companyImage]])
        { (isFetched, ResultData) in
            if isFetched
            {
                print(self.parameter["paymentamount"] as! String)
                if self.parameter["paymentamount"] as! String != "0"
                {
                    let Data = ResultData["Data"] as? [String:Any] ?? ["":""]
                    let url = Data["url"] as? String ?? ""
                    
                    let paymentVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EXPaymentVC") as! EXPaymentVC
                    paymentVC.url = url
                    self.present(paymentVC, animated: true, completion: nil)
                }
                else
                {
                    print(ResultData)
                    self.dismissToList()
                }
            }
        }
    }
    
    func dismissToList()
    {
        let alert = UIAlertController(title: "Slot has been booked for exhibitor",
                                      message: nil,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok",
                                      style: .default,
                                      handler:
            { _ in
                let listVC = self.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController as! EXMyEventsOrganiserVC
                listVC.isRequiredReload = true
                listVC.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
}
