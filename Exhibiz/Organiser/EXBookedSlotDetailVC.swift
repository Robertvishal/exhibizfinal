//
//  EXBookedSlotDetailVC.swift
//  Exhibiz
//
//  Created by Appzoc on 19/03/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit

class EXBookedSlotDetailVC: UIViewController
{
    @IBOutlet var companyImage: BaseImageView!
    @IBOutlet var companyNameLBL: UILabel!
    @IBOutlet var descriptionLBL: UILabel!
    @IBOutlet var addressLBL: UILabel!
    @IBOutlet var startDayLBL: UILabel!
    @IBOutlet var endDayLBL: UILabel!
    @IBOutlet var startDateLBL: UILabel!
    @IBOutlet var endDateLBL: UILabel!
    @IBOutlet var eventTypeLBL: UILabel!
    @IBOutlet var emailLBL: UILabel!
    
    var slotDetail:ADSlotsModel!
    var address = ""
    var startDay = ""
    var endDay = ""
    var startDate = ""
    var endDate = ""
    var eventType = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        companyNameLBL.text = slotDetail.company_name
        descriptionLBL.text = slotDetail.description
        addressLBL.text = slotDetail.address
//        startDayLBL.text =
//        endDayLBL.text =
        startDateLBL.text = startDate
        endDateLBL.text = endDate
        eventTypeLBL.text = eventType
        emailLBL.text = slotDetail.user_email
        
        let url = URL(string: "\(baseURL)\(imageURL)\(String(describing: (self.slotDetail.booking_image)!))")
        companyImage.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "noProfilePic.png"))
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func blockBTNTapped(_ sender: BaseButton)
    {
    }
    
}
