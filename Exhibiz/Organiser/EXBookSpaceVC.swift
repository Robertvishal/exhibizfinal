//
//  EXBookSpaceVC.swift
//  Exhibiz
//
//  Created by Appzoc on 15/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import ImageSlideshow

class EXBookSpaceVC: UIViewController
{
    @IBOutlet var slideShowView: ImageSlideshow!
    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var descriptionLBL: UILabel!
    @IBOutlet var priceLBL: UILabel!
    @IBOutlet var sizeLBL: UILabel!
    
    var slotDetails = SlotModel()
    var eventTitle = ""
    var eventDescription = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        var imageSourceArray = [KingfisherSource]()
        imageSourceArray.append(KingfisherSource(urlString: "\(baseURL)\(imageURL)\(slotDetails.image)",
            placeholder:UIImage(named: "noimage.jpg"))!)
        print("\(baseURL)\(imageURL)\(slotDetails.image)")
        slideShowView.setImageInputs(imageSourceArray)
        slideShowView.contentScaleMode = .scaleAspectFill
        titleLBL.text = eventTitle
        descriptionLBL.text = eventDescription
        priceLBL.text = slotDetails.price + " SR"
        sizeLBL.text = slotDetails.size + " sq ft"
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addDetailsBTNTapped(_ sender: UIButton)
    {
        let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXBookSpaceDetailsVC")
        self.present(nextVC, animated: true, completion: nil)
    }
}
