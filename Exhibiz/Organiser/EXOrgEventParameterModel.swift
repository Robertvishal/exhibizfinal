//
//  EXOrgEventParameterModel.swift
//  Exhibiz
//
//  Created by Appzoc on 07/03/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import Foundation
import UIKit

public class EXOrgEventParameterModel {
    public var title : String = ""//
    public var description : String = ""//
    public var start_date : String = ""//
    public var end_date : String = ""//
    
    public var pics : [UIImage] = []//
    public var slots_num: String = ""//
    public var sad_num: String = ""//
    public var sponsers : [String] = []//
    public var entry_type: String = "0"//
    public var price: String = "0"
    
    public var address : String = ""
    public var lat : String = ""
    public var lon : String = ""
    public var location: String = ""
    public var category_id : String = ""
    
    static let shared = EXOrgEventParameterModel()
    
    func getJson() -> Json{
        var params = Json()
        params["title"] = title
        params["description"] = description
        params["start_date"] = start_date
        params["end_date"] = end_date
        params["sponsers"] = sponsers
        params["sad_num"] = sad_num
        params["entry_type"] = entry_type
        params["pics"] = pics
        params["category_id"] = category_id
        params["slots_num"] = slots_num
        params["price"] = price
        
        params["address"] = address
        params["lat"] = lat
        params["lon"] = lon
        params["location"] = location
        
        return params
    }
    private init(){
    }
    
}
