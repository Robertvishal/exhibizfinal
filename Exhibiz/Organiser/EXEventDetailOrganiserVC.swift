//
//  EXEventDetailOrganiserVC.swift
//  Exhibiz
//
//  Created by Appzoc on 15/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import ImageSlideshow
import Kingfisher

class EXEventDetailOrganiserVC: UIViewController
{
    @IBOutlet var organisersCV: UICollectionView!
    @IBOutlet var sponsorsCV: UICollectionView!
    @IBOutlet var slotCV: UICollectionView!
    @IBOutlet var uploadEventBTN: BaseButton!
    @IBOutlet var slideShowView: ImageSlideshow!
    @IBOutlet var slotCVHeight: NSLayoutConstraint!
    
    @IBOutlet var eventTitleLBL: UILabel!
    @IBOutlet var descriptionLBL: UILabel!
    @IBOutlet var locationLBL: UILabel!
    @IBOutlet var startDayNameLBL: UILabel!
    @IBOutlet var endDayNameLBL: UILabel!
    @IBOutlet var startDateLBL: UILabel!
    @IBOutlet var endDateLBL: UILabel!
    @IBOutlet var eventTypeLBL: UILabel!
    @IBOutlet var slotsAvailableLBL: UILabel!
    @IBOutlet var entryTypePaidBTN: BaseButton!
    @IBOutlet var entryTypeFreeBTN: BaseButton!
    @IBOutlet var ticketPriceLBL: UILabel!
    @IBOutlet var heightTicketPriceView: NSLayoutConstraint!

    @IBOutlet var slotsHeadingLBL: UILabel!
    @IBOutlet var speakersHeight: NSLayoutConstraint!
    @IBOutlet var speakersTV: UITableView!
    
    @IBOutlet var editEventBTN: UIButton!
    // Data variables
    final var isFromMyEvents = false // for upload data after adding
    final var content: ADFuturePresentPastModel?
    final var organisorsArray = [ADAssignedMembersModel]()
    final var sponsorsArray = [ADSponersModel]()
    final var selectedOrganisors = [ADOrganisorListModel]()
    final var selectedSponsors = [ADSponsorListModel]()
    final var editEventParameter = Json()

    // data received from edit screen
    final var selectedSponsorsImages = [UIImage]()
    final var eventImages = [AnyObject]()
    final var assignedMembers = [ADAssignedMembersModel]()
    final var eventId: Int = 0

    private var slotCount: Int = 0
    private var sliderImagesArray = [KingfisherSource]()
    var imageLinkArray:[String] = []
    
    lazy var URLEditEvent = "ExhibizWeb/backend/api/edit/events/"
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        if isFromMyEvents
        {
            uploadEventBTN.setTitle("Announcement", for: .normal)
            editEventBTN.isHidden = false
        }
        else
        {
            uploadEventBTN.setTitle("Save Changes", for: .normal)
            editEventBTN.isHidden = true
            self.view.viewWithTag(10)?.isHidden = true
        }
        slideShowView.contentScaleMode = .scaleAspectFill
        setUpInterface()
    }

    override func viewDidAppear(_ animated: Bool)
    {
        
        if isFromMyEvents {
            slotCVHeight.constant = slotCV.contentSize.height
            slotCV.isHidden = false
            slotsHeadingLBL.isHidden = false
            
        }else {
            slotCVHeight.constant = 0
            slotCV.isHidden = true
            slotsHeadingLBL.isHidden = true

            
        }
        
        var const = (self.view.viewWithTag(10)?.constraints)
        const![1].constant = 0
        
        if content?.category_id! == 1
        { speakersHeight.constant = speakersTV.contentSize.height + 30
        }
        else
        { speakersHeight.constant = 0
        }
    }

    private func setUpInterface()
    {
        let format: String = "dd-MM-yyyy"
        self.clearData()
        guard let content = content else { return }
        setEventImages()
        if isFromMyEvents {
            if let organisors = content.assignedmembers, !organisors.isEmpty{
                organisorsArray = organisors
            }
            if let sponsors = content.sponers, !sponsors.isEmpty{
                sponsorsArray = sponsors
            }
            eventId = content.id!


        }else {
            if !assignedMembers.isEmpty{
                organisorsArray = assignedMembers
            }
        }
        //slotsAvailableLBL.text = content.slots_number?.description
        
        if let slots = content.slots_number {
            slotCount = slots
        }
        eventTitleLBL.text = content.title
        descriptionLBL.text = content.description
        locationLBL.text = content.address
        startDayNameLBL.text = "Start Date"//dayNameFrom(content.start_date, format: format)
        endDayNameLBL.text = "End Date"//dayNameFrom(content.end_date, format: format)
        startDateLBL.text = content.start_date//fullDateFrom(content.start_date, format: format)
        endDateLBL.text = content.end_date//fullDateFrom(content.end_date, format: format)
        switch content.category_id! {
        case 1:
            eventTypeLBL.text = "Conference"
            break
        case 2:
            eventTypeLBL.text = "Career Fair"
            break
        case 3:
            eventTypeLBL.text = "Exhibition"
            break
        default:
            eventTypeLBL.text = ""
        }

        if content.ticket_type == 1 {
            /// paid tickets
            entryTypePaidBTN.backgroundColor = blueColor
            entryTypePaidBTN.setTitleColor(.white, for: .normal)
            
            entryTypeFreeBTN.backgroundColor = .white
            entryTypeFreeBTN.setTitleColor(.darkGray, for: .normal)
            heightTicketPriceView.constant = 70
            
        }else if content.ticket_type == 0{
            /// free tickets
            entryTypePaidBTN.backgroundColor = .white
            entryTypePaidBTN.setTitleColor(.darkGray, for: .normal)
            
            entryTypeFreeBTN.backgroundColor = blueColor
            entryTypeFreeBTN.setTitleColor(.white, for: .normal)
            heightTicketPriceView.constant = 1
            
        }
        ticketPriceLBL.text = content.price.unwrap
        organisersCV.reloadData()
        sponsorsCV.reloadData()
        slotCV.reloadData()
    }
    
    
    /// clearing all variables
    private func clearData(){
        organisorsArray.removeAll()
        sponsorsArray.removeAll()
        sliderImagesArray.removeAll()
    }
    
    /// set images to slideshow
    private func setEventImages(){
        if isFromMyEvents {
            guard let eventImageURLs = content?.event_images, !eventImageURLs.isEmpty else { return }
            if let eventImagesURLArray = content?.event_images, !eventImagesURLArray.isEmpty {
                for itemURL in eventImagesURLArray {
                    sliderImagesArray.append(KingfisherSource(urlString: baseURLImage + itemURL)!)
                    imageLinkArray.append(itemURL)
                }
            }
            BaseThread.asyncMain {
                self.slideShowView.setImageInputs(self.sliderImagesArray)
                self.setUpTapGesture()
            }
        }else{
           
            guard !eventImages.isEmpty else { return }
            
            var sliderImages = [InputSource]()
            
            for item in eventImages {
                if let itemImage = item as? UIImage {
                    sliderImages.append(ImageSource(image: itemImage))
                }else if let itemURL = item as? String {
                    sliderImages.append(KingfisherSource(urlString: baseURLImage + itemURL)!)
                }
            }
            
            BaseThread.asyncMain {
                self.slideShowView.setImageInputs(sliderImages)
            }
        }
    }
    
    func setUpTapGesture(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        slideShowView.addGestureRecognizer(tapGesture)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer){
        slideShowView.presentFullScreenController(from: self)

        
//        print("Gesture Tapped")
//        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EXImageViewerVC") as! EXImageViewerVC
//        vc.sourceType = .link
//        vc.linkSource = imageLinkArray
//        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func gotoMapTapped(_ sender: UIButton) {
        self.openInMap()
    }
    
    func openInMap(){
        guard let latitude = content?.lat else{return}
        guard let longitude = content?.lon else {return}
        let mapAppURL = "comgooglemaps://?center=\(latitude),\(longitude)&zoom=18"
        let browserURL = "https://www.google.com/maps/@\(latitude),\(longitude),18z"
        if let _ = URL(string: "comgooglemaps://"), UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!){
            UIApplication.shared.openURL(URL(string: "\(mapAppURL)")!)
        }else if UIApplication.shared.canOpenURL(URL(string: "\(browserURL)")!){
            UIApplication.shared.openURL(URL(string: "\(browserURL)")!)
        }
    }
    
    @IBAction func bookSlotTapped(_ sender: UIButton) {
        
        bannerWithMessage(message: "Slot booking is under construction")
        
//        let formatter = DateFormatter()
//        formatter.dateFormat = "dd-MM-yyyy"
//        let todayy = Date()
//        //let now = formatter.string(from: todayy)
//
//        let EndDate = formatter.date(from: (content?.end_date)!)
//        if todayy > EndDate!
//        {
//
//            bannerWithMessage(message: "Exhibition Closed")
//
//        }else {
//
//
//            let vc = UIStoryboard(name: "SlotBookingWebview", bundle: nil).instantiateViewController(withIdentifier: "EXSlotBookingWebviewVC") as! EXSlotBookingWebviewVC
//            vc.isFromOrganisor = true
//            vc.content = self.content
//            //vc.eventDetails = self.eventDetails
//            self.present(vc, animated: true, completion: nil)
//
//        }

        
        
//        let vc = UIStoryboard(name: "SlotBookingWebview", bundle: nil).instantiateViewController(withIdentifier: "EXSlotBookingWebviewVC") as! EXSlotBookingWebviewVC
//       // vc.url = content!.center_url
//        self.present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func editBTNTapped(_ sender: UIButton)
    {
        let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXOrgEditEventVC") as! EXOrgEditEventVC
        nextVC.eventId = self.eventId
        self.present(nextVC, animated: true, completion: nil)
    }
    
    @IBAction func uploadBTNTapped(_ sender: BaseButton)
    {
        if isFromMyEvents
        {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let announceVC = storyBoard.instantiateViewController(withIdentifier: "EXCreateAnnounceVC") as! EXCreateAnnounceVC
            announceVC.eventID = String(describing: content!.id!)
            announceVC.userType = .Organiser
            self.present(announceVC, animated: true, completion: nil)
        }
        else
        {
            webPostEventDetails()
        }
    }
}

// CollectionView Delegates
extension EXEventDetailOrganiserVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == organisersCV
        {
            return organisorsArray.count

        }
        else if collectionView == sponsorsCV
        {
            return isFromMyEvents ? sponsorsArray.count : selectedSponsorsImages.count

        }
        else
        {
            return 0//isFromMyEvents ? (self.content?.slots?.count)! : 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == organisersCV
        {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EXADOrganisorsCVC", for: indexPath) as? EXADOrganisorsCVC else { return UICollectionViewCell() }
            
            let organisor = organisorsArray[indexPath.item]
            cell.organisorNameLBL.text = organisor.username
            cell.organisorImage.kf.setImage(with: URL(string: baseURLImage + organisor.image!), placeholder:#imageLiteral(resourceName: "noProfilePic.png") , options: nil, progressBlock: nil, completionHandler: nil)

            return cell
        }
        else if collectionView == sponsorsCV
        {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EXADSponsersCVC", for: indexPath) as? EXADSponsersCVC else { return UICollectionViewCell() }
            
            if isFromMyEvents {
                let sponsor = sponsorsArray[indexPath.item]
                cell.sponsorImage.kf.setImage(with: URL(string: baseURLImage + sponsor.image!), placeholder:#imageLiteral(resourceName: "noimage.jpg") , options: nil, progressBlock: nil, completionHandler: nil)
            }else{
                let sponsorImage = selectedSponsorsImages[indexPath.item]
                cell.sponsorImage.image = sponsorImage
            }

            return cell
        }
        
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SlotsCVC
            cell.slotNoLBL.text = String(indexPath.item+1)
            
            if (self.content?.slots?[indexPath.item].status != 0)
            {
                let url = URL(string: "\(baseURL)\(imageURL)\(String(describing: (self.content?.slots?[indexPath.item].booking_image)!))")
                cell.logoImage.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "noimage.jpg"))
            }

            if (self.content?.category_id! != 2) &&
                (self.content?.slots?[indexPath.item].price == 0.0 || self.content?.slots?[indexPath.item].size == 0.0)
            { cell.backgroundColor = UIColor.lightGray
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == organisersCV
        {
        }
        else if collectionView == sponsorsCV
        {
        }
        else
        {
            if self.content?.category_id! == 2 && (self.content?.slots?[indexPath.item].status == 0)
            {
                let storyBoard = UIStoryboard(name: "EXOrganiser", bundle: nil)
                let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXBookSpaceVC") as! EXBookSpaceVC
                
                let slotDetail = SlotModel()
                slotDetail.image = (self.content?.slots?[indexPath.item].image)!
                slotDetail.id = (self.content?.slots?[indexPath.item].id)!
                slotDetail.size = String((self.content?.slots?[indexPath.item].size)!)
                slotDetail.price = String((self.content?.slots?[indexPath.item].price)!)
                
                nextVC.slotDetails = slotDetail
                nextVC.eventTitle = self.content!.title!
                nextVC.eventDescription = self.content!.description!
                self.present(nextVC, animated: true, completion: nil)
            }
            else if (self.content?.slots?[indexPath.item].status != 0)
            {
                let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXBookedSlotDetailVC") as! EXBookedSlotDetailVC
                nextVC.slotDetail = (self.content?.slots?[indexPath.item])!
                nextVC.address = self.content!.address!
                nextVC.startDate = self.content!.start_date!
                nextVC.endDate = self.content!.end_date!
                nextVC.eventType = self.content!.category!
                self.present(nextVC, animated: true, completion: nil)
            }
            else if self.content?.slots?[indexPath.item].price == 0.0 || self.content?.slots?[indexPath.item].size == 0.0
            {
                
                bannerWithMessage(message: "Slot details not added for booking")
            }
            else
            {
                let storyBoard = UIStoryboard(name: "EXOrganiser", bundle: nil)
                let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXBookSpaceVC") as! EXBookSpaceVC
                
                let slotDetail = SlotModel()
                slotDetail.image = (self.content?.slots?[indexPath.item].image)!
                slotDetail.id = (self.content?.slots?[indexPath.item].id)!
                slotDetail.size = String((self.content?.slots?[indexPath.item].size)!)
                slotDetail.price = String((self.content?.slots?[indexPath.item].price)!)
                
                nextVC.slotDetails = slotDetail
                nextVC.eventTitle = self.content!.title!
                nextVC.eventDescription = self.content!.description!
                self.present(nextVC, animated: true, completion: nil)
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView == organisersCV
        { return CGSize(width: 80, height: 100)
        }
        else if collectionView == sponsorsCV
        { return CGSize(width: 120, height: 100)
        }
        else
        { return CGSize(width: (slotCV.bounds.width-25)/4, height: (slotCV.bounds.width-25)/4)
        }
    }
    
}

// Speaker TableView Delegates
extension EXEventDetailOrganiserVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if isFromMyEvents
        { return (self.content?.confernecedetails.count)!
        }
        else
        { return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! speakerConferenceCell
        
        if isFromMyEvents
        {
            cell.speakerName.text = self.content?.confernecedetails[indexPath.row].name
            let url = URL(string: "\(baseURL)\(imageURL)\(String(describing: self.content!.confernecedetails[indexPath.row].image))")
            cell.speakerImageView.kf.setImage(with: url, placeholder: UIImage(named: "noProfilePic.png"))
        }
        else
        {
            
        }
        
        return cell
    }
}

// Mark:- Handling web service
extension EXEventDetailOrganiserVC
{
    
    private func webPostEventDetails()
    {
        var parameter = editEventParameter
        debugPrint("editEventParameter",parameter as AnyObject)
        
        var imageParameter = [String:[UIImage]]()
        if let eventImages = parameter["pics"] as? [UIImage]
        {
            imageParameter["pics[]"] = eventImages
        }
        parameter.removeValue(forKey: "pics")
        parameter["category_id"] =  (editEventParameter["category_id"] as? Int)?.description
        parameter["entry_type"] = (editEventParameter["entry_type"] as? Int)?.description
        parameter["slots_num"] = (editEventParameter["slots_num"] as? Int)?.description
        parameter["sponsers"] =  (editEventParameter["sponsers"] as? [Int])?.description
        parameter["assign"] = (editEventParameter["assign"] as? [Int])?.description
        
        debugPrint("parameter:",parameter)
        
        Webservices.postMethodMultiPartImage(url: URLEditEvent + eventId.description, parameter: parameter, imageParameter: imageParameter) { (isSucceeded, response) in
            
            if isSucceeded {
                let message = response["Message"] as? String ?? "Event Updated"
                bannerWithMessage(message: message)
                BaseThread.asyncMainDelay(seconds: 2,
                {
                    self.dismissVC()
                })
            }
        }
        
    }
    
    private func dismissVC()
    {
        let myEventsVC = self.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController as! EXMyEventsOrganiserVC
        myEventsVC.isRequiredReload = true
        self.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
}

/*
 if isFromMyEvents {
 
 }else{
 let organisor = selectedOrganisors[indexPath.item]
 cell.organisorNameLBL.text = organisor.username
 cell.organisorImage.kf.setImage(with: URL(string: baseURLImage + organisor.image!), placeholder:#imageLiteral(resourceName: "3.jpg") , options: nil, progressBlock: nil, completionHandler: nil)
 
 }

 */
