//
//  EXAddAdressVC.swift
//  googleMapIntgrtn
//
//  Created by Janaina on 2/5/18.
//  Copyright © 2018 Janaina. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import MapKit
import DropDown

class EXOrgEditAddressVC: UIViewController,CLLocationManagerDelegate,GMSMapViewDelegate,UITextFieldDelegate
{
    @IBOutlet weak var navigationBarView: UIView!
    @IBOutlet weak var backBTN: UIButton!
    @IBOutlet var mapViewDisplayView: UIView!
    @IBOutlet weak var searchBarView: UIView!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var searchBTN: UIButton!
    @IBOutlet weak var addLocationBTN: UIButton!
    
    var mapView1 = GMSMapView()
    var gmsFetcher: GMSAutocompleteFetcher!
    var marker = GMSMarker()
    var userSetLatitude = 181.0
    var userSetLongitude = 181.0
    var userSetAddress = String()
    private var userSetLocation = ""
    let dropDown = DropDown()
    var resultsArray = [String]()
    var placeIDArray = [String]()
    var indexOfPlaceID = 0
    
    final var editEventParameter = Json()
    final var selectedSponsorsImages = [UIImage]()
    final var eventImages = [AnyObject]()
    final var assignedMembers = [ADAssignedMembersModel]()
    final var eventId: Int = 0
    
    var isFromAdminEdit = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        debugPrint("EventParameter:Address:",editEventParameter as AnyObject)
        setUpInterface()
    }
    
    private func setUpInterface()
    {
        if let address = self.editEventParameter["address"] as? String, address != "" {
            self.userSetAddress = address
        }
        
        if let lat = self.editEventParameter["lat"] as? String, lat != "" {
            self.userSetLatitude = Double(lat)!
        }
        
        if let lon = self.editEventParameter["lon"] as? String, lon != "" {
            self.userSetLongitude = Double(lon)!
        }
        
        if let location = self.editEventParameter["location"] as? String, location != "" {
            self.userSetLocation = location
        }
    }

    
    override func viewWillAppear(_ animated: Bool)
    {
        searchTF.delegate = self
        var openingLatitude = 9.9971
        var openingLongitude = 76.3028

        if userSetLatitude != 181.0 && userSetLongitude != 181.0 {
           openingLatitude = userSetLatitude
           openingLongitude = userSetLongitude
        }
        
        BaseThread.asyncMain {
            let camera = GMSCameraPosition.camera(withLatitude: openingLatitude, longitude: openingLongitude, zoom: 10)
            self.mapView1 = GMSMapView.map(withFrame: self.mapViewDisplayView.frame, camera: camera)
            self.mapView1.delegate = self
            self.view.addSubview(self.mapView1)
            self.mapView1.superview?.addSubview(self.addLocationBTN)
            self.mapView1.superview?.addSubview(self.searchBarView)
            if self.userSetLatitude != 181.0 && self.userSetLongitude != 181.0 {
                self.setMarkerOnExistingLocation()
            }
        }
    }

    
    private func setMarkerOnExistingLocation(){
        mapView1.clear()
        let coordinate = CLLocationCoordinate2D(latitude: userSetLatitude, longitude: userSetLongitude)
        marker = GMSMarker(position: coordinate)
        marker.snippet = userSetAddress
        marker.map = mapView1
        marker.appearAnimation = GMSMarkerAnimation.pop
        mapView1.selectedMarker = marker
        marker.icon = UIImage(named: "Location")
    }
    
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
   func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D)
    {
        mapView1.clear()
        userSetLatitude = coordinate.latitude
        userSetLongitude = coordinate.longitude
        
        marker = GMSMarker(position: coordinate)
        let location = CLLocation(latitude: userSetLatitude, longitude: userSetLongitude)

        fetchAddress(location: location)
        {place, subAdministrativeArea,postalCode,country  in

            print(place)
            debugPrint(subAdministrativeArea)
            
            self.userSetAddress = place + "," + subAdministrativeArea + "," + postalCode + "," + country
            self.marker.snippet = self.userSetAddress
            self.userSetLocation = place
       }
   
        marker.map = mapView
        marker.appearAnimation = GMSMarkerAnimation.pop
        mapView1.selectedMarker = marker
        marker.icon = UIImage(named: "Location")
        
   }
    
   func fetchAddress(location: CLLocation, completion: @escaping (String, String, String, String) -> ())
   {
        CLGeocoder().reverseGeocodeLocation(location)
        { placemarks, error in
            if let error = error
            {
                print(error)
            } else if let place = placemarks?.first?.name,
                let subAdministrativeArea = placemarks?.first?.subAdministrativeArea,
                let postalCode = placemarks?.first?.postalCode,
                let country =  placemarks?.first?.country
                
            {
                completion(place, subAdministrativeArea, postalCode, country)
            }
        }
    }
    
    
    
    @IBAction func searchBTNTapped(_ sender: UIButton)
    {
      guard let text = searchTF.text, !text.isEmpty else
      {
         mapView1.clear()
         return
      }
     self.locationOnMap()
        searchTF.text = ""
        mapView1.clear()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
      if let text = textField.text,
      let textRange = Range(range, in: text)
      {
               let updatedText = text.replacingCharacters(in: textRange,with: string)
               self.resultsArray.removeAll()
               self.placeIDArray.removeAll()
        
            gmsFetcher = GMSAutocompleteFetcher()
            gmsFetcher.delegate = self
            gmsFetcher?.sourceTextHasChanged(updatedText)
        }
        return true
    }
    

    func locationOnMap(){
       
      GMSPlacesClient.shared().lookUpPlaceID(self.placeIDArray[self.indexOfPlaceID])
       { (place, error) in
                
                if let error = error
                {
                    
                    print("lookup place id query error: \(error.localizedDescription)")
                    
                    return
                }
               guard let place = place else
               {
                    
                    print("No place details for \(self.placeIDArray[self.indexOfPlaceID])")
                
                    return
                }
        
        self.userSetLatitude = place.coordinate.latitude
        self.userSetLongitude = place.coordinate.longitude
        self.userSetAddress = place.formattedAddress!
        
        let locationNameArray = place.formattedAddress?.components(separatedBy: ",")
        self.userSetLocation = (locationNameArray?.isEmpty)! ? "" : locationNameArray![0]
        
        
        let camera = GMSCameraPosition.camera(withLatitude: self.userSetLatitude, longitude: self.userSetLongitude, zoom: 15)
        self.mapView1.camera = camera
        
        self.marker.position = CLLocationCoordinate2D(latitude: self.userSetLatitude, longitude: self.userSetLongitude)
        self.marker.snippet = self.userSetAddress
        self.marker.icon = UIImage(named: "Location")
        self.marker.map = self.mapView1
        self.marker.appearAnimation = GMSMarkerAnimation.pop
        self.mapView1.selectedMarker = self.marker
        
        self.addLocationBTN.isEnabled = true

        }
    }
    
  
    @IBAction func addLocationBTNTapped(_ sender: UIButton)
    {
        if validateData()
        {
            self.editEventParameter["address"] = self.userSetAddress
            self.editEventParameter["lat"] = self.userSetLatitude.description
            self.editEventParameter["lon"] = self.userSetLongitude.description
            self.editEventParameter["location"] = self.userSetLocation
            
            if isFromAdminEdit
            {
                let previousVC = self.presentingViewController as! EXAdminEditEventVC
                previousVC.editEventParameter = self.editEventParameter
                self.dismiss(animated: true, completion:
                {
                        previousVC.locationChanged()
                })

            }
            else
            {
                let previousVC = self.presentingViewController as! EXOrgEditEventVC
                previousVC.editEventParameter = self.editEventParameter
                self.dismiss(animated: true, completion:
                    {
                        previousVC.locationChanged()
                })

            }
            
        }

        debugPrint("addLocationBTNTapped",editEventParameter as AnyObject)
        
    }
    
    private func validateData() -> Bool{
        //return true
        
        if userSetLongitude != 181.0 &&
           userSetLatitude != 181.0 &&
           !userSetAddress.isEmpty &&
           !userSetLocation.isEmpty {
            return true
        }else{
            bannerWith(title: "Mandatory", andMessage: "Choose a location")
            return false
        }
    }
}

extension EXOrgEditAddressVC: GMSAutocompleteFetcherDelegate
{
    public func didAutocomplete(with predictions: [GMSAutocompletePrediction])
    {
        
        for prediction in predictions
        {
            
            if let prediction = prediction as GMSAutocompletePrediction!
            {
                self.resultsArray.append(prediction.attributedFullText.string)
                self.placeIDArray.append(prediction.placeID!)
                
            }
            dropDown.anchorView = searchTF
            dropDown.dataSource = resultsArray
            dropDown.direction = .bottom
            dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
            dropDown.selectionAction =
            {  index,item in
                self.indexOfPlaceID = index
                self.searchTF.text = item
            }
            dropDown.show()
        }
    }
    
    func didFailAutocompleteWithError(_ error: Error)
    {
        print(error)
    }
}

















