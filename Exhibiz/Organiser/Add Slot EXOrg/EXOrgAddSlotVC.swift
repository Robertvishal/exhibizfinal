//
//  EXEXAddSlotVC.swift
//  Exibiz_POC
//
//  Created by Appzoc on 20/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import OpalImagePicker

class EXOrgAddSlotVC: UIViewController {

    @IBOutlet var slotTable: UITableView!
    @IBOutlet var previewImage: UIView!
    @IBOutlet var slotImageView: UIImageView!
    
    final var eventId: Int = 0
    
    private var cellArray = [ADSlotsModel?]()
    private var deselectedCell:EXOrgAddSlotTVC?
    private var isRotatedArrowIMG: Bool = false
    private var selectedIndexPath: IndexPath?
    private var isExpandedCell: Bool = false
    private let placeholderImageName = "Upload Floor Plan"
    private let selectedImageName = "Slot_image.jpeg"
    private var selectedImage: UIImage?
    private let slotName = "Slot "
    
    private lazy var URLGetSlotDetails = "ExhibizWeb/backend/api/get/slots/"
    private lazy var URLUpdateSlotDetails = "ExhibizWeb/backend/api/add/slots"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        previewImage.frame.origin.x = self.view.frame.width
        webGetSlots()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if(touch.view == previewImage){
                UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseInOut, animations: {
                    self.previewImage.frame.origin.x = self.view.frame.width
                    self.previewImage.backgroundColor = .clear
                }, completion: nil)
            }

        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backBTNTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /// Image rotation animation
    private func rotateArrowIMG(of selectedCell: EXOrgAddSlotTVC){
        let towardsDown = CGFloat(Double.pi/2)
        let towardsRight = CGFloat(-Double.pi/2)
        
        BaseThread.asyncMain {
            UIView.animate(withDuration: 0.3, animations: {
                if self.deselectedCell == nil {
                    /// tapping on the cell first time
                    selectedCell.arrowIMG.transform = selectedCell.arrowIMG.transform.rotated(by: towardsDown)
                    self.isRotatedArrowIMG = true
                }else if self.deselectedCell == selectedCell {
                    /// tapping on already selected cell
                    if self.isRotatedArrowIMG {
                        selectedCell.arrowIMG.transform = selectedCell.arrowIMG.transform.rotated(by: towardsRight)
                        self.isRotatedArrowIMG = false
                    }else{
                        selectedCell.arrowIMG.transform = selectedCell.arrowIMG.transform.rotated(by: towardsDown)
                        self.isRotatedArrowIMG = true
                    }
                }else{
                    /// tapping on other cell not already selected cell
                    selectedCell.arrowIMG.transform = selectedCell.arrowIMG.transform.rotated(by: towardsDown)
                    guard let dCell = self.deselectedCell else { self.isRotatedArrowIMG = true; return }
                    if self.isRotatedArrowIMG {
                        dCell.arrowIMG.transform = dCell.arrowIMG.transform.rotated(by: towardsRight)
                    }
                    self.isRotatedArrowIMG = true
                }
                self.deselectedCell = selectedCell
            })
        }
        
    }

}


// Mark: - Handling tableview datasources and delegates

extension EXOrgAddSlotVC: UITableViewDataSource, UITableViewDelegate, OpalImagePickerControllerDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if selectedIndexPath == indexPath && !isExpandedCell {selectedIndexPath = nil}
        return selectedIndexPath == indexPath ? (isExpandedCell ? 222 :  57) :  57
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "EXOrgAddSlotTVC") as? EXOrgAddSlotTVC else { return UITableViewCell() }
        cell.slotTitleLBL.text = slotName + String(indexPath.row+1)
        cell.saveDetailsBTN.indexPath = indexPath
        cell.addImageBTN.indexPath = indexPath
        cell.openCloseCellBTN.indexPath = indexPath
        cell.showImageBTN.indexPath = indexPath
        cell.openCloseCellBTN.addTarget(self, action:  #selector(openCloseCellBTNTapped(_:)), for: .touchUpInside)
        cell.saveDetailsBTN.addTarget(self, action: #selector(saveDetailsBTNTapped(_ :)), for: .touchUpInside)
        cell.addImageBTN.addTarget(self, action: #selector(addImageBTNTapped(_:)), for: .touchUpInside)
        cell.showImageBTN.addTarget(self, action:  #selector(showImageBTNTapped(_:)), for: .touchUpInside)

        return cell
    }

    
    @objc func openCloseCellBTNTapped(_ sender: IPButton) {
        let indexPath = sender.indexPath!
        guard let selectedCell = slotTable.cellForRow(at: indexPath) as? EXOrgAddSlotTVC else { return }
        
        if selectedIndexPath == indexPath {
            isExpandedCell = isExpandedCell ? false : true
        }else if selectedIndexPath == nil {
            isExpandedCell = true
        }
        selectedIndexPath = indexPath
        updateContent(of: selectedCell, byIndexPath: selectedIndexPath!)
        slotTable.beginUpdates()
        self.rotateArrowIMG(of: selectedCell)
        slotTable.endUpdates()

    }
    private func updateContent(of cell : EXOrgAddSlotTVC, byIndexPath indexPath: IndexPath){
        if let content = cellArray[indexPath.row], content.price != nil,content.price != 0.0, content.size != nil,content.size != 0.0 {
            cell.sizeTF.text = content.size?.description
            cell.priceTF.text = content.price?.description
            if (content.image != nil && content.image != "") || content.currentImage != nil {
                cell.imageNameLBL.text = selectedImageName
            }else{
                cell.imageNameLBL.text = placeholderImageName
            }
        }else{
            cell.sizeTF.text = nil
            cell.priceTF.text = nil
            cell.imageNameLBL.text = placeholderImageName
            
        }
        cell.sizeTF.resignFirstResponder()
        cell.priceTF.resignFirstResponder()
        
        
    }
    
    @objc func showImageBTNTapped(_ sender: IPButton){
        if let content = cellArray[(sender.indexPath?.row)!] {
            if let urlImage = content.image, urlImage != "" {
                self.slotImageView.kf.setImage(with: URL(string: baseURLImage + urlImage))
                UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseInOut, animations: {
                    self.previewImage.frame.origin.x = 0.0
                    self.previewImage.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3042059075)
                }, completion: nil)
            }else {
                if let image = content.currentImage {
                    self.slotImageView.image = image
                    UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseInOut, animations: {
                        self.previewImage.frame.origin.x = 0.0
                        self.previewImage.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3042059075)
                    }, completion: nil)
                }
            }
            
        }
    }
    
    @objc func addImageBTNTapped(_ sender: IPButton){
        
        let imagePicker = OpalImagePickerController()
        imagePicker.imagePickerDelegate = self
        imagePicker.maximumSelectionsAllowed = 1
        present(imagePicker, animated: true, completion: nil)
    }
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        
        guard let selectedCell = slotTable.cellForRow(at: selectedIndexPath!) as? EXOrgAddSlotTVC else { return }
        if !images.isEmpty {
            selectedImage = images[0]
            selectedCell.imageNameLBL.text = selectedImageName
            
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    @objc func saveDetailsBTNTapped(_ sender: IPButton){
        
        guard let cell = slotTable.cellForRow(at: sender.indexPath!) as? EXOrgAddSlotTVC else { return }
        
        if validateData(of: cell) {
            if let cellData = cellArray[(sender.indexPath?.row)!] {
                cellData.size = Double(cell.sizeTF.text!)
                cellData.price = Double(cell.priceTF.text!)
                cellData.currentImage = selectedImage
                cellArray[(sender.indexPath?.row)!] = cellData
                webPostSlotDetails(cellData, slotImage: selectedImage)
            }
            
        }
        cell.sizeTF.resignFirstResponder()
        cell.priceTF.resignFirstResponder()
    }
    private func validateData(of cell: EXOrgAddSlotTVC) -> Bool{
        
        if !BaseValidator.isNotEmpty(string: cell.sizeTF.text) {
            bannerWithMessage(message: "Enter size")
            return false
        }else {
            if 0 == Int(cell.sizeTF.text!) || !BaseValidator.isValid(number: cell.sizeTF.text!) {
                bannerWithMessage(message: "Enter valid size")
                return false
            }
        }
        if !BaseValidator.isNotEmpty(string:  cell.priceTF.text) {
            bannerWithMessage(message: "Enter price")
            return false
        }else {
            if !BaseValidator.isValid(number: cell.priceTF.text!) {
                bannerWithMessage(message: "Enter valid price")
                return false
            }
        }
        if cell.imageNameLBL.text == placeholderImageName {
            bannerWithMessage(message: "Add image")
            return false
        }
        
        return true
    }

}


// Mark:- Handling web service

extension EXOrgAddSlotVC {
    
    private func webGetSlots(){
        Webservices.getMethod(url: URLGetSlotDetails + eventId.description) { (isSucceeded, response) in
            if isSucceeded {
                guard let array:JsonArray = response["Data"] as? Array, !array.isEmpty else { return }
                
                self.cellArray =  ADSlotsModel.getValues(from: array)
                
                BaseThread.asyncMain {
                    self.slotTable.reloadData()
                }
            
            }
            
        }
    }
    
    private func webPostSlotDetails(_ cellData: ADSlotsModel, slotImage: UIImage?){
        var params = Json()
        params["size"] = cellData.size?.description
        params["price"] = cellData.price?.description
        params["status"] = cellData.status?.description
        params["created_at"] = cellData.created_at
        params["slots_id"] = cellData.id?.description
        
        debugPrint(params as AnyObject)
        var imageParam = [String: [UIImage]]()
        if slotImage != nil {
            imageParam["image"] = [slotImage!]
        }
        
        Webservices.postMethodMultiPartImage(url: URLUpdateSlotDetails, parameter: params, imageParameter: imageParam) { (isSucceeded, response) in
            if isSucceeded
            {
                debugPrint(response as AnyObject)
                bannerWithMessage(message: "Slot Details Added Succesfully")
            }
        }

    }
    
    
    
}


extension String {
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
}
/*
 let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXOrgSlotImageShowVC") as! EXOrgSlotImageShowVC
 let transition = CATransition()
 transition.duration = 0.5
 transition.type = kCATransitionMoveIn
 transition.subtype = kCATransitionFromRight
 transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
 view.window!.layer.add(transition, forKey: kCATransition)
 view.window?.layer.backgroundColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1148598031)
 present(nextVC, animated: false, completion: nil)
 */
