//
//  EXEXAddSlotTVC.swift
//  Exibiz_POC
//
//  Created by Appzoc on 20/02/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class IPButton: UIButton {
    var indexPath:IndexPath?
    convenience init(indexPath: IndexPath) {
        self.init(frame:CGRect.zero)
        self.indexPath = indexPath
    }
}

class EXOrgAddSlotTVC: UITableViewCell {

    @IBOutlet var slotTitleLBL: UILabel!
    @IBOutlet var arrowIMG: UIImageView!
    @IBOutlet var openCloseCellBTN: IPButton!
    
    @IBOutlet var sizeTF: UITextField!
    @IBOutlet var priceTF: UITextField!
    @IBOutlet var imageNameLBL: UILabel!
    @IBOutlet var addImageBTN: IPButton!
    @IBOutlet var saveDetailsBTN: IPButton!
    @IBOutlet var showImageBTN: IPButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
