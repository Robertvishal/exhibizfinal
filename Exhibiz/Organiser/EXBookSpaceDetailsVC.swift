//
//  EXBookSpaceDetailsVC.swift
//  Exhibiz
//
//  Created by Appzoc on 15/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import PlaceholderTextView
import OpalImagePicker

class EXBookSpaceDetailsVC: UIViewController,OpalImagePickerControllerDelegate,UITextViewDelegate
{
    @IBOutlet var emailTV: PlaceholderTextView!
    @IBOutlet var companyTV: PlaceholderTextView!
    @IBOutlet var mobileTV: PlaceholderTextView!
    @IBOutlet var descriptionTV: PlaceholderTextView!
    @IBOutlet var addressTV: PlaceholderTextView!
    @IBOutlet var companyImage: UIImageView!
    @IBOutlet var imageBTN: UIButton!
    
    var slotStatus = "N"
    var exhibitorID = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    @IBAction func imageBTNTapped(_ sender: UIButton)
    {
        let imagePicker = OpalImagePickerController()
        imagePicker.maximumSelectionsAllowed = 1
        imagePicker.imagePickerDelegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage])
    {
        companyImage.image = images[0]
        imageBTN.setTitle("", for: .normal)
        picker.dismiss(animated: true, completion: nil)
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView == emailTV
        {
            Webservices.getMethod(url: "ExhibizWeb/backend/api/get/user_email?email=\(emailTV.text!)", errorBanner: false)
            { (isFetched, resultData) in
                if isFetched
                {
                    for item in resultData["Data"] as! [[String:Any]]
                    {
                        self.companyTV.text = item["company_name"] as? String ?? ""
                        self.mobileTV.text = item["contactno"] as? String ?? ""
                        self.exhibitorID = item["id"] as? String ?? ""
                    }
                }
                else
                { self.slotStatus = "Y"
                }
            }
        }
    }
    
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func bookSpaceBTNTapped(_ sender: UIButton)
    { let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXPaymentSelectVC") as! EXPaymentSelectVC
        
        var parameter = [String:Any]()
        
        parameter["companyname"] = self.companyTV.text!
        parameter["description"] = self.descriptionTV.text!
        parameter["address"] = self.addressTV.text!
        parameter["exhibition_id"] = ""
        parameter["email"] = self.emailTV.text!
        parameter["new"] = slotStatus
        parameter["phone"] = self.mobileTV.text!
        parameter["exhibitor_id"] = self.exhibitorID
        parameter["created_at"] = getCreatedDate()
        
        if companyImage.image == nil
        { bannerWithMessage(message: "Choose Company Logo")
        }
        else if emailTV.text.isEmpty
        { bannerWithMessage(message: "Enter Email")
        }
        else if companyTV.text.isEmpty
        { bannerWithMessage(message: "Enter Company")
        }
        else if mobileTV.text.isEmpty
        { bannerWithMessage(message: "Enter Mobile")
        }
        else if !BaseValidator.isValid(digits: mobileTV.text)
        { bannerWithMessage(message: "Enter Valid Mobile Number")
        }

        else if addressTV.text.isEmpty
        { bannerWithMessage(message: "Enter Address")
        }
        else
        {
            nextVC.parameter = parameter
            nextVC.companyImage = companyImage.image!
            self.present(nextVC, animated: true, completion: nil)
        }
    }
}
