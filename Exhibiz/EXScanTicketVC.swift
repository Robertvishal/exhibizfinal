//
//  EXScanTicketVC.swift
//  Exhibiz
//
//  Created by Appzoc on 21/06/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import AVFoundation

class NameCell: UITableViewCell
{
    @IBOutlet var nameLBL: UILabel!
    @IBOutlet var checkImage: UIImageView!
}

class EXScanTicketVC: UIViewController
{
    lazy var readerVC: QRCodeReaderViewController =
    {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
        }
        return QRCodeReaderViewController(builder: builder)
    }()
    
    @IBOutlet var contentTV: UITableView!
    @IBOutlet var tableVIewHeight: NSLayoutConstraint!
    @IBOutlet var confirmVisitBTN: BaseButton!
    
    var isReloadRequired = true
    var ticketsArray = [[String:Any]]()
    var selectedticketID = [String]()
    
    var userIDTemp = ""
    var eventIDTemp = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if isReloadRequired
        {
            isReloadRequired = false
            self.openScanner()
        }
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func confirmBTNTapped(_ sender: BaseButton)
    {
        print(self.selectedticketID.description)
        postConfirmVisit()
    }
    
    
    @IBAction func scanBTNTapped(_ sender: UIButton)
    { openScanner()
    }
    
    func openScanner()
    {
        readerVC.completionBlock =
        { (result: QRCodeReaderResult?) in
                self.readerVC.dismiss(animated: true, completion: nil)
            if result?.value != nil
            {
                print(result!.value)
                print(self.convertToDictionary(text: result!.value)!)
                let userID = self.convertToDictionary(text: result!.value)!["user_id"] as? String ?? ""
                let eventID = self.convertToDictionary(text: result!.value)!["event_id"] as? String ?? ""
                self.userIDTemp = userID
                self.eventIDTemp = eventID
                self.getUserTickets(userID: userID, eventID: eventID)
            }
            
        }
        
        // Presents the readerVC as modal form sheet
        readerVC.modalPresentationStyle = .popover
        present(readerVC, animated: true, completion: nil)
    }
    
    func convertToDictionary(text: String) -> [String: Any]?
    {
        if let data = text.data(using: .utf8)
        {
            do
            {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            }
            catch
            {
                print(error.localizedDescription)
            }
        }
        return [String:Any]()
    }
}

extension EXScanTicketVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return ticketsArray.count > 0 ? ticketsArray.count : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        self.tableVIewHeight.constant = self.contentTV.contentSize.height
        
        if ticketsArray.count > 0
        {
            confirmVisitBTN.isUserInteractionEnabled = true
            confirmVisitBTN.backgroundColor = blueColor
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! NameCell
            cell.nameLBL.text = ticketsArray[indexPath.row]["name"] as? String ?? ""
            
            if (ticketsArray[indexPath.row]["status"] as? String ?? "") == "1"
            {
                cell.nameLBL.textColor = UIColor.darkGray
            }
            else
            {
                cell.nameLBL.textColor = UIColor.lightGray
            }
            
            if selectedticketID.contains((ticketsArray[indexPath.row]["id"] as? String ?? ""))
            { cell.checkImage.image = #imageLiteral(resourceName: "checked")
            }
            else
            { cell.checkImage.image = nil
            }
            return cell
        }
        else
        {
            confirmVisitBTN.isUserInteractionEnabled = false
            confirmVisitBTN.backgroundColor = UIColor.lightGray
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "noCell")
            return cell!
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if (ticketsArray[indexPath.row]["status"] as? String ?? "") == "2"
        {
            bannerWith(title: "Sorry", andMessage: "This ticket is already marked as visited")
        }
        else if selectedticketID.contains((ticketsArray[indexPath.row]["id"] as? String ?? ""))
        {  selectedticketID = selectedticketID.filter {$0 != (ticketsArray[indexPath.row]["id"] as? String ?? "")}
        }
        else
        {  selectedticketID.append((ticketsArray[indexPath.row]["id"] as? String ?? ""))
        }
        contentTV.reloadData()
    }

}

// WebServices
extension EXScanTicketVC
{
    func getUserTickets(userID:String, eventID:String)
    {
        Webservices.getMethod(url: "ExhibizWeb/backend/api/get/usertickets/\(eventID)/\(userID)")
        { (isFetched, resultData) in
            if isFetched
            {
                self.selectedticketID.removeAll()
                self.ticketsArray.removeAll()
                let data = resultData["Data"] as? [[String:Any]] ?? [[String:Any]]()
                for items in data
                {
                    if items["status"] as? String ?? "" != "0"
                    {
                        self.ticketsArray.append(items)
                    }
                }
                self.contentTV.reloadData()
            }
        }
    }
    
    func postConfirmVisit()
    {
        let postParameter = self.selectedticketID.description
        print(postParameter)
        Webservices.postMethod(url: "ExhibizWeb/backend/api/update/ticket_status", parameter: ["ticket_id":postParameter])
        { (isFetched, resultData) in
            if isFetched
            {
                bannerWith(title: "Success", andMessage: "Selected Tickets are marked as visited")
                self.getUserTickets(userID: self.userIDTemp, eventID: self.eventIDTemp)
            }
        }
    }
}








