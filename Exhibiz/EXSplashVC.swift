//
//  EXSplashVC.swift
//  Exhibiz
//
//  Created by Appzoc on 02/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import Alamofire
import BRYXBanner

class EXSplashVC: UIViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(presentNextVC), userInfo: nil, repeats: false)
    }
    
    @objc func presentNextVC()
    {
        let nextVc = self.storyboard!.instantiateViewController(withIdentifier: "EXLoginVC")
        self.present(nextVc, animated: true, completion: nil)
    }
}


