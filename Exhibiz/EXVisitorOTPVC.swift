//
//  EXVisitorOTPVC.swift
//  Exhibiz
//
//  Created by Akhil Chandran on 17/11/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import Firebase

class EXVisitorOTPVC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var OTp_TF: UITextField!
    var parameterToPass = [String:Any]()
    var mob = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        
        
        //setGradientBackground()
        OTp_TF.keyboardType = UIKeyboardType.numberPad
        //pincodeField.becomeFirstResponder()
        OTp_TF.delegate = self
        
        
        let phoneNumber = mob
        let activity = ActivityIndicator()
        activity.start()
        
        
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                //self.showMessagePrompt(error.localizedDescription)
                
                debugPrint(error.localizedDescription)
                activity.stop()
                //showBanner(title: "Server error", message: "", style: .warning, color: .red)
                BaseAlert.alert(withTitle: "Error", message: error.localizedDescription, tintColor: .black, andPresentOn: self)
                return
            }
            else
            {
                BaseAlert.alert(withTitle: "Otp Sent Successfully", message:"", tintColor: .blue, andPresentOn: self)
                
                debugPrint(verificationID ?? "")
                debugPrint(verificationID ?? "")
                UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                
                activity.stop()
                
            }
            
            // Sign in using the verificationID and the code sent to the user
            // ...
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBOutlet var backBtn: UIButton!
    
    @IBAction func ProceedTap(_ sender: BaseButton) {
        
        if OTp_TF.text?.count == 6
        {
            self.verify(code: OTp_TF.text!)
        }
        else
        {
            BaseAlert.alert(withTitle: "Enter Valid OTP", message:"", tintColor: .blue, andPresentOn: self)
            
        }
        
    }
    
    @IBAction func backTAp(_ sender: UIButton) {
        
        self.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
        
    }
    
    
    func verify(code:String) {
        
        
        
        print(code)
        
        let activity = ActivityIndicator()
        activity.start()
        
        // This test verification code is specified for the given test phone number in the developer console.
        let testVerificationCode = code
        
        let verificationID = UserDefaults.standard.string(forKey: "authVerificationID")
        
        print(verificationID)
        
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID ?? "",
            verificationCode: testVerificationCode)
        
        
        
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if let error = error {
                // ...
                
                debugPrint(error.localizedDescription)
                activity.stop()
                BaseAlert.alert(withTitle: "Error", message: error.localizedDescription, tintColor: .black, andPresentOn: self)
                return
            }
            else
            {
                debugPrint("login")
                print(authResult?.user.uid ?? "xxx")
                activity.stop()
                print(authResult?.user.phoneNumber ?? "0")
                UserDefaults.standard.set(authResult?.user.phoneNumber ?? "0", forKey: "UserMobile")
                self.callWeb()
            }
            // User is signed in
            // ...
            
            
        }
        
        
        
        
        
    }
    
    
    
    
    func callWeb()
    {
        
                Webservices.postMethod(url: registrationApi, parameter: parameterToPass)
                { (isFetched, resultData) in
                    if isFetched
                    {
                        bannerWithMessage(message: resultData["Message"] as? String ?? "")
                        
                        self.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)

                    }
                }
    
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
