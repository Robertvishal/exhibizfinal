//
//  AppDelegate.swift
//  Exhibiz
//
//  Created by Appzoc on 02/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import UserNotifications
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleSignIn
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate
{
    var window: UIWindow?
    static var userDetails = UserDetails()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
        
        // old credential
//        GMSServices.provideAPIKey("AIzaSyDy6NraBrsnBzXQqNUuB1ASyug_xKx7-hU")
//        GMSPlacesClient.provideAPIKey("AIzaSyDy6NraBrsnBzXQqNUuB1ASyug_xKx7-hU")
        
        // new credential
        GMSServices.provideAPIKey("AIzaSyATaZozj2ZM78FZcbZLjQnwwgx50BIq3v0")
        GMSPlacesClient.provideAPIKey("AIzaSyATaZozj2ZM78FZcbZLjQnwwgx50BIq3v0")
        
        GIDSignIn.sharedInstance().clientID = "2679404113-18h9gcsl2nmu6bm8t9tqbjmu8p5mne9g.apps.googleusercontent.com"
        FirebaseApp.configure()
        registerForPushNotifications()
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool
    {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }

    func applicationWillResignActive(_ application: UIApplication)
    {
    }

    func applicationDidEnterBackground(_ application: UIApplication)
    {
    }

    func applicationWillEnterForeground(_ application: UIApplication)
    {
    }

    func applicationDidBecomeActive(_ application: UIApplication)
    {
    }

    func applicationWillTerminate(_ application: UIApplication)
    {
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        UserDefaults.standard.set(deviceTokenString, forKey: "DeviceToken")
        print("APNs device token: \(deviceTokenString)")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error)
    {
        UserDefaults.standard.set("123456", forKey: "DeviceToken")
        print(error)
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any])
    {
        print("Push notification received: \(String(describing: data["aps"])),\(data)")
        gotoNotification()
        print("Push Received")
    }
    
    func registerForPushNotifications() {
        if #available(iOS 10, *)
        {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){
                (granted, error) in
                guard granted else { return }
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
        else if #available(iOS 9, *)
        {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    func gotoNotification(){
        if Credentials.shared.id == ""{
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EXLoginVC") as! EXLoginVC
            let navigationController = UINavigationController.init(rootViewController: vc)
            navigationController.isNavigationBarHidden = true
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()
        }else{
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EXAnnounceListVC") as! EXAnnounceListVC
            vc.userType = .Visitor
            let navigationController = UINavigationController.init(rootViewController: vc)
            navigationController.isNavigationBarHidden = true
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()
        }
    }
    
}

class UserDetails
{
    var userID = ""
}





