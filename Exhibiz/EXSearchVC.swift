//
//  EXSearchVC.swift
//  Exhibiz
//
//  Created by Appzoc on 16/04/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit

class searchCell: UITableViewCell
{
    @IBOutlet var eventNameLBL: UILabel!
    @IBOutlet var locationLBL: UILabel!
}


class EXSearchVC: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet var searchTF: UITextField!
    @IBOutlet var contentTV: UITableView!
    
    var eventList = [EventListModel]()
    var tempEventList = [EventListModel]()
    
    var contentArray = [ADFuturePresentPastModel]()
    var tempContentArray = [ADFuturePresentPastModel]()
    
    var fromUser = UserType.Visitor
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        searchTF.becomeFirstResponder()
    }

    @IBAction func closeBTNTapped(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func searchTextChanged(_ sender: UITextField)
    {
        tempEventList.removeAll()
        tempContentArray.removeAll()
        
        if fromUser == .Admin || fromUser == .Organiser
        {
            for items in contentArray
            { if items.title!.lowercased().range(of: sender.text!.lowercased()) != nil
                { tempContentArray.append(items)
                }
            }
        }
        else if fromUser == .Exhibitor || fromUser == .Visitor
        {
            for items in eventList
            { if items.title.lowercased().range(of: sender.text!.lowercased()) != nil
                { tempEventList.append(items)
                }
            }
        }
        contentTV.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if fromUser == .Admin || fromUser == .Organiser
        { return tempContentArray.count
        }
        else
        { return tempEventList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! searchCell
        
        if fromUser == .Admin || fromUser == .Organiser
        {
            cell.eventNameLBL.text = tempContentArray[indexPath.row].title
            cell.locationLBL.text = tempContentArray[indexPath.row].address
        }
        else
        {
            cell.eventNameLBL.text = tempEventList[indexPath.row].title
            cell.locationLBL.text = tempEventList[indexPath.row].address
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        switch fromUser
        {
        case .Admin:
            let previousVC = self.presentingViewController as! EXMyEventsVC
            self.dismiss(animated: true)
            {
                previousVC.selectedFromSearch(eventModel: self.tempContentArray[indexPath.row])
            }
            
        case .Organiser:
            let previousVC = self.presentingViewController as! EXMyEventsOrganiserVC
            self.dismiss(animated: true)
            {
                previousVC.selectedFromSearch(eventModel: self.tempContentArray[indexPath.row])
            }
            
        case .Exhibitor:
            let previousVC = self.presentingViewController as! EXAllEventsExhibitorVC
            self.dismiss(animated: true)
            {
                previousVC.selectedFromSearch(eventModel: self.tempEventList[indexPath.row])
            }
            
        case .Visitor:
            let previousVC = self.presentingViewController as! EXAllEventsVisitorVC
            self.dismiss(animated: true)
            {
                previousVC.selectedFromSearch(eventModel: self.tempEventList[indexPath.row])
            }
        }
        
    }
}




