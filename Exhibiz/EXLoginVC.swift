//
//  EXLoginVC.swift
//  Exhibiz
//
//  Created by Appzoc on 23/04/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import FBSDKCoreKit
import GoogleSignIn

class EXLoginVC: UIViewController, GIDSignInUIDelegate, GIDSignInDelegate
{
    @IBOutlet var usernameTF: UITextField!
    @IBOutlet var passwordTF: UITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    @IBAction func nextBTNTapped(_ sender: UIButton)
    {
        if isValid()
        { loginWebCall()
        }
    }
    
    @IBAction func forgotBTNTapped(_ sender: UIButton)
    {
        let alert = UIAlertController(title: "Forgot Password ?", message: "Enter your email to reset password", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.text = ""
        }
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler:
            { [weak alert] (_) in
                let textField = alert?.textFields![0]
                //print("Text field: \(textField!.text!)")
                if BaseValidator.isValid(email: textField!.text!)
                {
                    //print("ExhibizWeb/backend/api/forgotpassword/\(textField!.text!)")
                    Webservices.postMethod(url: "ExhibizWeb/backend/api/forgotpassword/\(textField!.text!)",
                        parameter: [String:Any](),
                        CompletionHandler:
                        { (isFetched, result) in
                            if isFetched
                            {
                                bannerWithMessage(message: "Password reset link has been sent to your Email")
                            }
                        })
                }
                else
                { bannerWithMessage(message: "Enter a valid email")
                  self.forgotBTNTapped(sender)
                }
        }))
        alert.addAction((UIAlertAction(title: "CANCEL", style: .cancel, handler: nil)))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func signUpBTNTapped(_ sender: UIButton)
    {
        let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXVisitorSignUpVC")
        self.present(nextVC, animated: true, completion: nil)
    }
    
    @IBAction func fbBTNTapped(_ sender: UIButton)
    {
        //when login button clicked
        let loginManager = LoginManager()
        loginManager.logIn([ .publicProfile, .email ], viewController: self)
        { loginResult in
            switch loginResult
            {
                case .failed(let error):
                    print(error)
                case .cancelled:
                    print("User cancelled login.")
            case .success( _, _, _):
                    self.getFBUserData()
            }
        }
    }
    
    //function is fetching the user data
    func getFBUserData()
    {
        if((FBSDKAccessToken.current()) != nil)
        {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"]).start(completionHandler:
            { (connection, result, error) -> Void in
                if (error == nil)
                {
                    let data = result as? [String:Any] ?? [String:Any]()
                    let email = data["email"] as? String ?? ""
                    let id = data["id"] as? String ?? ""
                    let name = data["name"] as? String ?? ""
                    print(data)
                    self.socialRegisterWebCall(email: email, username: name, type: "1", id: "\(id)")
                }
            })
        }
    }
    
    
    @IBAction func gPlusBTNTapped(_ sender: UIButton)
    {
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
                     withError error: Error!)
    {
        if (error == nil)
        {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            
            if user.profile.hasImage
            {
                print(user.profile.imageURL(withDimension: 200))
            }
            self.socialRegisterWebCall(email: email!, username: fullName!, type: "2", id: String(describing: userId!))
        }
        else
        {
            print("\(error)")
        }
    }
    
    
    
    func isValid() -> Bool
    { if usernameTF.text!.isEmpty
    { bannerWithMessage(message: "Enter Email")
        return false
    }
    else if passwordTF.text!.isEmpty
    { bannerWithMessage(message: "Enter Password")
        return false
    }
    else
    { return true
        }
    }
    
    func loginWebCall()
    {
        var parameter = [String:String]()
        parameter["email"] = usernameTF.text!
        parameter["password"] = passwordTF.text!
        parameter["device_id"] = UserDefaults.standard.string(forKey: "DeviceToken")
        parameter["os"] = "I"
        
        print(parameter)
        Webservices.postMethod(url: loginApi, parameter: parameter)
        { (isFetched, resultData) in
            if isFetched
            {
                self.loginWithUserType(resultData: resultData)
            }
        }
    }
}


extension EXLoginVC
{
    func socialRegisterWebCall(email:String,
                               username:String,
                               type:String,
                               id:String)
    {
        var parameter = [String:Any]()
        parameter["email"] = email
        parameter["password"] = ""
        parameter["device_id"] = UserDefaults.standard.string(forKey: "DeviceToken")
        parameter["os"] = "I"
        parameter["phone"] = ""
        parameter["username"] = username
        parameter["interest"] = "3"
        parameter["usertype"] = "[5]"
        parameter["signuptype"] = type
        parameter["socialid"] = id
        
        print(parameter)
        Webservices.postMethod(url: registrationApi, parameter: parameter)
        { (isFetched, resultData) in
            if isFetched
            {
                self.loginWithUserType(resultData: resultData)
            }
        }
    }
    
    func loginWithUserType(resultData:[String:Any])
    {
        let userID = ((resultData["Data"] as! [[String:Any]])[0])["id"] as? String ?? ""
        let imageURL = ((resultData["Data"] as! [[String:Any]])[0])["image"] as? String ?? ""
        let userInterest = ((resultData["Data"] as! [[String:Any]])[0])["interest"] as? String ?? ""
        let contactNo = ((resultData["Data"] as! [[String:Any]])[0])["contactno"] as? String ?? ""
        let userEmail = ((resultData["Data"] as! [[String:Any]])[0])["email"] as? String ?? ""
        let userType = ((resultData["Data"] as! [[String:Any]])[0])["type_id"] as? String ?? ""
        
        if userType == "4"
        {
            let username = ((resultData["Data"] as! [[String:Any]])[0])["company_name"] as? String ?? ""
            UserDefaults.standard.set(username, forKey: "Username")
        }
        else
        {
            let username = ((resultData["Data"] as! [[String:Any]])[0])["username"] as? String ?? ""
            UserDefaults.standard.set(username, forKey: "Username")
        }
        
        UserDefaults.standard.set(userID, forKey: "UserID")
        UserDefaults.standard.set(imageURL, forKey: "ImageURL")
        UserDefaults.standard.set(userEmail, forKey: "UserEmail")
        UserDefaults.standard.set(contactNo, forKey: "ContactNo")
        UserDefaults.standard.set(userInterest, forKey: "UserInterest")
        UserDefaults.standard.set(self.passwordTF.text!, forKey: "UserPassword")
        
        if userType == "2"
        {
            Credentials.shared.id = userID
            Credentials.shared.email = userEmail
            
            let isShow = 1//resultData["Isshowbanner"] as! Int
            //let ShowUrl = resultData["Url"] as! String
            
            if isShow == 1
            {
                let storyBoard = UIStoryboard(name: "EXAdmin", bundle: nil)
                let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXMyEventsVC") as! EXMyEventsVC
                nextVC.isRequiredReload = true
                self.present(nextVC, animated: true, completion: nil)
            }
            else
            {
                let storyBoard = UIStoryboard(name: "EXAdmin", bundle: nil)
                let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXMyEventsVC") as! EXMyEventsVC
                nextVC.isRequiredReload = true
                self.present(nextVC, animated: true, completion: nil)
            }
        }
        else if userType == "3"
        {
            BaseMemmory.write(object: userEmail, forKey: "userName")
            BaseMemmory.write(object: userID, forKey: "userId")
            
            Credentials.shared.id = userID
            Credentials.shared.email = userEmail
            
            let storyBoard = UIStoryboard(name: "EXOrganiser", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXMyEventsOrganiserVC") as! EXMyEventsOrganiserVC
            nextVC.isRequiredReload = true
            self.present(nextVC, animated: true, completion: nil)
        }
        else if userType == "4"
        {
            let storyBoard = UIStoryboard(name: "EXExhibitor", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXAllEventsExhibitorVC") as! EXAllEventsExhibitorVC
            nextVC.isReloadRequired = true
            self.present(nextVC, animated: true, completion: nil)
        }
        else if userType == "5"
        {
            let storyBoard = UIStoryboard(name: "EXVisitor", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXAllEventsVisitorVC") as! EXAllEventsVisitorVC
            nextVC.isReloadRequired = true
            self.present(nextVC, animated: true, completion: nil)
        }
        else
        {
            bannerWithMessage(message: "User Type Error")
        }
    }
}





