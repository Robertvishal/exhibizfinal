//
//  EXEditProfileVC.swift
//  Exhibiz
//
//  Created by Appzoc on 10/04/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import OpalImagePicker
import ALCameraViewController
import Kingfisher
import BRYXBanner

class EXEditProfileVC: UIViewController,OpalImagePickerControllerDelegate
{
    @IBOutlet var profileImage: BaseImageView!
    @IBOutlet var userNameTF: UITextField!
    @IBOutlet var passwordTF: UITextField!
    @IBOutlet var emailTF: UITextField!
    @IBOutlet var mobileTF: UITextField!
    
    @IBOutlet var interestHeight: NSLayoutConstraint!
    
    var isVisitor = false
    var selectedUserInterest = 0
    var selectedProfileImage = [UIImage]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        profileImage.isUserInteractionEnabled = true
        profileImage.addGestureRecognizer(tapGestureRecognizer)
        mobileTF.isUserInteractionEnabled = false
        emailTF.isUserInteractionEnabled = false

        setUpInterface()
        
    }
    
    private func setUpInterface() {
        BaseThread.asyncMain {
            self.userNameTF.text = UserDefaults.standard.string(forKey: "Username")
            self.passwordTF.text = UserDefaults.standard.string(forKey: "UserPassword")
            self.emailTF.text = UserDefaults.standard.string(forKey: "UserEmail")
            self.mobileTF.text = UserDefaults.standard.string(forKey: "ContactNo")
            
            if self.isVisitor
            { self.selectedUserInterest = Int(UserDefaults.standard.string(forKey: "UserInterest") ?? "0")!
            }
            
            let url = URL(string: "\(baseURL)\(imageURL)\(UserDefaults.standard.string(forKey: "ImageURL")!)")
            self.profileImage.kf.setImage(with: url, placeholder: UIImage(named: "noProfilePic.png"))

        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        if isVisitor == true
        { interestHeight.constant = 40
        }
        else
        { interestHeight.constant = 0
        }
    }

    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
//        let tappedImage = tapGestureRecognizer.view as! UIImageView
        let imagePicker = OpalImagePickerController()
        imagePicker.imagePickerDelegate = self
        imagePicker.maximumSelectionsAllowed = 1
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage])
    {
        profileImage.image = images[0]
        selectedProfileImage.append(images[0])
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func showPasswordBTNTapped(_ sender: UIButton)
    {
        if sender.image(for: .normal) == #imageLiteral(resourceName: "showPassword.png")
        {
            sender.setImage(#imageLiteral(resourceName: "hidePassword.png"), for: .normal)
            passwordTF.isSecureTextEntry = false
        }
        else
        {
            sender.setImage(#imageLiteral(resourceName: "showPassword.png"), for: .normal)
            passwordTF.isSecureTextEntry = true
        }
    }
    
    @IBAction func interestBTNTapped(_ sender: BaseButton)
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXVisitorInterestVC") as! EXVisitorInterestVC
        nextVC.fromEdit = true
        nextVC.selectedInterest = selectedUserInterest
        self.present(nextVC, animated: true, completion: nil)
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneBTNTapped(_ sender: BaseButton)
    {
        if isValid()
        {
            var parameter = [String:String]()
            parameter["name"] = userNameTF.text!
            parameter["password"] = passwordTF.text!
            parameter["emailid"] = emailTF.text!
            parameter["contact"] = mobileTF.text!
            parameter["interest"] = String(selectedUserInterest)
            
            let userID = UserDefaults.standard.string(forKey: "UserID")!
            let userType = isVisitor ? "5" : "4"
            
            //print("ExhibizWeb/backend/api/edit/profile/\(String(describing: userID))/\(userType)")
            //print(parameter)
            Webservices.postMethodMultiPartImageWithResponseReturnIfFailure(url: "ExhibizWeb/backend/api/edit/profile/\(userID)/\(userType)",
                                                parameter: parameter,
                                                imageParameter: ["Image":selectedProfileImage],
                                                CompletionHandler:
           { (isFetched, result) in
             if isFetched
             {
                self.restoreUserDefaults(resultData: result)
             }else {
                
                self.restoreUserDefaults(resultData: result)
            }
           })
        }
    }
    
    func restoreUserDefaults(resultData:[String:Any])
    {
        
        guard let result = resultData["Data"] as? [Json] ,result[0].index(forKey: "contactno") != nil, result[0].index(forKey: "email") != nil else {
            let Message = resultData["Message"] as? String ?? "Error occured while fetching data"
            Banner(title: "", subtitle: Message, backgroundColor: bannerRed).show(duration: 3)

            return
        }
        
    

        let userID = ((resultData["Data"] as! [[String:Any]])[0])["id"] as? String ?? ""
        let imageURL = ((resultData["Data"] as! [[String:Any]])[0])["image"] as? String ?? ""
        let userInterest = ((resultData["Data"] as! [[String:Any]])[0])["interest"] as? String ?? ""
        let contactNo = ((resultData["Data"] as! [[String:Any]])[0])["contactno"] as? String ?? ""
        let userEmail = ((resultData["Data"] as! [[String:Any]])[0])["email"] as? String ?? ""
        
        if isVisitor
        {
            let username = ((resultData["Data"] as! [[String:Any]])[0])["username"] as? String ?? ""
            UserDefaults.standard.set(username, forKey: "Username")
        }
        else
        {
            let username = ((resultData["Data"] as! [[String:Any]])[0])["company_name"] as? String ?? ""
            UserDefaults.standard.set(username, forKey: "Username")
        }
        UserDefaults.standard.set(userID, forKey: "UserID")
        UserDefaults.standard.set(imageURL, forKey: "ImageURL")
        UserDefaults.standard.set(userEmail, forKey: "UserEmail")
        UserDefaults.standard.set(contactNo, forKey: "ContactNo")
        UserDefaults.standard.set(userInterest, forKey: "UserInterest")
        UserDefaults.standard.set(self.passwordTF.text!, forKey: "UserPassword")
        
        setUpInterface()
        
        self.dismiss(animated: true, completion:
        {
            Banner(subtitle: "Profile details updated", backgroundColor: greenColour).show(duration: 3)
        })
        
    }
    
    
    func isValid() -> Bool
    {
        if userNameTF.text!.isEmpty
        { bannerWithMessage(message: "Name cannot be left blank")
          return false
        }
        else if passwordTF.text!.isEmpty
        { bannerWithMessage(message: "Password cannot be left blank")
          return false
        }
        else if emailTF.text!.isEmpty
        { bannerWithMessage(message: "Email cannot be left blank")
          return false
        }
        else if mobileTF.text!.isEmpty
        { bannerWithMessage(message: "Mobile cannot be left blank")
          return false
        }
        else if isVisitor && selectedUserInterest == 0
        { bannerWithMessage(message: "Choose an area of interest")
          return false
        }
        else
        {
            if BaseValidator.isValid(digits: mobileTF.text!) {
                return true

            }else {
                bannerWithMessage(message: "Enter valid mobile number")
                return false
            }
            
        }
    }

}






