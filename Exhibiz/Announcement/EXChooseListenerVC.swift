//
//  EXChooseListenerVC.swift
//  Exhibiz
//
//  Created by Appzoc on 20/04/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import M13Checkbox

class EXChooseListenerVC: UIViewController
{
    @IBOutlet var organiserCheckBox: M13Checkbox!
    @IBOutlet var exhibitorCheckBox: M13Checkbox!
    @IBOutlet var visitorCheckBox: M13Checkbox!
    
    @IBOutlet var organiserHeight: NSLayoutConstraint!
    @IBOutlet var exhibitorHeight: NSLayoutConstraint!
    @IBOutlet var visitorHeight: NSLayoutConstraint!
    
    var userType = UserType.Admin
    var selectedListeners = [String]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if userType == .Admin
        {
        }
        if userType == .Organiser
        { organiserHeight.constant = 0
        }
        if userType == .Exhibitor
        { organiserHeight.constant = 0
          exhibitorHeight.constant = 0
        }
        
        if selectedListeners.contains("3")
        {
            organiserCheckBox.setCheckState(.checked, animated: true)
        }
        if selectedListeners.contains("4")
        {
            exhibitorCheckBox.setCheckState(.checked, animated: true)
        }
        if selectedListeners.contains("5")
        {
            visitorCheckBox.setCheckState(.checked, animated: true)
        }
    }
    
    @IBAction func organiserBTNTapped(_ sender: UIButton)
    {
        if organiserCheckBox.checkState == .checked
        { organiserCheckBox.setCheckState(.unchecked, animated: true)
          selectedListeners = selectedListeners.filter { $0 != "3" }
        }
        else
        { organiserCheckBox.setCheckState(.checked, animated: true)
          selectedListeners.append("3")
        }
    }
    
    @IBAction func exhibitorBTNTapped(_ sender: UIButton)
    {
        if exhibitorCheckBox.checkState == .checked
        { exhibitorCheckBox.setCheckState(.unchecked, animated: true)
          selectedListeners = selectedListeners.filter { $0 != "4" }
        }
        else
        { exhibitorCheckBox.setCheckState(.checked, animated: true)
          selectedListeners.append("4")
        }
    }
    
    @IBAction func visitorBTNTapped(_ sender: UIButton)
    {
        if visitorCheckBox.checkState == .checked
        { visitorCheckBox.setCheckState(.unchecked, animated: true)
          selectedListeners = selectedListeners.filter { $0 != "5" }
        }
        else
        { visitorCheckBox.setCheckState(.checked, animated: true)
          selectedListeners.append("5")
        }
    }
    
    
    @IBAction func doneBTNTapped(_ sender: UIButton)
    {
        let previousVC = self.presentingViewController as! EXCreateAnnounceVC
        previousVC.selectedListeners = self.selectedListeners
        self.dismiss(animated: true, completion: {previousVC.listenersSelected()})
    }
    
    @IBAction func cancelBTNTapped(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}
