//
//  EXCreateAnnounceVC.swift
//  Exhibiz
//
//  Created by Appzoc on 16/04/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import PlaceholderTextView
import OpalImagePicker
import BRYXBanner

class imageCellAnnounce: UICollectionViewCell
{
    @IBOutlet var imageView: UIImageView!
}

class EXCreateAnnounceVC: UIViewController,OpalImagePickerControllerDelegate
{
    @IBOutlet var announceTitleTxtV: PlaceholderTextView!
    @IBOutlet var descriptionTxtV: PlaceholderTextView!
    @IBOutlet var listenersLBL: PlaceholderTextView!
    @IBOutlet var contentCV: UICollectionView!
    
    var eventID = ""
    var selectedImage:UIImage!
    var userType = UserType.Admin
    var selectedListeners = [String]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        print(eventID)
        print(userType)
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func chooseListenersBTNTapped(_ sender: UIButton)
    {
        let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "EXChooseListenerVC") as! EXChooseListenerVC
        nextVC.userType = self.userType
        nextVC.selectedListeners = self.selectedListeners
        self.present(nextVC, animated: true, completion: nil)
    }
    
    func listenersSelected()
    {
        var stringArray = [String]()
        if selectedListeners.contains("3")
        {  stringArray.append("Organisers")
        }
        if selectedListeners.contains("4")
        {  stringArray.append("Exhibitors")
        }
        if selectedListeners.contains("5")
        {  stringArray.append("Visitors")
        }
        listenersLBL.text = stringArray.joined(separator: ", ")
    }
    
    @IBAction func announceBTNTapped(_ sender: BaseButton)
    {
        if isValid()
        {
            if userType != .Admin
            {
                selectedListeners.append("2")
            }
            var parameter = [String:String]()
            parameter["userid"] = UserDefaults.standard.string(forKey: "UserID")!
            parameter["type_of_users"] = String(describing: selectedListeners)
            parameter["title"] = announceTitleTxtV.text
            parameter["text"] = descriptionTxtV.text
            parameter["exhibition_id"] = "0"
            
            print(selectedListeners)
            webCallPostComment(parameter: parameter, image: selectedImage)
        }
    }
    
    func isValid() -> Bool
    {
        if announceTitleTxtV.text.isEmpty
        { bannerWithMessage(message: "Enter Announcement Title")
          return false
        }
        else if descriptionTxtV.text.isEmpty
        {   bannerWithMessage(message: "Enter description")
            return false
        }
        else if selectedImage == nil
        {
            bannerWithMessage(message: "Choose an image")
            return false
        }
        else if selectedListeners.isEmpty
        {
            bannerWithMessage(message: "Choose type of listeners")
            return false
        }
        else
        {   return true
        }
    }
    
}

// CollectionView Delegates
extension EXCreateAnnounceVC: UICollectionViewDelegate,UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if self.selectedImage == nil
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell2", for: indexPath)
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell1", for: indexPath) as! imageCellAnnounce
            cell.imageView.image = selectedImage
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let imagePicker = OpalImagePickerController()
        imagePicker.imagePickerDelegate = self
        imagePicker.maximumSelectionsAllowed = 1
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage])
    {
        selectedImage = images[0]
        contentCV.reloadData()
        picker.dismiss(animated: true, completion: nil)
    }

}

// Web Service
extension EXCreateAnnounceVC
{
    func webCallPostComment(parameter:[String:String], image:UIImage)
    {
        print(parameter)
        
        Webservices.postMethodMultiPartImage(url: "ExhibizWeb/backend/api/send/announcements/\(self.eventID)",
            parameter: parameter,
            imageParameter: ["image" : [image]])
        { (isFetched, resultData) in
            if isFetched
            {
                print(resultData)
                self.dismiss(animated: true, completion:
                {Banner(subtitle: "Announcement Posted", backgroundColor: greenColour).show(duration: 2)})
            }
        }
    }
}









