//
//  EXAnnounceDetailVC.swift
//  Exhibiz
//
//  Created by Appzoc on 09/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit
import PlaceholderTextView

class AnnounceDetailCell1: UITableViewCell
{
    @IBOutlet var announceImage: BaseImageView!
    @IBOutlet var announceTitleLBL: UILabel!
    @IBOutlet var descriptionLBL: UILabel!
}

class AnnounceDetailCell2: UITableViewCell
{
    @IBOutlet var usernameLBL: UILabel!
    @IBOutlet var userCommentLBL: UILabel!
    @IBOutlet var userImage: BaseImageView!
    @IBOutlet var deleteBTN: UIButton!
    @IBOutlet var deleteBTNWidth: NSLayoutConstraint!
}


class EXAnnounceDetailVC: UIViewController, UITextViewDelegate
{
    @IBOutlet var contentTV: UITableView!
    @IBOutlet var commentTextView: TextViewFixed!
    
    var announceID = ""
    var titleString = ""
    var descriptionString = ""
    var announceImage = ""
    
    var isReloadRequired = true
    var contentArray = [[String:Any]]()
    var userType = UserType.Admin
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        commentTextView.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if isReloadRequired
        {
            isReloadRequired = false
            webCallGetComment()
        }
    }
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    { self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func deleteBTNTapped(_ sender: UIButton)
    {
        let optionMenu = UIAlertController(title: "Do you want to delete the Comment", message: nil, preferredStyle: .actionSheet)
        
        let delete = UIAlertAction(title: "Delete", style: .destructive, handler:
        { (alert: UIAlertAction!) -> Void in
            var parameter = [String:String]()
            parameter["id"] = (self.contentArray[sender.tag])["id"] as? String ?? ""
            parameter["type"] = "1"
            self.webCalldeleteComment(parameter: parameter)
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        optionMenu.addAction(delete)
        optionMenu.addAction(cancel)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        self.view.setNeedsLayout()
        
    }
    
    @IBAction func sentCommentBTNTapped(_ sender: UIButton)
    {
        if commentTextView.text.isEmpty
        {
            bannerWithMessage(message: "Enter your comment")
        }
        else
        {
            var parameter = [String:String]()
            parameter["userid"] = UserDefaults.standard.string(forKey: "UserID")!
            parameter["comment"] = commentTextView.text
            parameter["created_at"] = getCreatedDate()
    
            webCallPostComment(parameter: parameter)
        }
    }
    
}

// TableView Delegates
extension EXAnnounceDetailVC:UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if contentArray.isEmpty
        { return 2
        }
        else
        { return contentArray.count+1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! AnnounceDetailCell1
            cell.announceTitleLBL.text = titleString
            cell.descriptionLBL.text = descriptionString
            
            let url = URL(string: "\(baseURL)\(imageURL)\(announceImage)")
            cell.announceImage.kf.setImage(with: url, placeholder: UIImage(named: "noimage.jpg"))
            return cell
        }
        else if contentArray.isEmpty
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell3")
            return cell!
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! AnnounceDetailCell2
            
            if (self.contentArray[indexPath.row-1])["type_id"] as! String == "4"
            {
                cell.usernameLBL.text = (self.contentArray[indexPath.row-1])["company_name"] as? String ?? ""
            }
            else
            {
                cell.usernameLBL.text = (self.contentArray[indexPath.row-1])["username"] as? String ?? ""
            }
            cell.userCommentLBL.text = (self.contentArray[indexPath.row-1])["comment"] as? String ?? ""
            let url = URL(string: "\(baseURL)\(imageURL)\((self.contentArray[indexPath.row-1])["image"] as? String ?? "")")
            cell.userImage.kf.setImage(with: url, placeholder: UIImage(named: "noProfilePic.png"))
            
            if userType == .Admin
            {
                cell.deleteBTNWidth.constant = 40
                cell.deleteBTN.tag = indexPath.row-1
            }
            else
            {
                cell.deleteBTNWidth.constant = 0
            }
            return cell
        }
    }
}

// WebService
extension EXAnnounceDetailVC
{
    func webCallGetComment()
    {
        Webservices.getMethod(url: "ExhibizWeb/backend/api/get/announce_comment/\(self.announceID)", errorBanner: false)
        { (isFetched, resultData) in
            if isFetched
            {
                print(resultData)
                self.contentArray = resultData["Data"] as? [[String:Any]] ?? [[String:Any]]()
                self.contentTV.reloadData()
            }
            else
            {
                self.contentArray.removeAll()
                self.contentTV.reloadData()
            }
        }
    }
    
    func webCallPostComment(parameter:[String:String])
    {
        print(parameter)
        Webservices.postMethod(url: "ExhibizWeb/backend/api/add/announce_comment/\(self.announceID)", parameter: parameter)
        { (isFetched, resultData) in
            if isFetched
            {
                print(resultData)
                self.commentTextView.resignFirstResponder()
                self.commentTextView.text = ""
                self.webCallGetComment()
                self.contentTV.scrollTableToTop()
            }
        }
    }
    
    func webCalldeleteComment(parameter:[String:String])
    {
        print(parameter)
        Webservices.postMethod(url: "ExhibizWeb/backend/api/post/announcementorcomment_delete", parameter: parameter)
        { (isFetched, resultData) in
            if isFetched
            {
                print(resultData)
                self.webCallGetComment()
            }
        }
    }
}




