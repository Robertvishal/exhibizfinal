//
//  EXAnnounceListVC.swift
//  Exhibiz
//
//  Created by Appzoc on 09/02/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import UIKit

enum UserType
{
    case Admin
    case Organiser
    case Exhibitor
    case Visitor
}

class AnnounceListCell: UITableViewCell
{
    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var descriptionTxtView: UITextView!
    @IBOutlet var eventImage: BaseImageView!
    @IBOutlet var commntDeleteBTN: BaseButton!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        descriptionTxtView.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}

class EXAnnounceListVC: UIViewController
{
    @IBOutlet var contentTV: UITableView!
    @IBOutlet var sentAnnounceBTN: BaseButton!
    @IBOutlet var receivedAnnounceBTN: BaseButton!
    @IBOutlet var noAnnounceLBL: UILabel!
    
    @IBOutlet var announceHeight: NSLayoutConstraint!
    
    var isReloadRequired = true
    var contentArray = [[String:Any]]()
    
    var userType = UserType.Admin
    var url = ""
    var userID = UserDefaults.standard.string(forKey: "UserID")!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        if userType == .Visitor
        {
            announceHeight.constant = 0
            url = "ExhibizWeb/backend/api/get/announcements/"+userID+"?type=1"
        }
        else if userType == .Admin
        {
            url = "ExhibizWeb/backend/api/get/announcements/"+userID+"?type=0"
            receivedAnnounceBTN.setTitle("All", for: .normal)
        }
        else
        {
            url = "ExhibizWeb/backend/api/get/announcements/"+userID+"?type=0"
            receivedAnnounceBTN.setTitle("Received", for: .normal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if isReloadRequired
        {
            isReloadRequired = false
            webCall()
        }
    }
    
    @IBAction func sentAnnounceTapped(_ sender: BaseButton)
    {
        sentAnnounceBTN.backgroundColor = blueColor
        receivedAnnounceBTN.backgroundColor = UIColor.white
        sentAnnounceBTN.setTitleColor(UIColor.white, for: .normal)
        receivedAnnounceBTN.setTitleColor(UIColor.darkGray, for: .normal)
        
        url = "ExhibizWeb/backend/api/get/announcements/"+userID+"?type=0"
        webCall()
    }
    
    @IBAction func receivedAnnounceTapped(_ sender: BaseButton)
    {
        sentAnnounceBTN.backgroundColor = UIColor.white
        receivedAnnounceBTN.backgroundColor = blueColor
        receivedAnnounceBTN.setTitleColor(UIColor.white, for: .normal)
        sentAnnounceBTN.setTitleColor(UIColor.darkGray, for: .normal)
        
        url = "ExhibizWeb/backend/api/get/announcements/"+userID+"?type=1"
        webCall()
    }
    
    @IBAction func deleteAnnounceBTNTapped(_ sender: UIButton)
    {
        if userType == .Admin
        {
            let optionMenu = UIAlertController(title: "Do you want to delete the Announcement", message: nil, preferredStyle: .actionSheet)
            
            let delete = UIAlertAction(title: "Delete", style: .destructive, handler:
            { (alert: UIAlertAction!) -> Void in
                var parameter = [String:String]()
                parameter["id"] = (self.contentArray[sender.tag])["announcement_id"] as? String ?? ""
                parameter["type"] = "0"
                self.webCallDeleteAnnounce(parameter: parameter)
            })
            
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            optionMenu.addAction(delete)
            optionMenu.addAction(cancel)
            self.present(optionMenu, animated: true, completion: nil)
        }
        else
        {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXAnnounceDetailVC") as! EXAnnounceDetailVC
            nextVC.titleString = (self.contentArray[sender.tag])["announcementtitle"] as! String
            nextVC.descriptionString = (self.contentArray[sender.tag])["announcement"] as! String
            nextVC.announceImage = (self.contentArray[sender.tag])["image"] as! String
            nextVC.announceID = (self.contentArray[sender.tag])["announcement_id"] as! String
            nextVC.userType = self.userType
            self.present(nextVC, animated: true, completion:
            {
                nextVC.commentTextView.becomeFirstResponder()
            })
        }
        
    }
    
    
    @IBAction func backBTNTapped(_ sender: UIButton)
    {
        if self.navigationController?.visibleViewController == self{
            self.navigationController?.popViewController(animated: true)
            let vc = UIStoryboard(name: "EXVisitor", bundle: nil).instantiateViewController(withIdentifier: "EXAllEventsVisitorVC") as! EXAllEventsVisitorVC
            self.navigationController?.pushViewController(vc, animated: false)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
}

// TableView Delegates
extension EXAnnounceListVC:UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return contentArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! AnnounceListCell
        cell.titleLBL.text = (contentArray[indexPath.row])["announcementtitle"] as? String ?? ""
        cell.descriptionTxtView.text = (contentArray[indexPath.row])["announcement"] as? String ?? ""
        
        let url = URL(string: "\(baseURL)\(imageURL)\((contentArray[indexPath.row])["image"] as? String ?? "")")
        cell.eventImage.kf.setImage(with: url, placeholder: UIImage(named: "noimage.jpg"))
        
        if userType == .Admin
        {
            cell.commntDeleteBTN.setTitle("Delete", for: .normal)
            cell.commntDeleteBTN.isUserInteractionEnabled = true
            cell.commntDeleteBTN.tag = indexPath.row
        }
        else
        {
            cell.commntDeleteBTN.setTitle("Comment", for: .normal)
            cell.commntDeleteBTN.isUserInteractionEnabled = true
            cell.commntDeleteBTN.tag = indexPath.row
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "EXAnnounceDetailVC") as! EXAnnounceDetailVC
        nextVC.titleString = (self.contentArray[indexPath.row])["announcementtitle"] as! String
        nextVC.descriptionString = (self.contentArray[indexPath.row])["announcement"] as! String
        nextVC.announceImage = (self.contentArray[indexPath.row])["image"] as! String
        nextVC.announceID = (self.contentArray[indexPath.row])["announcement_id"] as! String
        nextVC.userType = self.userType
        self.present(nextVC, animated: true, completion: nil)
    }
}

// WebService
extension EXAnnounceListVC
{
    func webCall()
    {
        Webservices.getMethod(url: self.url, errorBanner: false)
        { (isFetched, resultData) in
            if isFetched
            {
                print(resultData)
                self.contentArray = resultData["Data"] as? [[String:Any]] ?? [[String:Any]]()
                self.contentTV.reloadData()
                self.noAnnounceLBL.isHidden = true
            }
            else
            {
                self.contentArray.removeAll()
                self.contentTV.reloadData()
                self.noAnnounceLBL.isHidden = false
            }
        }
    }
    
    func webCallDeleteAnnounce(parameter:[String:String])
    {
        Webservices.postMethod(url: "ExhibizWeb/backend/api/post/announcementorcomment_delete",
                               parameter: parameter)
        { (isFetched, resultData) in
            if isFetched
            {
                print(resultData)
                self.webCall()
            }
            else
            {
            }
        }
    }
}









